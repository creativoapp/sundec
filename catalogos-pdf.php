<?php require('components/header.php'); ?>
<section class="is-view catalog-pdf-view">
    <div class="container">
        <div class="columns">
            <div class="column is-two-thirds">
                <h1>CATALOGOS EN VERSIÓN PDF</h1>
                <p>Sundec Cancún pone a tu disposición también la versión en PDF de nuestros catalogos de productos y servicios.</p>
                <br><br>
                <?php
                $catalog = new Catalog();
                $pdf_list = json_decode($catalog->viewAllCatalogs());
                foreach ($pdf_list as $modules) {
                ?>
                    <div class="boxAnclaPdf">
                        <h3><?= strtoupper($modules->categoria) ?></h3>
                        <ul class="listPdfCatalogs">
                            <?php
                            foreach ($modules->files as $pdf) {
                            ?>
                                <li>
                                    <a href="/sources/catalogo/pdf/<?= trim($pdf->file) ?>" target="_blank" title="<?= $pdf->catalogo ?>"><i class="fa fa-file-pdf-o"></i></a>
                                    <a href="/sources/catalogo/pdf/<?= trim($pdf->file) ?>" target="_blank"><span><?= $pdf->catalogo ?></span></a>
                                </li>
                            <?php
                            }
                            ?>
                            <div class="clr"></div>
                        </ul>
                    </div>
                <?php } ?>
            </div>
            <div class="column">
                <div class="boxOffice-data">
                    <h3>Encuentranos en:</h3>
                    <span>Av. Huayacan Zona 5 </span>
                    <span>Manzana 101 Lote 29</span>
                    <span>Cancún, Quintana Roo C.P.77560</span>
                    <br><br>
                    <div>
                        <img alt="Cortinas y Persianas decorativas en Cancún" src="/sources/side-mail.png">
                        <a href="mailto:contacto@sundecdecoracion.com" class="contactSide">contacto@sundecdecoracion.com</a>
                        <div class="clr"></div>
                    </div>
                    <div class="clr"></div>
                    <div>
                        <img alt="Pisos de madera o deck en Cancún empresa que instala" src="/sources/side-phone.png">
                        <a href="tel:(998) 2149274 / (998) 8889292" class="contactSide">(998) 2149274 / (998) 8889292</a>
                        <div class="clr"></div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require('components/footer.php'); ?>