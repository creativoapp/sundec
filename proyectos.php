<?php require('components/header.php'); ?>

<section class="is-view is-view-projects container">
    <div class="columns is-multiline">
        <div class="column is-full">
        
            <h1>NUESTROS PROYECTOS</h1>
            <p>Conoce algunos de nuestros proyectos concluidos y clientes satisfechos.</p>

        </div>

        <?php 
			$project = new Projects();
			$view = $project->viewProjects();

			foreach ($view as $html) { ?>
				
				<article class="column is-one-quarter isProject">
					<img src="/sources/projects/<?= $html['miniatura']; ?>" alt="<?php echo $html['altimg']; ?>">
                    <h2><?= $html['proyecto']; ?></h2>
				</article>

			<?php
			}
			?>

    </div>
</section>

<?php require('components/footer.php'); ?>