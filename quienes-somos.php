<?php require('components/header.php'); ?>


<section class="is-view is-view-about">
    <div class="container">
        
        <div class="columns">
            <div class="column is-half">
                <h1>¿QUIENES SOMOS?</h1>
                <h2>Sundec Decoración</h2>

                <p>Sundec Decoración en Cancún, es una empresa mexicana establecida en la ciudad de Cancún, Q. Roo, dedicada a la 
                venta de productos de alta calidad que cubran las necesidades más exigentes para el mercado de la decoración 
                residencial, comercial y hotelera, buscando siempre la satisfacción plena del usuario final, a través de dicha 
                calidad y un excelente servicio.</p>

                <p>Sundec Decoración cuenta con varios Productos y Servicios para la Decoración de su casa u Hotel en Cancún, 
                entre los cuales destacan sus Cortinas y Persianas que pueden ser colocadas en cualquier establecimiento de Cancún.</p>
            
                <p>Contamos con diferentes artículos de decoración para complementar tu diseño, al igual que persianas y cortinas que harán lucir tus espacios mejor. También tenemos alfombras, toldos, papel tapiz, pisos de madera, pisos vinílicos, decks y muchos productos más. ¡Ven a visitarnos!</p>
            </div>

            <div class="column is-half isImage">
                <img src="/assets/img/decoracion-en-cancun-sundec.jpg" title="Sundec Cancún Decoración para el Hogar" alt="Tienda de Decoración en Cancún Muebles Papel Tapiz y Persianas">
                <a href="<?php echo $path.'presentacion-sundec-decoracion.pdf'; ?>" target="_blank" class="buttonPresentation">PRESENTACIÓN</a>
            </div>
        </div>

        <div class="columns">
			<div class="column is-10 is-offset-1 isVideo">
				<!--VIDEO-->
				<iframe width="100%" height="600px" src="//www.youtube.com/embed/WytzmMooQ9U" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
    
    </div>
</section>

<?php require('components/footer.php'); ?>