<?php require('components/header.php'); ?>


<section class="is-view-home">

	<!--SLIDER-->
	<div class="is-slider">
		<div id="slideshow" class="nivoSlider">
		<?php 
			$slider = new Slider();
			$imageslider = $slider->getsliderimages();
			
			foreach($imageslider as $slide) {

			if($slide['link'] == 1) {
			?>
				<a href="http://<?= $slide['link_url']; ?>" title="<?= $slide['title']; ?>"><img src="/sources/slider/<?= $slide['img']; ?>" alt="<?= $slide['alt']; ?>"></a>
			
			<?php } else { ?>
			
				<img src="/sources/slider/<?= $slide['img']; ?>" alt="<?= $slide['alt']; ?>">
			
			<?php } 
			
			}
			?>

		</div>
	</div>

	<!--CATALOG-->
	<div class="is-overview">
		<div class="container">
			<div class="columns is-multiline">
				
				<div class="column is-half is-text">
					<h1>DECORACIÓN EN CANCÚN</h1>
					<h2>Sundec Decoración</h2>
					<p>Sundec Decoración en Cancún, es una empresa mexicana establecida en la ciudad de Cancún, Q. Roo, dedicada a la 
					venta de productos de alta calidad que cubran las necesidades más exigentes para el mercado de la decoración 
					residencial, comercial y hotelera, buscando siempre la satisfacción plena del usuario final, a través de dicha 
					calidad y un excelente servicio.</p>
					<p>Sundec Decoración cuenta con varios Productos y Servicios para la Decoración de su casa u Hotel en Cancún, 
					entre los cuales destacan sus Cortinas y Persianas que pueden ser colocadas en cualquier establecimiento de Cancún. Además de contar con una variedad de diseños únicos que hará resaltar cualquier decoración de interiores en Cancún.</p>
					<p>Entre las especialidades de decoración, tenemos artículos decorativos con diseños vanguardistas y únicos, así como alfombras y decks para contrastar cualquier ambiente ya sea en una sala, oficina u hotel.</p>
					<p>Sundec se encarga de proveerte los mejores pisos, cortinas, persianas, toldos, decks, alfombras y artículos decorativos para tu proyecto de diseño de interiores. Tenemos años de experiencia en el mercado, cuenta con que encontrarás lo que necesitas para tener la decoración que deseas.</p>
				</div>

				<div class="column is-half isCategoryCatalog isFirstCategoryCatalog">
					<a href="catalogo/persianas-cortinas-cancun"><img title="Persianas en Cancun" src="/assets/img/persianas-cortinas-en-cancun.jpg" alt="Cortinas y Persianas en Cancún Decorativas" /></a>
					<h2><a title="Persianas y Cortinas en Cancún" href="catalogo/persianas-cortinas-cancun">PERSIANAS Y CORTINAS</a></h2>
					<div class="isCities">
						<a href="/catalogo/persianas-cortinas-cancun">Cancún</a><i class="fas fa-circle"></i>
						<a href="/playa-del-carmen/catalogo/cortinas-y-persinas">Playa del Carmen</a><i class="fas fa-circle"></i>
						<a href="/tulum/catalogo/cortinas-y-persinas">Tulum</a>
					</div>
				</div>

				<div class="column is-half isCategoryCatalog">
					<a href="/catalogo/toldos-cancun"><img title="Toldos en Cancun" src="/assets/img/toldos-en-cancun.jpg" alt="Toldos en Cancún para Jardín o alberca de hotel" /></a>
					<h2><a title="Celosías en Cancún" href="catalogo/toldos-cancun">TOLDOS</a></h2>
					<div class="isCities">
						<a href="/catalogo/toldos-cancun">Cancún</a><i class="fas fa-circle"></i>
						<a href="/playa-del-carmen/catalogo/toldos">Playa del Carmen</a><i class="fas fa-circle"></i>
						<a href="/tulum/catalogo/toldos">Tulum</a>
					</div>
				</div>

				<div class="column is-half isCategoryCatalog">
					<a href="/catalogo/pisos-de-madera-cancun"><img title="Pisos de Madera en Cancún" src="/assets/img/pisos-en-cancun.jpg" alt="Pisos de Madera en Cancún" /></a>
					<h2><a title="Pisos de Madera en Cancún" href="catalogo/pisos-de-madera-cancun">PISOS</a></h2>
					<div class="isCities">
						<a href="/catalogo/pisos-de-madera-cancun">Cancún</a><i class="fas fa-circle"></i>
						<a href="/playa-del-carmen/catalogo/pisos">Playa del Carmen</a><i class="fas fa-circle"></i>
						<a href="/tulum/catalogo/pisos">Tulum</a>
					</div>
				</div>

			</div>
		</div>
	</div>

	
	<div class="container">
		
		<div class="columns">
			<!--BRANDS-->
			<div class="column is-full">

			<?php 
				$brandsModel = new Brands();
				$brands = json_decode($brandsModel->getBrands(10));

				if($brands != null) {
			?>

			<h3 class="is-text-center">LAS MEJORES MARCAS</h3>

			<div class="owl-carousel isBrands">

				<?php foreach($brands as $brand) { ?>

				<div class="item">
					<img src="/sources/marcas/<?= $brand->{'brandimg'};?>">
				</div>

				<?php } ?>

				

			</div>

			<?php } ?>

			</div>
		
		</div>


		<div class="columns">
			<div class="column is-10 is-offset-1 isVideo">
				<!--VIDEO-->
				<iframe width="100%" height="600px" src="//www.youtube.com/embed/WytzmMooQ9U" frameborder="0" allowfullscreen></iframe>
			</div>

		</div>
	</div>

</section>
	
<?php require 'components/footer.php'; ?>