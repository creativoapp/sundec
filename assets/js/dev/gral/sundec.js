$(document).ready(function(){

    // Albums
    if($('.isMenuTabs').length > 0) {

        $('.isMenuTabs a').on('click', function(event){
            event.preventDefault();

            var albumto = $(this).attr('href');

            $("body, html").animate({ 
                scrollTop: $(''+albumto+'').offset().top - 175
            }, 1000);

        });


        $(window).on('scroll', function() {  

            var scroll = $(this).scrollTop();
            
            if(scroll > 600) {
                $('.isMenuTabs').addClass('isFixed');
            }
            else {
                $('.isMenuTabs').removeClass('isFixed');
            }

        });

    }
    

});