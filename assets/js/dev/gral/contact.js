$(document).ready(function () {
    $('form#contacto').on('submit', function (e) {
        e.preventDefault();
        var from = new String(window.location);
        var data = {
            'nombre': $('#ipt-name').val().trim(),
            'email': $('#ipt-mail').val().trim(),
            'telefono': $('#ipt-phone').val().trim(),
            'mensaje': $('#ipt-message').val().trim(),
            'from': from,
            'action': 'contact'
        };
        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        var valid = true;
        if (data.mensaje == '') {
            valid = false;
            $('#ipt-message').focus();
        }
        if (data.email == '' || !regex.test(data.email)) {
            valid = false;
            $('#ipt-mail').focus();
        }
        if (data.nombre == '') {
            valid = false;
            $('#ipt-name').focus();
        }
        if ($('#ipt-promo').length) {
            data.promo = $('#ipt-promo').val();
        }
        if (valid == true) {
            $.ajax({
                data: data,
                url: window.location.protocol + '//' + window.location.hostname + '/handlers/hdl-mails.php',
                type: 'POST',
                beforeSend: function () {
                    $('#sendForm').val('ENVIANDO...');
                    $('#sendForm').attr("disabled", true);
                },
                success: function (rs) {
                    if (rs == true) {
                        alert('Hemos recibido su información satisfactoriamente, nos comunicaremos con usted a la brevedad posible.');
                        location.reload();
                    }
                    if (rs == false) {
                        alert('Oh oh, no se han podido enviar los datos.' + "\r\nIntentelo mas tarde por favor!!");
                        $('#sendForm').val('CONTACTAR');
                        $('#sendForm').attr("disabled", false);
                    }
                }
            });
        }
    });
});