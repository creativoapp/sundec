$(window).load(function() {
	
	$('#slideshow').nivoSlider({
		effect: 'fade',
		animSpeed: 500,                 // Slide transition speed
		pauseTime: 5000,
		controlNav: false,
		directionNav: false
	});

});


$(document).ready(function(){

    var app = {};

	//Wowjs
	new WOW().init();

	$('.isBrands').owlCarousel({
		loop: true,
		margin: 50,
		nav: false,
		autoplay: true,
		responsive:{
			0:{
				items:2
			},
			600:{
				items:3
			},
			1000:{
				items:5
			}
		}
	});


	//Fancybox
	$('.fancybox').fancybox({
		padding: 5
	});

});