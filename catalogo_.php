<?php require('components/header.php'); ?>

<section class="is-view is-view-catalog">
	<div class="container">

		<div class="columns">
			<div class="column is-half is-overview">
				<h1>Catálogo de Decoración <span>en Cancún</span></h1>
				<p>En nuestro <strong>Catálogo de Decoración</strong> por Sundec Cancún encontrarás una gran variedad de productos y servicios para la Decoración de Interiores y Exteriores de Casas, Oficinas u Hoteles.</p>
				<p>Somos Profesionales de la Decoración brindando Soluciones creativas en todos los aspectos necesarios para crear un ambiente moderno y amigable</p>
				<p>Contamos con los siguientes Servicios de Decoración en Cancún: Persianas y Cortinas, Toldos, Pisos de Ingeniería, madera, vinílicos, laminados, Decks Sintéticos y Naturales, Muebles, Telas para tapicería, cojines y cortinas, Papel Tapiz, Candiles, Lámparas Decorativas y Artículos Decorativos </p>
				<p>Sundec se ha dedicado desde hace varios años a brindar soluciones en decoración de interiores en Cancún a cada cliente que busca un diseño nuevo para el hogar, despacho, estudio, oficina y cualquier ambiente.</p>
			</div>
			<div class="column is-half isCities">
				<img src="/assets/img/catalogo-decoracion-en-cancun.jpg">

				<h3 class="isCancun"><a href="/catalogo">Decoración en <span>Cancún</span></a></h3>
				<h3 class="isPlaya"><a href="/playa-del-carmen/catalogo">Decoración en <span>Playa del Carmen</span></a></h3>
				<h3 class="isTulum"><a href="/tulum/catalogo">Decoración en <span>Tulum</span></a></h3>
			</div>
		</div>

	</div>

	<div class="isCatalogs">
		<div class="container">
			<div class="columns is-multiline">

				<?php
				$catalog = new Catalog();
				$catalogs = $catalog->getCatalogs();

				if ($catalogs != null) {
					$iCol = 1;
					foreach ($catalogs as $ctry) {
				?>

						<div class="column is-half is-clearfix isCtry isDir<?= $iCol; ?>">
							<a href="<?= $ctry['link']; ?>" title="<?= $ctry['titlelink']; ?>"><img src="/sources/catalogo/<?= $ctry['image']; ?>" alt="<?= $ctry['alt']; ?>"></a>
							<div class="isInfo">
								<h2><a href="<?= $ctry['link']; ?>" title="<?= $ctry['titlelink']; ?>"><?= $ctry['category']; ?></a></h2>
								<p><?= $ctry['extract']; ?>...</p>
								<a href="<?= $ctry['link']; ?>" title="<?= $ctry['titlelink']; ?>">Saber más <i class="fas fa-angle-right"></i></a>
							</div>
						</div>

				<?php
						$iCol++;
						if ($iCol == 3) {
							$iCol = 1;
						}
					}
				}
				?>
			</div>
		</div>
	</div>

</section>

<?php require('components/footer.php'); ?>