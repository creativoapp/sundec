<?php
ob_start();

$path = "http://www.sundecdecoracion.com/";
$page = basename($_SERVER['SCRIPT_NAME']);
$route = $_SERVER['PHP_SELF'];

// Constants
define('_CSS', '/assets/css/');
define('_JS', '/assets/js/');

function setActive($nav, $page)
{
    echo $nav . '.php' == $page ? 'is-active' : null;
}


include('models/mSlider.php');
include('models/mSettings.php');
include('models/mSeo.php');
include('models/mCatalogo.php');
include('models/mGalerias.php');
include('models/mBrands.php');
include('models/mPromos.php');
include('models/mProjects.php');
include('models/mCities.php');

$dates = new Setting();
$settings = json_decode($dates->showDates());
$contacto = json_decode($dates->showContacto());


//maniobra web cancun
//$eroute = explode('/', $route);
//$nroute = '/'.$eroute[2];

if ($route == '/catalog-page.php') {
    $destroy = explode('/', $_SERVER['QUERY_STRING']);
    $destroy = explode('&', $destroy[0]);
    $param_city = str_replace('city=', '/', $destroy[0]);
    $param_category = str_replace('.php', '', str_replace('categoria=', '/', $destroy[1]));
    if ($param_city == '/cancun') {
        $route = '/catalogo' . $param_category;
    } else {
        $route = $param_city . '/catalogo' . $param_category;
    }
    $mCity = new Cities();
    //Validar Ciudad
    $city_obj = $mCity->ValidateCity($param_city);
    if ($city_obj['status'] == false) {
        header("Location:/404");
    }
}

if ($route == '/catalogo-city.php') {
    $city_obj = ['status' => false];
    if (isset($_GET['city'])) {
        $mCity = new Cities();
        $param_city = '/'.$_GET['city'];
        $city_obj = $mCity->ValidateCity($param_city);
    }
    if ($city_obj['status'] == false) {
        header("Location:/404");
    }
}
