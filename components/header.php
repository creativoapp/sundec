<?php include('config.php'); ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    
    <?php 
        if($route == '/catalog-page.php'){
            $destroy = explode('/', $_SERVER['QUERY_STRING']);
            $route = '/catalogo/'.$destroy[1];
            //$nroute = '/catalogo/'.$destroy[1]; //webcancun
        }
    
        $seo = new Seo();
        $metas = $seo->getSeo($route);
        
        if($metas == null) {

            $metas = array('title' => 'Decoración en Cancún | Persianas, Cortinas, Pisos, alfombras y más | Sundec Decoración',
                                   'keywords' => null,
                                   'description' => 'Sundec Decoración Cancún. Empresa especializada en Decoración de Interiores en Cancún, también de exteriores; cuenta cortinas y persianas decorativas. También se especializa en pisos de madera y muebles Decorativos.');
        }

        $metas = (object)$metas;
    ?>

    <title><?= $metas->title; ?></title>
    <meta name="keywords" content="<?= $metas->keywords; ?>" />
    <meta name="description" content="<?= $metas->description; ?>" />
    
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon" />
  
    <!--CSS
    ========================== -->
    <link rel="stylesheet" href="<?= _CSS.'bulma.min.css'; ?>">
    <link rel="stylesheet" href="<?= _CSS.'components.min.css'; ?>">
    <link rel="stylesheet" href="<?= _CSS.'sd.min.css'; ?>">

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body class="body">
    <?php $headerClass = $page == 'index.php' ? 'is-transparent' : 'is-solid'; ?>
    <header class="is-header <?= $headerClass; ?>">
        <div class="container">
            <div class="columns is-multiline">
            
                <div class="column is-one-fifth is-logo">
                    <a href="/"><img title="Sundec Cancún | Soluciones Decorativas" alt="Decoración en Cancún, empresa de Soluciones Decorativas" src="/assets/img/sundec-decoracion-cancun.png"></a>
                </div>

                <a href="#" id="btn-menu"><i class="fa fa-bars"></i></a>

                <div class="column one-third is-phones">
                    <a href="mailto:<?= $settings->{'mailH'};?>"><i class="fas fa-envelope-open-text"></i><?= $settings->{'mailH'};?></a>
                    <a href="tel:9982149274"><i class="fas fa-fax"></i> (998) 214 9274<!--<?php echo $settings->{'phoneH'};?>--></a>
                    <a href="https://wa.me/+19982149274?text=Hola,%20estoy%20interesado%20en%20sus%20productos" target="_blank"><i class="fab fa-whatsapp"></i> (998) 214 9274</a>
                </div>

                <div class="column is-one-fifth is-social">
                    <a href="https://www.facebook.com/sundecdecoracion" target="_blank" class="is-facebook" title="Síguenos en Facebook"><i class="fab fa-facebook"></i></a>
                    <a href="https://twitter.com/Sundec_cancun" class="is-twitter" target="_blank" title="Síguenos en Twitter"><i class="fab fa-twitter"></i></a>
                    <a href="https://www.instagram.com/sundecdecoracion/" class="is-instagram" target="_blank" title="Síguenos en Instagram"><i class="fab fa-instagram"></i></a>
                    <a href="http://www.pinterest.com/sundec/" class="is-pinterest" target="_blank" title="Síguenos en Pinterest"><i class="fab fa-pinterest"></i></a>
                </div>

                <div class="column is-full is-menucol">
                    <ul class="is-menu">
                        <li><a title="Sundec Decoración Cancún" href="/" class="<?php setActive('index', $page); ?>" >INICIO</a></li>
                        <li><a title="Soluciones Decorativas Sundec Cancún" href="/quienes-somos" class="<?php setActive('quienes-somos', $page); ?>">QUIENES SOMOS</a></li>
                        <li><a title="Catálogo Sundec Decoración" href="/catalogo" class="<?php setActive('catalogo', $page); ?>">CATÁLOGO</a></li>                        
                        <li><a title="Proyectos Decorativos por Sundec Decoración" href="/proyectos" class="<?php setActive('proyectos', $page); ?>">PROYECTOS</a></li>
                        <li><a title="Marcas Decorativas por Sundec Decoración" href="/nuestras-marcas" class="<?php setActive('nuestras-marcas', $page); ?>">NUESTRAS MARCAS</a></li>                        
                        <li><a title="Promociones en Decoración en Cancún" href="/promociones" class="<?php setActive('promociones', $page); ?>">PROMOCIONES</a></li>
                        <li><a title="Cotizar Decoración de Interiores Cancún" href="/contacto" class="<?php setActive('contacto', $page); ?>">CONTACTO</a></li>
                        <li><a title="Blog sobre soluciones decorativas en Cancún" href="/blog/">BLOG</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </header>