<?php
$catalog = new Catalog();
$catalog_cancun = $catalog->getCatalogs(1);
$catalog_playa = $catalog->getCatalogs(2);
$catalog_tulum = $catalog->getCatalogs(3);
?>
<footer class="is-footer">
	<div class="container">
		<div class="columns is-multiline">

			<div class="column is-one-quarter">
				<strong class="is-title">Cancún</strong>
				<ul>
					<?php foreach ($catalog_cancun as $ctlog) { ?>
						<li class="is-uppercase"><a href="<?= $ctlog['link'] ?>"><?= $ctlog['category'] ?></a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="column is-one-quarter">
				<strong class="is-title">Playa del Carmen</strong>
				<ul>
					<?php foreach ($catalog_playa as $ctlog) { ?>
						<li class="is-uppercase"><a href="<?= $ctlog['link'] ?>"><?= $ctlog['category'] ?></a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="column is-one-quarter">
				<strong class="is-title">Tulum</strong>
				<ul>
					<?php foreach ($catalog_tulum as $ctlog) { ?>
						<li class="is-uppercase"><a href="<?= $ctlog['link'] ?>"><?= $ctlog['category'] ?></a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="column is-one-quarter is-social">
				<a href="https://www.facebook.com/sundecdecoracion" target="_blank" class="is-facebook" title="Síguenos en Facebook"><i class="fab fa-facebook"></i></a>
				<a href="https://twitter.com/Sundec_cancun" class="is-twitter" target="_blank" title="Síguenos en Twitter"><i class="fab fa-twitter"></i></a>
				<a href="https://www.instagram.com/sundecdecoracion/" class="is-instagram" target="_blank" title="Síguenos en Instagram"><i class="fab fa-instagram"></i></a>
				<a href="http://www.pinterest.com/sundec/" class="is-pinterest" target="_blank" title="Síguenos en Pinterest"><i class="fab fa-pinterest"></i></a>
				<span><i class="fas fa-envelope-open-text"></i><?= $settings->{'mailH'}; ?></span>
				<span>Av Huayacán 5, Zona Hotelera, 77560 Cancún, Quintana Roo</span>
			</div>

			<div class="column is-full">
				<ul class="is-links">
					<li><a title="Decoración en Cancún" href="/">INICIO</a></li>
					<li><a title="Soluciones Decorativas Sundec Cancún" href="quienes-somos">QUIENES SOMOS</a></li>
					<li><a title="Catálogo Sundec Decoración" href="catalogo">CATALOGO</a></li>
					<li><a title="Marcas decorativas por Sundec Decoración" href="nuestras-marcas">NUESTRAS MARCAS</a></li>
					<li><a title="Promociones en Decoración en Cancún" href="promociones">PROMOCIONES</a></li>
					<li><a title="Contacta y Cotiza para Decorar tu Hogar" href="contacto">CONTACTO</a></li>
					<li><a title="Sundec Decoración Cancún Blog" href="/blog/">BLOG</a></li>
				</ul>
				<span class="is-by">© <?= date('Y'); ?>. Sundec Decoración. Todos los derechos Reservados. <small><a href="https://www.webcancun.com.mx/paginas-web" target="_blank" title="Páginas web en cancún">Diseño Web</a> y <a href="https://www.webcancun.com.mx/posicionamiento-web" target="_blank" title="Posicionamiento web en cancun">Posicionamiento Web</a>: <a href="https://www.webcancun.com.mx" target="_blank" title="Marketing digital en cancún">Web Cancún</a></small></span>
			</div>



		</div>
	</div>
</footer>


<script src="https://kit.fontawesome.com/94233a6903.js" crossorigin="anonymous" defer></script>

<!--JS
    ========================== -->
<script type="text/javascript" src="<?= _JS . 'jquery-1.9.0.min.js'; ?>"></script>
<script type="text/javascript" src="<?= _JS . 'components.js'; ?>"></script>
<script type="text/javascript" src="<?= _JS . 'sd.js'; ?>"></script>

<script src="https://kit.fontawesome.com/94233a6903.js" crossorigin="anonymous"></script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
	var Tawk_API = Tawk_API || {},
		Tawk_LoadStart = new Date();
	(function() {
		var s1 = document.createElement("script"),
			s0 = document.getElementsByTagName("script")[0];
		s1.async = true;
		s1.src = 'https://embed.tawk.to/5f060e1567771f3813c0ab96/default';
		s1.charset = 'UTF-8';
		s1.setAttribute('crossorigin', '*');
		s0.parentNode.insertBefore(s1, s0);
	})();
</script>

<!--Google analytics-->
<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-57365544-1', 'auto');
	ga('require', 'displayfeatures');
	ga('send', 'pageview');
</script>

</body>

</html>
<?php ob_end_flush(); ?>