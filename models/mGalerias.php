<?php
require_once('BD.php');

//@Class::YATES
//@Autor::Alex Jimenez
//@Clase para la gestion/creacion de paginas
class Gallerys extends BD
{

	private $folderSources;

	function __construct()
	{
	 	$this->folderSources = 'http://www.yachtrentalsincancun.com/sources/galerias/';
	}


	//@Method::listPages
	//@Autor::Alex Jimenez
	//@Metodo que lista las paginas creadas
	function listGallery($type)
	{
		$bd = $this->openBD();
		if(!empty($type))
		{
			$filter = $bd->prepare('SELECT * FROM categorias, anclas WHERE categorias.idCategoria = anclas.fkCategory AND categorias.idCategoria = :category AND anclas.status = 1');
			$filter->bindParam('category', $type);			
		}
		else
		{
			$filter = $bd->prepare('SELECT  * FROM categorias, anclas WHERE categorias.idCategoria = anclas.fkCategory');
		}
		$filter->execute();

		define('_PAGES', 30);
		if(isset($_GET['page']))
		{
		  	$page = $_GET['page'];
		}
		else
		{
		    $page = 1;
		}
		
		$inicio = ($page - 1) * _PAGES;
		$registros = $filter->rowCount();
		$noPages = ceil($registros / _PAGES);
		
		if(!empty($type))
		{
			$query = $bd->prepare('SELECT categoria, idAncla, ancla, urlPage FROM categorias, anclas, seo WHERE categorias.idCategoria = anclas.fkCategory AND categorias.idCategoria = seo.idYacht AND categorias.idCategoria = :category AND anclas.status = 1 ORDER BY categoria ASC LIMIT :inicio,'._PAGES.' ');
			$query->bindParam('category', $type);
		}
		else
		{
			$query = $bd->prepare('SELECT categoria, idAncla, ancla, urlPage FROM categorias, anclas, seo WHERE categorias.idCategoria = anclas.fkCategory AND categorias.idCategoria = seo.idYacht AND anclas.status = 1 ORDER BY categoria ASC LIMIT :inicio,'._PAGES.' ');
		}
		
		$query->bindParam('inicio', $inicio, PDO::PARAM_INT);
		$query->execute();

		$table = '<table class="tResults tPages searchResults">
				  <tr>
				  		<td>Categoria</td><td>Ancla</td><td>Pagina</td><td>Fotos</td><td>&nbsp;</td>
				  </tr>';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			$numero = $bd->prepare('SELECT * FROM albums WHERE fkAncla = :idp');
			$numero->bindParam(':idp', $row['idAncla']);
			$numero->execute();

			$num = $numero->rowCount();
			
			$table .= '<tr>
							<td>'.$row['categoria'].'</td>
							<td>'.$row['ancla'].'</td>
							<td>'.$row['urlPage'].'</td>
							<td><span class="circle-num">'.$num.'</span></td>
							<td><a href="gallery?setting='.$row['idAncla'].'" title="Acciones"><img src="sources/setting-action.png" width="20"></a></td>
					   </tr>';
		}

		$table .= '</table>';

		if($noPages > 1)
			{
			    for($y = 1; $y <= $noPages; $y++ )
			    {
			        if($page == $y)
			        {
			            $table .= '<a class="page currentpag" href="gallery?page='.$y.'">'.$y.'</a>';
			        }
			        else
			        {
			            $table .= '<a class="page" href="gallery?page='.$y.'">'.$y.'</a>';
			        }
			    }
			}
		$this->closeBD($bd);
		return $table;
		
		
		
	}


	//@Method::insertImages
	//@Autor::Alex Jimenez
	//@Metodo para la insercion de imagenes para las galerias
	function insertImages($page, $ancla, $routeimg)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('INSERT INTO albums (photo, name, fkCategory, fkAncla) VALUES (:image, :name, :fkc, :fka)');
		$run = $query->execute(array(
									'image' => $routeimg,
									'name' => $ancla,
									'fkc' => $page,
									'fka' => $ancla ));
		
		if($run == 1)
		{
			$response = json_encode(array('state' => 'succes'));
		}
		else
		{
			$response = json_encode(array('state' => 'failed'));
		}
		
		return $response;
		$this->closeBD($bd);	
	}



	//@Method::objectGallery
	//@Autor::Alex Jimenez
	//@Metodo que imprime la galeria
	function objectGallery($page)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT yates.idYacht FROM yates, seo WHERE seo.urlPage = :page AND seo.idYacht = yates.idYacht');
		$query->bindParam(':page', $page);
		$run = $query->execute();
		
		if($run == 1)
		{
			$row = $query->fetch(PDO::FETCH_ASSOC);
			$filterGallery = $row['idYacht'];
			
			$getquery = $bd->prepare('SELECT * FROM galeria WHERE idPage = :filter');
			$getquery->bindParam(':filter', $filterGallery);
			$exe = $getquery->execute();

			if($exe == 1)
			{
				$listImages = '';
				while ($object = $getquery->fetch(PDO::FETCH_ASSOC)) 
				{
					//$listImages .= '[ENTER]'.$object['imageGallery'];
					$listImages .= '<a href="'.$this->folderSources.$object['imageGallery'].'">
                						<img 
                    						src="'.$this->folderSources.$object['imageGallery'].'"
                    						alt="'.$object['altImageGallery'].'"
                    						data-big="'.$this->folderSources.$object['imageGallery'].'"
                    						data-title="'.$object['viewTitle'].'"
                    						data-description="'.$object['viewDescription'].'" />
            						</a>';
				}
				return $listImages;
			}	

		}
		
		$this->closeBD($bd);	
	}


	//@Method::viewImages
	//@Autor::Alex Jimenez
	//@Metodo que imprime las imagenes para su edicion
	function viewImages($page)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM albums WHERE fkAncla = :page');
		$query->bindParam(':page', $page);
		$run = $query->execute();

		if($run == 1)
		{
			$viewHtml = '<table class="tResults tPages searchResults">';
			$viewHtml .= 	'<tr>
								<td>Nombre</td><td>Imagen</td><td>alt Imagen</td><td colspan="2">&nbsp;</td>
							</tr>';
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

				$hide = $_SESSION['rol'] != 3 ? '' : 'style="display:none;"';

				$viewHtml .= '<tr>
									<td>'.$row['name'].'</td>
									<td>'.$row['photo'].'</td>
									<td>'.$row['altPhoto'].'</td>
									<td><a href="#" '.$hide.' data-reveal-id="editImage" class="big-link edimage" data-idimage="'.$row['idPhoto'].'" title="Editar Galeria"><img src="sources/edit-action.png" width="20"></a></td>
									<td><a href="gallery?'.$_SERVER['QUERY_STRING'].'&idImage='.$row['idPhoto'].'" '.$hide.' title="Eliminar Imagenes"><img src="sources/delete-action.png" width="20"></a></td>
							  </tr>';
			}

			$viewHtml .= '</table>';
		}

		return $viewHtml;
		$this->closeBD($bd);
	}


	//@Method::getInfoImage
	//@Autor::Alex Jimenez
	//@Metodo que recupera la informacion de cada imagen de la galeria
	function getInfoImage($idPage)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM albums WHERE idPhoto = :idpage');
		$query->bindParam(':idpage', $idPage);
		$run = $query->execute();

		if($run == 1)
		{
			$row = $query->fetch(PDO::FETCH_ASSOC);
			$response = json_encode(array(
										'rState' => 'succes',
										'attrName' => $row['name'],
										'attrAlt' => $row['altPhoto']));
		}
		else
		{
			$response = json_encode(array('rState' => 'failed', 'message' => 'Ocurrio un problema, por favor intentelo más tarde.'));
		}

		return $response;
		$this->closeBD($bd);
	}


	//@Method::setInfoImage
	//@Autor::Alex Jimenez
	//@Metodo que actuliza la informacion de las imagenes de la galeria
	function setInfoPage($name, $alt, $id)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('UPDATE albums SET name = :name, altPhoto = :alt WHERE idPhoto = :id');
		$run = $query->execute(array(
									'name' => $name,
									':alt' => $alt,
									':id' => $id));

		if($run == 1)
		{
			$response = json_encode(array('state' => 'succes'));
		}
		else
		{
			$response = json_encode(array('state' => 'failed'));
		}

		return $response;
		$this->closeBD($bd);
	}


	//@Method::deleteImage
	//@Autor::Alex Jimenez
	//@Metodo que eliminar la imagen seleccionada
	function deleteImage($id)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('DELETE FROM albums WHERE idPhoto = :id');
		$query->bindParam(':id', $id);
		$run = $query->execute();

		if($run == 1)
		{
			$response = json_encode(array('state' => 'succes'));
		}
		else
		{
			$response = json_encode(array('state' => 'failed'));
		}

		return $response;
		$this->closeBD($bd);
	}


}

?>