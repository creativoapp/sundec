<?php
require_once('BD.php');

//@Class::BRANDS
//@Autor::Alex Jimenez
//@Clase para la gestion/creacion de marcas
class Projects extends BD
{

	private $folderSources;

	function __construct() {
	 	$this->folderSources = 'http://www.sundecdecoracion.com/sources/projects/';
	}


	function listProjects() {

		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM proyectos WHERE estado = 1 ORDER BY creado DESC');	
		$query->execute();

		$response = null;
		if($query->rowCount() > 0) {
			$response = $query->fetchAll();
		}

		return $response;
		$this->closeBD($bd);		
		
	}


	function insert($args) {
		
		$bd = $this->openBD();				
		$query = $bd->prepare('INSERT INTO proyectos (proyecto, miniatura, altimg, contenido, creado) VALUES (:a, :b, :c, :d, :e)');
		$query->bindParam('a', $args[0]);
		$query->bindParam('b', $args[1]);
		$query->bindParam('c', $args[2]);
		$query->bindParam('d', $args[3]);
		$query->bindParam('e', date('Y-m-d'));
		$query->execute();

		$state = $query->rowCount() > 0 ? 'succes' : 'failed';
		$response = json_encode(array('state' => $state));
		
		return $response;
		$this->closeBD($bd);	
	}


	function getProject($id) {

		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM proyectos WHERE MD5(pkproyecto) = :id');
		$query->bindParam(':id', $id);
		$query->execute();

		if($query->rowCount() > 0) {

			$row = $query->fetch(PDO::FETCH_ASSOC);
			$response = json_encode(array(
										'project' => $row['proyecto'],
										'thumb' => $row['miniatura'],
										'txalt' => $row['altimg'],
										'description' => $row['contenido']));
		}
		else
		{
			$response = json_encode(array('rState' => 'failed', 'message' => 'Ocurrio un problema, por favor intentelo más tarde.'));
		}

		return $response;
		$this->closeBD($bd);
	}
	

	function update($args) {
		$bd = $this->openBD();				
		$query = $bd->prepare('UPDATE proyectos SET proyecto = :a, miniatura = :b, altimg = :c, contenido = :d WHERE MD5(pkproyecto) = :id');
		$query->bindParam('a', $args[0]);
		$query->bindParam('b', $args[1]);
		$query->bindParam('c', $args[2]);
		$query->bindParam('d', $args[3]);
		$query->bindParam('id', $args[4]);
		$query->execute();

		$status = $query->rowCount() > 0 ? 'succes' : 'failed';
		$response = json_encode(array('state' => $status));
	

		return $response;
		//return json_encode(array('id' => $id));;
		$this->closeBD($bd);
	}


	function delete($id) {

		$bd = $this->openBD();				
		$query = $bd->prepare('DELETE FROM proyectos WHERE MD5(pkproyecto) = :id');
		$query->bindParam(':id', $id);
		$query->execute();

		$status = $query->rowCount() > 0 ? 'succes' : 'failed';
		$response = json_encode(array('state' => $status));
	
		return $response;
		$this->closeBD($bd);
	}


	//FRONT END
	function viewProjects() {

		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM proyectos WHERE estado = 1 ORDER BY creado DESC');
		$query->execute();

		$response = null;
		if($query->rowCount() > 0) {
			$response = $query->fetchAll();
		}

		return $response;
		$this->closeBD($bd);

	}


}

?>