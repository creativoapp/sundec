<?php
require_once('BD.php');

//@Class::YATES
//@Autor::Alex Jimenez
//@Clase para la gestion/creacion de paginas
class Yates extends BD
{

	private $folderSources;

	function __construct()
	{
	 	$this->folderSources = 'http://www.sundecdecoracion.com/sources/yates/';
	}


	//@Method::listPages
	//@Autor::Alex Jimenez
	//@Metodo que lista las paginas creadas
	function listPages($type)
	{
		$bd = $this->openBD();
		if(!empty($type))
		{
			if(is_numeric($type))
			{
				if($type == 0)
				{
					$filter = $bd->prepare('SELECT * FROM yates, seo WHERE yates.idYacht = seo.idYacht');
				}
				else
				{
					$filter = $bd->prepare('SELECT * FROM yates, seo WHERE yates.idYacht = seo.idYacht AND yates.categoryAcht = :category');
					$filter->bindParam('category', $type);
				}
				
			}
			else
			{
				$filter = $bd->prepare('SELECT * FROM yates, seo WHERE yates.idYacht = seo.idYacht AND seo.urlPage LIKE "%":search"%"');
				$filter->bindParam('search', $type);
			}
			
		}
		else
		{
			$filter = $bd->prepare('SELECT * FROM yates, seo WHERE yates.idYacht = seo.idYacht');
		}
		$filter->execute();

		define('_PAGES', 30);
		if(isset($_GET['page']))
		{
		  	$page = $_GET['page'];
		}
		else
		{
		    $page = 1;
		}
		
		$inicio = ($page - 1) * _PAGES;
		$registros = $filter->rowCount();
		$noPages = ceil($registros / _PAGES);
		
		if(!empty($type))
		{
			if(is_numeric($type))
			{
				if($type == 0)
				{
					$query = $bd->prepare('SELECT * FROM yates, seo WHERE yates.idYacht = seo.idYacht ORDER BY idSeo ASC LIMIT :inicio,'._PAGES.' ');
				}
				else
				{
					$query = $bd->prepare('SELECT * FROM yates, seo WHERE yates.idYacht = seo.idYacht AND yates.categoryAcht = :category ORDER BY idSeo ASC LIMIT :inicio,'._PAGES.' ');
					$query->bindParam('category', $type);
				}
				
			}
			else
			{
				$query = $bd->prepare('SELECT * FROM yates, seo WHERE yates.idYacht = seo.idYacht AND seo.urlPage LIKE "%":search"%" ORDER BY idSeo ASC LIMIT :inicio,'._PAGES.' ');
				$query->bindParam('search', $type);
			}
			
		}
		else
		{
			$query = $bd->prepare('SELECT * FROM yates, seo WHERE yates.idYacht = seo.idYacht ORDER BY idSeo ASC LIMIT :inicio,'._PAGES.' ');
		}
		
		$query->bindParam('inicio', $inicio, PDO::PARAM_INT);
		$query->execute();

		$table = '<table class="tResults tPages searchResults">
				  <tr>
				  		<td>URL</td><td>Titulo</td><td>Precio lista</td><td>Categoria</td><td colspan="2">&nbsp;</td>
				  </tr>';
		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			switch ($row['categoryAcht']) {
				case '1':
					$category = 'Luxury Yachts';
					break;
				case '2':
					$category = 'Fishing Charters';
					break;
				case '3':
					$category = 'Groups Yachts';
				case '4':
					$category = 'Catamarans';
					break;
				case '5':
					$category = 'Wakeboard, Ski & More';
					break;
			
			}
			
			$table .= '<tr>
							<td>'.$row['urlPage'].'</td>
							<td>'.$row['titleYacht'].'</td>
							<td>'.$row['priceYacht'].'</td>
							<td>'.$category.'</td>
							<td><a href="yates?edit='.$row['idYacht'].'" title="Editar Pagina"><img src="sources/edit-action.png" width="20"></a></td>
							<td><a href="yates?delete='.$row['idYacht'].'" title="Eliminar Pagina"><img src="sources/delete-action.png" width="20"></a></td>
					   </tr>';
			}

		$table .= '</table>';

		if($noPages > 1)
			{
			    for($y = 1; $y <= $noPages; $y++ )
			    {
			        if($page == $y)
			        {
			            $table .= '<a class="page currentpag" href="yates?page='.$y.'">'.$y.'</a>';
			        }
			        else
			        {
			            $table .= '<a class="page" href="yates?page='.$y.'">'.$y.'</a>';
			        }
			    }
			}
		$this->closeBD($bd);
		return $table;
		
		
		
	}


	//@Method::getSelectPages
	//@Autor::Alex Jimenez
	//@Metodo que obtiene la lista de paginas segun su categoria
	function getSelectPages($categoria)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM yates WHERE categoryAcht = :categoria');
		$query->bindParam(':categoria', $categoria);
		$run = $query->execute();
		
		if($run == 1)
		{
			$listPages = array();
			while($row = $query->fetch(PDO::FETCH_ASSOC))
			{
				array_push($listPages, $row['titleYacht'] .'*'. $row['idYacht']);
			}
			
		}
		
		return json_encode($listPages);
		$this->closeBD($bd);	
	}


	//@Method::insertPage
	//@Autor::Alex Jimenez
	//@Metodo para la creacion de paginas
	function insertPage($arguments)
	{
		$bd = $this->openBD();
		$query = $bd->prepare('INSERT INTO yates (titleYacht, imageYacht, altThumb, priceYacht, contentYacht, categoryAcht) VALUES (:title, :image, :alt, :price, :content, :category)');
		$run = $query->execute(array(
					':title' => $arguments[0], 
                    ':image' => $arguments[1],
                    ':alt' => $arguments[2],
                    ':price' => $arguments[3],
                    ':content' => $arguments[4],
                    ':category' => $arguments[5]));

		if($run == 1)
		{
			$page = $bd->lastInsertId();
			$seo = $bd->prepare('INSERT INTO seo (urlPage, descriptionPage, idYacht) VALUES (:url, :description, :idyacht)');
			$runseo = $seo->execute(array(
					':url' => $arguments[6], 
                    ':description' => $arguments[7],
                    ':idyacht' => $page));

			if($runseo == 1)
			{
				$response = json_encode(array('state' => 'succes', 'message' => 'Se ha creado la página  correctamente.' ));
			}
			else
			{
				$delete = $bd->prepare('DELETE FROM yates WHERE idYacht = :idyate');
				$delete->bindParam('idyate', $page);
				$delete->execute();
				$response = json_encode(array('state' => 'failed', 'message' => '<strong>Lo sentimos, ocurrio un problema. Por favor intentelo más tarde.</strong>' ));
			}
			
		}
		else
		{
			$response = json_encode(array('state' => 'failed', 'message' => '<strong>Lo sentimos, ocurrio un problema. Por favor intentelo más tarde.</strong>' ));
		}

		return $response;
	    $this->closeBD($bd);
	}


	//@Method::showArticles
	//@Autor::Alex Jimenez
	//@Metodo para impresion de las paginas
	function showArticles($category)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM yates, seo WHERE yates.categoryAcht = :category AND yates.idYacht = seo.idYacht ORDER BY yates.idYacht ASC');
		$query->bindParam(':category', $category);
		$run = $query->execute();
		
		if($run == 1)
		{
			$articles = '';
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

				$clrHtml = strip_tags($row['contentYacht']);
				$destroy = explode(' ', $clrHtml);

				count($destroy) < 30 ? $limite = count($destroy) : $limite = 30;

				$extracto = '';
				for ($i = 0; $i < $limite; $i++) { 
					$extracto .= ' '.$destroy[$i];
				}

				$articles .= '<article class="boxYatch-list">
									<div class="columns four nomrg">
										<a href="'.$row['urlPage'].'">
											<img src="'.$this->folderSources.$row['imageYacht'].'" alt="'.$row['altThumb'].'" />
										</a>
							  		</div>
							  		<div class="columns seven nomrg-r">
										<h2>'.$row['titleYacht'].'</h2>
										<p>'.$extracto.'...</p>
										<span>'.$row['priceYacht'].'</span><a href="'.$row['urlPage'].'" class="read-more">READ MORE</a>
									</div>
									<div class="clr"></div>
							</article>';
			}
			return $articles;
		}
		else
		{
			$response = json_encode(array('state' => 'failed', 'message' => '<strong>Lo sentimos, ocurrio un problema. Por favor intentelo más tarde.</strong>' ));
		}
		
		return $articles;
		$this->closeBD($bd);
		
		
	}


	//@Method::showPage
	//@Autor::Alex Jimenez
	//@Metodo visualizacion de la pagina
	function showPage($url)
	{
		$bd = $this->openBD();				
		$query = $bd->prepare('SELECT * FROM yates, seo WHERE seo.urlPage = :url AND yates.idYacht = seo.idYacht ORDER BY yates.idYacht ASC');
		$query->bindParam(':url', $url);
		$run = $query->execute();
		
		if($run == 1)
		{
			$row = $query->fetch(PDO::FETCH_ASSOC);
			$page = json_encode(array(
									'titulo' => $row['titleYacht'], 
									'contenido' => $row['contentYacht'] ));
		}
		
		return $page;
		$this->closeBD($bd);
		
		
	}


	//@Method::getYates
	//@Autor::Alex Jimenez
	//@Metodo que obtine la información de una pagina para su edicion
	function getYates($id)
	{
		$bd = $this->openBD();
		$query = $bd->prepare('SELECT * FROM yates, seo WHERE yates.idYacht = :id AND yates.idYacht = seo.idYacht');
		$query->bindParam('id', $id);
		$exe = $query->execute();

		if($exe == 1){ 
			
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
				$response = json_encode(array(
										'title' => $row['titleYacht'],
										'url' => $row['urlPage'],
										'precio' => $row['priceYacht'],
										'categoria' => $row['categoryAcht'],
										'alt' => $row['altThumb'],
										'content' => $row['contentYacht'])); 	
			}  
		}
		else {
			$response = json_encode(array('state' => 'failed'));
		}

		return $response;
		$bd->closeBD();
	}

	//@Method::delPage
	//@Autor::Alex Jimenez
	//@Metodo para eliminar una pagina
	function deletePage($id)
	{
		$bd = $this->openBD();
		$query = $bd->prepare('DELETE FROM Y, S USING yates AS Y INNER JOIN seo AS S WHERE Y.idYacht = :id AND Y.idYacht = S.idYacht');
		$query->bindParam('id', $id);
		$exe = $query->execute();

		if($exe == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}


	//@Method::modificPage
	//@Autor::Alex Jimenez
	//@Metodo para modificar una pagina
	function modificPage($arguments)
	{
		$bd = $this->openBD();
		if(!empty($arguments[1]))
		{			
			$query = $bd->prepare('UPDATE yates, seo SET yates.titleYacht = :title, yates.imageYacht = :image, yates.altThumb = :alt, yates.priceYacht = :price, yates.contentYacht = :content, yates.categoryAcht = :category, seo.urlPage = :url WHERE yates.idYacht = :id AND yates.idYacht = seo.idYacht');
			$exe = $query->execute(array(
										'title' => $arguments[0],
										'image' => $arguments[1],
										'alt' => $arguments[2],
										'price' => $arguments[3],
										'content' => $arguments[4],
										'category' => $arguments[5],
										'url' => $arguments[6],
										'id' => $arguments[7]));
		}
		else
		{
			$query = $bd->prepare('UPDATE yates, seo SET yates.titleYacht = :title, yates.altThumb = :alt, yates.priceYacht = :price, yates.contentYacht = :content, yates.categoryAcht = :category, seo.urlPage = :url WHERE yates.idYacht = :id AND yates.idYacht = seo.idYacht');
			$exe = $query->execute(array(
										'title' => $arguments[0],
										'alt' => $arguments[2],
										'price' => $arguments[3],
										'content' => $arguments[4],
										'category' => $arguments[5],
										'url' => $arguments[6],
										'id' => $arguments[7]));
		}
		
		

		if($exe == 1){ $response = json_encode(array('state' => 'succes', )); }  else {
			
			$response = json_encode(array('state' => 'failed', ));
		}

		return $response;
		$bd->closeBD();
	}

	


}

?>