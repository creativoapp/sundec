<?php
	require_once('BD.php');
	class Newsletter extends BD
	{
		public $mail;
		public $date;

		function __construct($pmail)
		{
			$this->mail = $pmail;
			$this->date = date('Y-m-d');
		}
		
		function addNewsletter()
		{

			$bd = $this->openBD();
			$query = $bd->prepare('INSERT INTO newsletter (mailNews, dateSuscribe) VALUES (:mail, :data)');
			$query->bindParam('mail', $this->mail);
			$query->bindParam('data', $this->date);
			
			$run = $query->execute();
			if($run == 1)
			{
				$response = json_encode(array('state' => 'succes', 'msgbody' => 'Te has suscrito exitosamente a nuestro newsletter para recibir todas nuestras promociones y noticias en tu e-mail.' ));
				return $response;
			}
			else
			{
				$response = json_encode(array('state' => 'failed', 'msgbody' => '<strong>Lo sentimos.</strong>Ocurrio un problema, por favor intentelo más tarde.' ));
				return $response;
			}

			$this->closeBD($bd);
		}



		


	}
?>