<?php require('components/header.php'); ?>

<section class="is-view is-view-brands container">
    
    <div class="columns">
        <div class="column is-half is-overview">
            <h1>¿NUESTRAS MARCAS?</h1>
            <p>Sundec Decoración en Cancún, es una empresa mexicana establecida en la ciudad de Cancún, Q. Roo, dedicada a la 
            venta de productos de alta calidad que cubran las necesidades más exigentes para el mercado de la decoración 
            residencial, comercial y hotelera, buscando siempre la satisfacción plena del usuario final, a través de dicha 
            calidad y un excelente servicio.</p>
            <p>Sundec Decoración cuenta con varios Productos y Servicios para la Decoración de su casa u Hotel en Cancún, 
            entre los cuales destacan sus Cortinas y Persianas que pueden ser colocadas en cualquier establecimiento de Cancún. 
            Cuentan con Persianas anticiclónicas para ser instaladas cuanddo lo deseen.</p>
            <p>Entre nuestras especialidades de Decoración, contamos con lámparas, candiles, pisos de diferentes materiales, elegantes toldos, alfombras para todo tipo de espacios y una gran variedad de persianas y cortinas. Solo distribuimos productos de alta calidad, por lo que las marcas son sometidas a una evaluación y son seleccionadas cuidadosamente para ofrecerles lo mejor a nuestros clientes.</p>
        </div>

        <div class="column is-half">
            <img src="/assets/img/catalogo-decoracion-en-cancun.jpg">
        </div>
    </div>

    <div class="columns is-multiline">
        <?php 
		$brandsModel = new Brands();
		$brands = json_decode($brandsModel->getBrands());

		if($brands != null) {
		    foreach($brands as $brand) { ?>

                <div class="column is-one-quarter isBrand">
                    <a href="<?= $brand->{'link'}; ?>" title="<?= $brand->{'title'}; ?>"><img src="/sources/marcas/<?= $brand->{'brandimg'};?>" alt="<?= $brand->{'alt'}; ?>"></a>
                    <h2><a href="<?= $brand->{'link'}; ?>" title="<?= $brand->{'title'}; ?>"><?= $brand->{'brand'}; ?></a></h2>
                    <p><?= $brand->{'description'}; ?></p>
                    <a href="<?= $brand->{'link'}; ?>" class="is-link" title="<?= $brand->{'title'}; ?>">Galería <i class="fas fa-angle-right"></i></a>
				</div>

            <?php } 
        } ?>
    </div>

</section>

<?php require('components/footer.php'); ?>