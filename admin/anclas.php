<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        <section class="topControlls">
            <div class="icon-add action-addGallery big-link" data-reveal-id="loadGallery"><img src="<?php echo $path.'admin/sources/add.png'; ?>" /></div>
            <a href="gallery" class="action-addGallery">SUBIR FOTOGRAFIAS</a>
            
            <div class="icon-info action-addGallery" style="margin-left:15px;"><img src="<?php echo $path.'admin/sources/INFO.png'; ?>" /></div>
            <a href="anclas" class="action-addGallery">ADMINISTRAR ANCLAS</a>
            <div class="clr"></div>
        </section>
        <section class="viewDash">
            
            <?php 
                $catalog = new Catalog();
                
            ?>

            <h1>Gestion de anclas</h1>
            <?php if(!isset($_GET['edit'])){ ?> 
            <form name="frm-addAncla" id="frm-addAncla" action="anclas" method="post" class="searchResults">
                <fieldset>
                    <label>Categoria</label>
                    <?php echo $catalog->frmSlctCats(null); ?>
                </fieldset>
                <fieldset>
                    <label>Ancla</label>
                    <input type="text" name="anc-name" id="anc-name" />
                    <?php if($_SESSION['rol'] != 3) { ?>
                    <input type="submit" name="anc-addAction" id="anc-addAction" value="AÑADIR" />
                    <?php } ?>
                </fieldset>
                <div class="clr"></div>
            </form>
            <?php 
                }
                else { 

                $getAncla = json_decode($catalog->getAncla($_GET['edit']));
                $pcategory = $getAncla->{'fk'};    
            ?>


            <form name="frm-updAncla" id="frm-updAncla" action="anclas" method="post" class="searchResults">
                <fieldset>
                    <label>Categoria</label>
                    <?php echo $catalog->frmSlctCats($pcategory); ?>
                </fieldset>
                <fieldset>
                    <label>Ancla</label>
                    <input type="text" name="anc-updname" id="anc-updname" value="<?php echo $getAncla->{'ancla'}; ?>" />
                    <input type="hidden" name="anc-id" id="anc-id" value="<?php echo $getAncla->{'id'}; ?>" />
                    <input type="submit" name="anc-addAction" id="anc-addAction" value="AÑADIR" />
                </fieldset>
                <div class="clr"></div>
            </form>
                
            <?php    
                }
                echo $catalog->viewAnclas();
            ?>
               

            <?php
                //@Controller::CREAR ANCLA
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario ycrea el ancla o subalbum                               
                if(isset($_POST['listCtalog']) && isset($_POST['anc-name'])){

                    $insert = json_decode($catalog->insertAncla($_POST['listCtalog'], $_POST['anc-name']));
                    if($insert->{'state'} == 'succes'){
                        header('Location:anclas?addok=true');
                    }
                }


                if(isset($_GET['addok']))
                {
                    echo '<div class="msg-success">Se añadio el ancla correctamente.</div>';
                    header("Refresh: 3; URL=anclas");
                }


                //@Controller::DESACTIVAR ANCLAS
                //@Autor::Alex Jimenez
                //@Recibe id del ancla para desactivar
                if(isset($_GET['delete']))
                {
                    $delete = json_decode($catalog->disabledAN($_GET['delete']));
                    if($delete->{'state'} == 'succes'){
                        header('Location:anclas?delok=true');
                    }
                }

                if(isset($_GET['delok']))
                {
                    echo '<div class="msg-success">Se elimino el ancla correctamente.</div>';
                    header("Refresh: 3; URL=anclas");
                }



                //@Controller::MODIFICAR ANCLA
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y modifica ancla o subalbum                          
                if(isset($_POST['anc-id']) && isset($_POST['anc-updname'])){

                    $update = json_decode($catalog->updateAncla($_POST['listCtalog'], $_POST['anc-updname'], $_POST['anc-id']));
                    if($update->{'state'} == 'succes'){
                        header('Location:anclas?updok=true');
                    }
                }


                if(isset($_GET['updok']))
                {
                    echo '<div class="msg-success">Se modifico el ancla correctamente.</div>';
                    header("Refresh: 3; URL=anclas");
                }


            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>