<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        <section class="topControlls">
            <div class="icon-info"><img src="<?php echo $path.'admin/sources/info.png'; ?>" /></div>
            <a href="#">MODIFIQUE SU INFORMACION</a>
            <div class="clr"></div>
        </section>
        <section class="viewDash">
            <h1>Configuración</h1>
            
            <?php
                $setting = new Setting();
                $config = json_decode($setting->getDates()); 
            ?>

            <form name="frm-updSetting" id="frm-updSetting" method="post">
                <fieldset>
                    <span><strong>HEADER</strong></span>
                    <label>E-mail</label>
                    <input type="text" name="set-mailHeader" id="set-mailHeader" value="<?php echo $config->{'mailH'}; ?>" />
                    <label>Teléfono</label>
                    <input type="text" name="set-phoneHeader" id="set-phoneHeader" value="<?php echo $config->{'phoneH'}; ?>" />
                </fieldset><br><br>
                <fieldset>
                    <!--<span><strong>FOOTER</strong></span>
                    <label>Dirección</label>
                    <input type="text" name="set-addresFooter" id="set-addresFooter" value="<?php echo $config->{'addresF'}; ?>" />
                    <label>E-mail</label>
                    <input type="text" name="set-mailFooter" id="set-mailFooter" value="<?php echo $config->{'mailF'}; ?>" />
                    <label>Teléfono</label>
                    <input type="text" name="set-phoneFooter" id="set-phoneFooter" value="<?php echo $config->{'phoneF'}; ?>" />-->
                    <input type="text" name="set-idSetting" id="set-idSetting" value="<?php echo $config->{'id'}; ?>" hidden="hidden" />
                    <?php if($_SESSION['rol'] != 3) { ?>
                    <input type="submit" name="set-btnUpd" id="set-btnUpd" value="ACTUALIZAR">
                    <?php } ?>
                </fieldset>
            </form>
            


            <?php
                //@Controller::ACTUALIZAR CONFIGURACION
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y procesa la actualizacion de los datos de configuracion
                if(isset($_POST['set-btnUpd']))
                {
                    $arguments = array(
                                    $_POST['set-mailHeader'],
                                    $_POST['set-phoneHeader'],
                                    $_POST['set-addresFooter'],
                                    $_POST['set-mailFooter'],
                                    $_POST['set-phoneFooter'],
                                    $_POST['set-idSetting']);
                    $update = json_decode($setting->setDates($arguments));
                    if($update->{'state'} == 'succes')
                    {
                        header('Location:setting?updok=true');
                    }
                }

                if(isset($_GET['updok']))
                {
                    echo '<div class="msg-success">Se actulizo la información exitosamente</div>';
                    header("Refresh: 3; URL=setting");
                }

                
            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>