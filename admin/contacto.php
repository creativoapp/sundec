<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        <section class="topControlls">
            <div class="icon-info"><img src="<?php echo $path.'admin/sources/info.png'; ?>" /></div>
            <a href="#">MODIFIQUE SU INFORMACION</a>
            <div class="clr"></div>
        </section>
        <section class="viewDash">
            <h1>Página Contacto</h1>
            
            <?php
                $setting = new Setting();
                $config = json_decode($setting->getContacto()); 
            ?>

            <form name="frm-updContacto" id="frm-updContacto" method="post">
                <fieldset>
                    <span><strong>MAPA</strong></span>
                    <label>Latitud</label>
                    <input type="text" name="cont-latCont" id="cont-latCont" value="<?php echo $config->{'latitud'}; ?>" />
                    <label>Longitud</label>
                    <input type="text" name="cont-longCont" id="cont-longCont" value="<?php echo $config->{'longitud'}; ?>" />
                </fieldset>
                <fieldset>
                    <span><strong>SIDEBAR</strong></span>
                    <label>Dirección</label>
                    <input type="text" name="cont-addres1" id="cont-addres1" value="<?php echo $config->{'addres1'}; ?>" />
                    <input type="text" name="cont-addres2" id="cont-addres2" value="<?php echo $config->{'addres2'}; ?>" />
                    <input type="text" name="cont-addres3" id="cont-addres3" value="<?php echo $config->{'addres3'}; ?>" />
                    <label>E-mail</label>
                    <input type="text" name="cont-mail" id="cont-mail" value="<?php echo $config->{'mailContacto'}; ?>" />
                    <label>Teléfono</label>
                    <input type="text" name="cont-phone" id="cont-phone" value="<?php echo $config->{'phoneContacto'}; ?>" />
                </fieldset>
                <fieldset>
                    <span><strong>FORMULARIO</strong></span>
                    <label>E-mail destino</label>
                    <input type="text" name="cont-mailform" id="cont-mailform" value="<?php echo $config->{'mailForm'}; ?>" />
                    <input type="text" name="cont-id" id="cont-id" hidden="hidden" value="<?php echo $config->{'id'}; ?>" />
                    <?php if($_SESSION['rol'] != 3) { ?>
                    <input type="submit" name="cont-btnUpd" id="cont-btnUpd" value="ACTUALIZAR">
                    <?php } ?>
                </fieldset>
            </form>
            


            <?php
                //@Controller::ACTUALIZAR CONTACTO
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y procesa la actualizacion de los datos de la pagina de contacto
                if(isset($_POST['cont-btnUpd']))
                {
                    $arguments = array(
                                    $_POST['cont-addres1'],
                                    $_POST['cont-addres2'],
                                    $_POST['cont-addres3'],
                                    $_POST['cont-mail'],
                                    $_POST['cont-phone'],
                                    $_POST['cont-mailform'],
                                    $_POST['cont-latCont'],
                                    $_POST['cont-longCont'],
                                    $_POST['cont-id']);
                    $update = json_decode($setting->setContacto($arguments));
                    if($update->{'state'} == 'succes')
                    {
                        header('Location:contacto?updok=true');
                    }
                }

                if(isset($_GET['updok']))
                {
                    echo '<div class="msg-success">Se actulizo la información exitosamente</div>';
                    header("Refresh: 3; URL=contacto");
                }

                
            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>