<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        <section class="topControlls">
            <div class="icon-add action-addMarca"><img src="<?php echo $path.'admin/sources/add.png'; ?>" /></div>
            <a href="#" class="action-addMarca">AGREGAR MARCA</a>
            <div class="clr"></div>
        </section>
        <section class="viewDash">
            
            <h1>Gestion de Marcas</h1>
            
            <?php
                $brand = new Brands(); 
                echo $brand->listBrands();
                
            ?>
            

            <div id="editMarca" class="reveal-modal">
                <h1>Editar información de la marca</h1>
                <form name="loadGallery" id="loadGallery" action="" method="post">
                    <fieldset>
                    <label>Marca</label>
                    <input type="text" name="mar-updname" id="mar-updname" /><br>
                    <label>alt Logotipo</label>
                    <input type="text" name="mar-updalt" id="mar-updalt" />
                    <div class="maskLinkBrand">
                        <label>Enlace a</label>
                        <small>www.sundecdecoracion.com/</small><input type="text" name="mar-updurl" id="mar-updurl" />
                        <div class="clr"></div>
                    </div>
                    <label>title Link</label>
                    <input type="text" name="mar-updtitle" id="mar-updtitle" />
                    <label>Descripción de la marca</label>
                    <textarea name="mar-upddescr" id="mar-upddescr" rows="3"></textarea>
                    <input type="hidden" name="mar-updid" id="mar-updid" />
                </fieldset>
                <fieldset>
                    <input type="submit" name="mar-btnUpdate" id="mar-btnUpdate" value="ACTUALIZAR">
                </fieldset>
                </form>
                <a class="close-reveal-modal">&#215;</a>
            </div>


            <form name="frm-addMarcas" id="frm-addMarcas" method="post" enctype="multipart/form-data">
                <fieldset>
                    <label>Marca</label>
                    <input type="text" name="mar-name" id="mar-name" /><br>
                    <label>Logotipo <small>300 - 300</small></label>
                    <input type="file" name="mar-logo" id="mar-logo" />
                    <label>alt Logotipo</label>
                    <input type="text" name="mar-alt" id="mar-alt" />
                    <div class="maskLinkBrand">
                        <label>Enlace a</label>
                        <small>www.sundecdecoracion.com/</small><input type="text" name="mar-url" id="mar-url" />
                        <div class="clr"></div>
                    </div>
                    <label>title Link</label>
                    <input type="text" name="mar-title" id="mar-title" />
                    <label>Descripción de la marca</label>
                    <textarea name="mar-descr" id="mar-descr" rows="3"></textarea>
                </fieldset>
                <fieldset>
                    <?php if($_SESSION['rol'] != 3) { ?>
                    <input type="submit" name="mar-btnAdd" id="mar-btnAdd" value="PUBLICAR">
                    <?php } ?>
                </fieldset>
            </form>


            <?php
                
                //@Controller::CREAR MARCA
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y genera la creacion de marcas
                if(isset($_POST['mar-btnAdd']) && $_FILES['mar-logo']['tmp_name'] != '')
                {   
                   
                    define('_BRANDSpath', $_SERVER['DOCUMENT_ROOT'].'/sources/marcas/');

                    $link = empty($_POST['mar-url']) ? NULL : $_POST['mar-url'];
                    $title = empty($_POST['mar-title']) ? NULL : $_POST['mar-title'];
                    $alt = empty($_POST['mar-alt']) ? NULL : $_POST['mar-alt'];
                    $descr = empty($_POST['mar-descr']) ? NULL : $_POST['mar-descr'];

                    $logo = $_FILES['mar-logo']['name'];
                    $args = array($_POST['mar-name'], $logo, $link, $title, $alt, $descr);

                    if(move_uploaded_file($_FILES['mar-logo']['tmp_name'], _BRANDSpath.$logo))
                    {   
                        $insert = json_decode($brand->insertMarca($args));
                        if($insert->{'state'} == 'succes')
                        {
                            header('Location:brands?insert=ok');
                        }
                    } 

                        
                }


                if(isset($_GET['insert']) && $_GET['insert'] == 'ok')
                {
                    echo '<div class="msg-success">Se creo una nueva marca correctamente</div>';
                    header("Refresh: 3; URL=brands");
                }


                
                //@Controller::BORRA MARCA
                //@Autor::Alex Jimenez
                //@Recibe dato id marca por get para eliminar la marca
                if(isset($_GET['idMarca']))
                {
                    $delMarca = json_decode($brand->deleteBrand($_GET['idMarca']));
                    if($delMarca->{'state'} == 'succes')
                    {
                        header('Location:brands');
                    }
                }



                //@Controller::EDITAR MARCA
                //@Autor::Alex Jimenez
                //@Recibe dato id marca por post para editar la marca
                if(isset($_POST['mar-btnUpdate']))
                {
                    $updMarca = json_decode($brand->editBrand($_POST['mar-updname'], $_POST['mar-updalt'], $_POST['mar-updurl'], $_POST['mar-updtitle'], $_POST['mar-updid'], $_POST['mar-upddescr']));
                    if($updMarca->{'state'} == 'succes')
                    {
                        header('Location:brands?editbrand=ok');
                    }
                    //echo $updMarca->{'id'};
                }


                if(isset($_GET['editbrand']) && $_GET['editbrand'] == 'ok')
                {
                    echo '<div class="msg-success">Se modificaron los datos de la marca correctamente</div>';
                    header("Refresh: 3; URL=brands");
                }
                
            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>