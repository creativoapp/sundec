<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        
        <!--<section class="topControlls">
            <div class="icon-add action-addMarca"><img src="<?php echo $path.'admin/sources/add.png'; ?>" /></div>
            <a href="#" class="action-addMarca">AGREGAR PROYECTO</a>
            <div class="clr"></div>
        </section>-->

        <section class="viewDash">
            
            <h1>Publicar Catalogo</h1>
            <?php $file = new Catalog(); ?>          

            <form name="frmNewPdf" id="frmNewPdf" method="post" action="" enctype="multipart/form-data">
                <fieldset>
                    <label>Titulo</label>
                    <input type="text" name="inpCatalogo" id="inpCatalogo" placeholder="Titulo del Catalogo" /><br>
                    <label>Catalogo</label>
                    <input type="file" name="inpFile" id="inpFile" />
                    <label>Categoria</label>
                    <select name="inpModule" id="inpModule">
                        <option value="0">Seleccione una Categoria</option>
                        <?php
                            $list = $file->listModules();
                            foreach ($list as $option) { ?>
                                <option value="<?php echo $option['pkmodulo'] ?>"><?php echo $option['modulo']; ?></option>
                        <?php        
                            }
                        ?>
                    </select>
                </fieldset>
                <fieldset>
                    <?php if($_SESSION['rol'] != 3) { ?>
                    <input type="submit" name="btnAddFile" id="btnAddFile" value="PUBLICAR">
                    <?php } ?>
                </fieldset>
            </form>


            <?php
                
                //@Controller::CREAR PROYECTO
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y genera la creacion de proyectos
                $catalogo = new Catalog();
                if(isset($_POST['inpCatalogo']) && $_FILES['inpFile']['tmp_name'] != '') {   
                   
                    $folder = $_SERVER['DOCUMENT_ROOT'].'/sources/catalogo/pdf/';

                    $title = filter_input(INPUT_POST, 'inpCatalogo', FILTER_SANITIZE_STRING);
                    $module = filter_input(INPUT_POST, 'inpModule', FILTER_SANITIZE_NUMBER_INT);

                    $thumb = $_FILES['inpFile']['name'];
                    $args = array($title, $thumb, $module);

                    if(move_uploaded_file($_FILES['inpFile']['tmp_name'], $folder.$thumb)) {   
                        $action = json_decode($catalogo->insertPdf($args));

                        if($action->{'state'} == 'succes') { header('Location:nuevo-catalogo-pdf?insert=ok'); }
                    } 

                        
                }


                if(isset($_GET['insert']) && $_GET['insert'] == 'ok') {
                    echo '<div class="msg-success">Catalogo cargado exitosamente</div>';
                    header("Refresh: 2; URL=catalogo-pdf");
                }

            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>