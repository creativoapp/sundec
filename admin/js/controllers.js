$(document).ready(function(){

	//@Controller::LOGIN
	//@Autor::Alex Jimenex
	$('.loading').hide();
	$('#btn-login').on('click', function(event){
		event.preventDefault();

		var user = $('#username').val();
		var pass = $('#password').val();

		if (user == null || user.length == 0 || /^\s+$/.test(user)) {
			$('#username').addClass('required');
			$('#username').focus();
		}
		else
		{
			if (pass == null || pass.length == 0 || /^\s+$/.test(pass)) {
				$('#password').addClass('required');
				$('#password').focus();
			}
			else
			{
				loginAdmin(user, pass);
			}
		}

	});

	$('#username, #password').on('keyup', function(){
		if($(this).hasClass('required'))
		{
			$(this).removeClass('required');
		}
	});

	//@Controller::NAVIGATION
	//@Autor::Alex Jimenez
	var height = $(window).height() - 40;
	var width = $(document).width() - 300;
	$('.navigation').css('height', height);
	$('.rightPanel').css('width', width);
	$('.contentsbnav').hide();

	$('.subnav').hover(
		function() {
		    $('.contentsbnav').slideDown('normal');
		}, function() {
		    $('.contentsbnav').slideUp('normal');
		}
	);

	$('.contentsbnav').hover(
		function(){

		},
		function(){
			$('.contentsbnav').slideUp('normal');
		}
	);


	//@Controller::SLIDER
	//@Autor::Alex Jimenez
	$('.slr-linkHide').hide();
	$('.form-addSlider').hide();
	var slr_check = false;

	$('.action-addslider').on('click', function(event){
		event.preventDefault();

		$('.form-addSlider').fadeIn('normal');

	});

	$('#slr-link').on('click', function(){
		if(slr_check == false)
		{
			$('.slr-linkHide').slideDown('normal');
			slr_check = true;
		}
		else
		{
			$('.slr-linkHide').slideUp('normal');
			slr_check = false;
		}
	});

	$('#slr-btnAdd').on('click', function(event){
		event.preventDefault();

		var file = $('#slr-image').val();
		var orden = $('#slr-order').val();

		if (file == null || file.length == 0 || /^\s+$/.test(file)) {
			alert('Por favor elija una imagen para publicar');
		}
		else
		{
			if (orden == null || orden.length == 0 || /^\s+$/.test(orden)) {
				alert('Por favor determine el orden de impresión');
			}
			else
			{
				$('#frm-addSlider').submit();
			}
		}
	});

	$('.slr-modOrder').on('focusout', function(){
		var almacen = parseInt($(this).val());
		var slider = $(this).data('idslider');
		
		setOrderSlider(almacen, slider);
	});


	$('.slider').on('click', function(event){
		event.preventDefault();

		var idslider = $(this).data('idslider');
		$('#slr-updid').val(idslider);
		getInfoSlider(idslider);
	});


	//@Controller::YATES
	//@Autor::Alex Jimenez
	$('#replace-url, #cat-url, #frm-addYates, #frm-addCate').hide();
	$('#cat-name').on('keyup', function(){
		var trunc = $(this).val().toLowerCase();
		//var url = trunc.replace(' ', '-');

		trunc = trunc.trim();
		var destroy = trunc.split(' ');
		var url = '';
		for (var i = 0; i < destroy.length; i++) {
			if(i == 0)
			{
				url = destroy[i];
			}
			else
			{
				url = url + '-' + destroy[i];
			}
		}
		$('#cat-url').val(url);
		$('#url').text(url);
	});

	$('#cat-city').on('change', function(){
		var city = $(this).find(':selected').data('url').trim();
		if (city == '/cancun') {
			city = '';
		}
		var url = 'http://www.sundecdecoracion.com' + city + '/catalogo/';
		$('#sw-link-ct').text(url);
	});

	$('#cat-name').on('focusout', function(){
		var urlpage = $(this).val();
		if (urlpage == null || urlpage.length == 0 || /^\s+$/.test(urlpage)) { } else {$('#replace-url').fadeIn('normal');}
	});

	$('#replace-url').on('click', function(event){
		event.preventDefault();

		$('#cat-url').fadeIn('normal');
		$('#cat-url').focus();
	});

	$('.action-addpage').on('click', function(event){
		event.preventDefault();

		$('.searchResults').fadeOut('normal', function(){
			$('#frm-addYates').fadeIn('normal');
			$('#frm-addCate').fadeIn('normal');
		})
		
	});

	$('#frm-addCate').on('submit', function(e){
		//e.preventDefault();
		var val = true;
		if ($('#cat-profile').val().trim() == '') {
			$('#cat-profile').focus();
			alert('Debes insertar una imagen');
			val = false;
		}
		if ($('#cat-url').val().trim() == '') {
			$('#cat-url').focus();
			val = false;
		}
		if ($('#cat-name').val().trim() == '') {
			$('#cat-name').focus();
			val = false;
		}

		$('#cat-content').text(editor.e.activeElement.innerHTML);
		//console.log(editor.e.activeElement.innerHTML);
		
		return val;
	});

	$('#frm-updCate').on('submit', function(e){
		var val = true;
		if ($('#cat-updurl').val().trim() == '') {
			$('#cat-updurl').focus();
			val = false;
		}
		if ($('#cat-updname').val().trim() == '') {
			$('#cat-updname').focus();
			val = false;
		}

		$('#cat-updContent').text(editor.e.activeElement.innerHTML);
		
		return val;
	});

	$('#yat-btnAdd').on('click', function(event){
		event.preventDefault();

		var title = $('#yat-titulo').val();
		var url = $('#yat-url').val();
		var category = $('#yat-category').val();	

		if (title == null || title.length == 0 || /^\s+$/.test(title)) {
			alert('No puede crear una página sin definir un titulo');
		}
		else
		{
			if (url == null || url.length == 0 || /^\s+$/.test(url)) {
				alert('No puede crear una página sin definir una url');
			}
			else
			{
				if (category == 0) {
					alert('Por favor elija una categoria de destino para la página a crear');
				}
				else
				{
					$('#frm-addYates').submit();
				}
			}
		}
	});


	//@Controller::SEO
	//@Autor::Alex Jimenez
	$('.seo').on('click', function(event){
		event.preventDefault();

		var idseo = $(this).data('idseo');
		$('#seo-id').val(idseo);

		getSeo(idseo);

	});


	$('#seo-btnUpd').click(function(event){
		event.preventDefault();

		var idUpdate = $('#seo-id').val();
		var title = $('#seo-title').val();
		var keywords = $('#seo-keywords').val();
		var description = $('#seo-description').val();

		setSeo(title, keywords, description, idUpdate);

	});


	//@Controller::GALERIAS
	//@Autor::Alex Jimenez
	$('#modaList').live('change', function(){
		var categoria = $('#modaList option:selected').val();
		
		loadPages(categoria);

	});


	$('.edimage').on('click', function(event){
		event.preventDefault();

		var page = $(this).data('idimage');
		$('#gal-editIdImage').val(page);
		imageEdit(page);
		
	});



	//@Controller::MARCAS
	//@Autor::Alex Jimenez
	$('#frm-addMarcas').hide();

	$('.action-addMarca').on('click', function(event){
		event.preventDefault();

		$('#frm-addMarcas').fadeIn('last');
	});


	$('.edimarc').on('click', function(event){
		event.preventDefault();

		var idMarca = $(this).data('idmarca');
		$('#mar-updid').val(idMarca);
		getInfoMarcas(idMarca);

	});

	//@Controller::PROYECTOS
	//@uthor::Alex Jimenez
	$('#btnAddProject').on('click', function(event){
		event.preventDefault();

		var project = {
			'title' : $('#inpProject').val(),
			'image' : $('#inpThumbnail').val(),
			'description' : $('#inpDescription').val()
		};

		if (project.title == null || project.title.length == 0 || /^\s+$/.test(project.title)) {
			alert('Es necesario asignar un nombre de Proyecto');
			$('#inpProject').focus();
			return false;
		}

		if (project.image == null || project.image.length == 0 || /^\s+$/.test(project.image)) {
			alert('No se ha seleccionado una imagen destacada para el Proyecto');
			return false;
		}	

		if (project.description == null || project.description.length == 0 || /^\s+$/.test(project.description)) {
			alert('Es necesario destacar una descripción para el Proyecto');
			$('#inpDescription').focus();
			return false;
		}

		$('#frmNewProject').submit();

	});


	$('.editProject').on('click', function(event){
		event.preventDefault();

		var project = $(this).data('idproject');
		$('#inpUpdate').val(project);
		getProject(project);

	});

	//@Controller::ARCHIVOS
	//@uthor::Alex Jimenez
	$('#btnAddFile').on('click', function(event){
		event.preventDefault();

		var file = {
			'name' : $('#inpCatalogo').val(),
			'temp' : $('#inpFile').val(),
			'module' : $('#inpModule').val()
		};

		if (file.name == null || file.name.length == 0 || /^\s+$/.test(file.name)) {
			alert('Es necesario asignarle un titulo al Catalogo');
			$('#inpCatalogo').focus();
			return false;
		}

		if (file.temp == null || file.temp.length == 0 || /^\s+$/.test(file.temp)) {
			alert('No se ha seleccionado ningun documento');
			$('#inpFile').focus();
			return false;
		}

		if (file.module == null || file.module.length == 0 || /^\s+$/.test(file.module) || file.module == 0) {
			alert('Es necesario asociarle una categoría al Catalogo');
			$('#inpModule').focus();
			return false;
		}

		$('#frmNewPdf').submit();

	});

	$('.editFile').on('click', function(event){
		event.preventDefault();

		var catalogo = $(this).data('idfile');
		$('#inpUpdate').val(catalogo);
		getCatalogo(catalogo);

	});

	//CATEGORIAS CATALOGOS PDF
	$('#btnCreateModule').on('click', function(event){
		event.preventDefault();

		var module = $('#inpCreateModule').val();
		if (module == null || module.length == 0 || /^\s+$/.test(module)) {
			alert('Es necesario ingresar un nombre de Categoria');
			$('#inpCreateModule').focus();
			return false;
		}

		createModule(module);
	});

	$('.deleteModule').on('click', function(event){
		event.preventDefault();

		var pkmodule = $(this).data('delmod');
		deleteModule(pkmodule);
	});

	$('.editModule').on('click', function(event){
		event.preventDefault();

		var pkmodule = $(this).data('editmod');
		var module = $(this).siblings('.inpInlineModulo').val();
		editModule(module, pkmodule);
	});

	//@Controller::PROMOCIONES
	//@Autor::Alex Jimenez
	$('#frm-addPromo').hide();

	$('.action-addPromo').on('click', function(event){
		event.preventDefault();

		$('#frm-addPromo').fadeIn('last');
	});


	$('.promo').on('click', function(event){
		event.preventDefault();

		var idpromo = $(this).data('idpromo');
		$('#pro-updid').val(idpromo);
		getInfoPromo(idpromo);
	});


});