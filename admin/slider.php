<?php include('includes/header.php'); ?>
    <section class="rightPanel">
        <section class="topControlls">
            <div class="icon-add action-addslider"><img src="<?php echo $path.'admin/sources/add.png'; ?>" /></div>
            <a href="#" class="action-addslider">NUEVO SLIDER</a>
            <div class="clr"></div>
        </section>
        <section class="viewDash">
            <h1>Slider Control</h1>
            <?php    
                $slider = new Slider();
                echo $slider->tableSlider();
            ?>
            
            <div id="myModal" class="reveal-modal">
                <h1>Editar Slider</h1>
                <form name="form-editSlider" id="form-editSlider" action="" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <label>Banner slider <small>960 - 450</small></label>
                        <input type="file" name="slr-updslider" id="slr-updslider" />
                        <label>Link a</label>
                        <input type="type" name="slr-updlink" id="slr-updlink" />
                        <div class="clr"></div>
                        <label>alt Banner</label>
                        <input type="text" name="slr-updalt" id="slr-updalt" />
                        <label>title Link</label>
                        <input type="text" name="slr-updtitle" id="slr-updtitle" /> 
                    </fieldset>
                    <fieldset>
                        <input type="hidden" name="slr-updid" id="slr-updid" />
                        <input type="submit" name="slr-btnUpd" id="slr-btnUpd" value="PUBLICAR">
                    </fieldset>
                </form>
                <a class="close-reveal-modal">&#215;</a>
            </div>

            <div class="form-addSlider">
                <h1>Agregar nuevo banner</h1>
                <form name="frm-addSlider" id="frm-addSlider" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <label>Imagen <small>960 - 450 Medida obligatoria</small></label>
                        <input type="file" name="slr-image" id="slr-image" />
                        <!--<label>Texto</label>
                        <input type="text" name="slr-texto" id="slr-texto" />-->
                        <label>alt Image slider</label>
                        <input type="text" name="slr-alt" id="slr-alt" />
                    </fieldset>
                    <fieldset>
                        <label class="lbl-link">Link</label>
                        <input type="checkbox" name="slr-link" id="slr-link" />
                        <div class="clr"></div>
                        <label class="slr-linkHide">Link a</label>
                        <span class="slr-linkHide">http://</span><input type="text" name="slr-urlto" id="slr-urlto" class="slr-linkHide" />
                        <label class="slr-linkHide">Title link</label>
                        <input type="text" name="slr-title" id="slr-title" class="slr-linkHide" />
                        <label>Orden</label>
                        <input type="text" name="slr-order" id="slr-order" />
                        <?php if($_SESSION['rol'] != 3) { ?>
                        <input type="submit" name="slr-btnAdd" id="slr-btnAdd" value="PUBLICAR">
                        <input type="submit" name="slr-btnCancel" id="slr-btnCancel" value="CANCELAR">
                        <?php } ?>
                    </fieldset>
                </form>
            </div>
            <div class="clr"></div>


            <?php
                //@Controller::INSERTAR BANNER
                //@Autor::Alex Jimenez
                //@Recibe datos de formulario y procesa insercion de banner
                if(isset($_FILES['slr-image']) && !empty($_FILES['slr-image']))
                {
                    if(empty($_POST['slr-texto'])) { $_POST['slr-texto'] = null; }
                    if(empty($_POST['slr-alt'])) { $_POST['slr-alt'] = null; }
                    if(empty($_POST['slr-link'])) { $_POST['slr-link'] = 0; } else { $_POST['slr-link'] = 1; }
                    if(empty($_POST['slr-urlto'])) { $_POST['slr-urlto'] = null; }
                    if(empty($_POST['slr-title'])) { $_POST['slr-title'] = null; }

                    define('_PATHSources', $_SERVER['DOCUMENT_ROOT'].'/sources/slider/');
                    $folio = date('mdY');
            
                    $banner = $_FILES['slr-image']['name'];
                    /*$_banner = explode('.', $banner);
                    $banner = $_banner[0].'-slr'.$folio.'.'.$_banner[1];*/
                    move_uploaded_file($_FILES['slr-image']['tmp_name'], _PATHSources.$banner);

                    $arguments = array(
                            $banner,
                            $_POST['slr-texto'],
                            $_POST['slr-alt'],
                            $_POST['slr-link'],
                            $_POST['slr-urlto'],
                            $_POST['slr-title'],
                            $_POST['slr-order']);

                    $insert = json_decode($slider->insertBanner($arguments));
                    if($insert->{'state'} == 'succes'){
                        header('Location:slider?addok=true');
                    }

                }

                if(isset($_GET['addok']))
                {
                    echo '<div class="msg-success">Se agrego el banner exitosamente</div>';
                    header("Refresh: 3; URL=slider");
                }

                //@Controller::ELIMINAR BANNER
                //@Autor::Alex Jimenez
                //@Recibe id por GET eliminar el banner del slider
                if(isset($_GET['delete']))
                {
                    $delbanner = json_decode($slider->delBanner($_GET['delete']));
                    if($delbanner->{'state'} == 'succes')
                    {
                        header('Location:slider?delok=true');
                    }
                }

                if(isset($_GET['delok']))
                {
                    echo '<div class="msg-success">Se elimino el banner seleccionado exitosamente</div>';
                    header("Refresh: 3; URL=slider");
                }



                //@Controller::EDITAR BANNER
                //@Autor::Alex Jimenez
                //@Recibe id por POST para editar el banner del slider
                if(isset($_POST['slr-btnUpd']) && !empty($_POST['slr-updid']))
                {
                    $banner = $_FILES['slr-updslider']['name'] != '' ? $_FILES['slr-updslider']['name'] : NULL;
                    $stateLink = !empty($_POST['slr-updlink']) ? 1 : 0;

                    $args = array($banner, $stateLink, $_POST['slr-updlink'], $_POST['slr-updalt'], $_POST['slr-updtitle'], $_POST['slr-updid']);

                    $updSlider = json_decode($slider->updateSlider($args));
                    if($updSlider->{'state'} == 'succes')
                    {
                        define('_PATHSources', $_SERVER['DOCUMENT_ROOT'].'/sources/slider/');

                        if($banner != NULL){
                            move_uploaded_file($_FILES['slr-updslider']['tmp_name'], _PATHSources.$banner);
                        }
                        
                        header('Location:slider?update=ok');
                    }
                }

                if(isset($_GET['update']) && $_POST['update'] = 'ok')
                {
                    echo '<div class="msg-success">Se modifico el banner seleccionado exitosamente</div>';
                    header("Refresh: 3; URL=slider");
                }
            ?>

        </section>

    </section>
    <div class="clr"></div>

</body>
</html>
<?php ob_end_flush(); ?>