<?php
error_reporting(0);
if (isset($_POST['action']) && $_POST['action'] == 'contact') {
    $promo = (isset($_POST['promo']) ? $_POST['promo'] : null);
    $body = makeMail($_POST['nombre'], $_POST['email'], $_POST['telefono'], $_POST['mensaje'], $_POST['from'], $promo);
    $headers = 'MIME-Version:1.0' . "\r\n" .
        'Content-type:text/html; charset=UTF-8' . "\r\n" .
        'From: info@sundecdecoracion.com' . "\r\n";
    //echo $body;
    if (mail('info@sundecdecoracion.com', 'Contacto Sitio Web', $body, $headers)) {
        echo true;
    } else {
        echo false;
    }
}

function makeMail($name, $mail, $phone, $message, $from, $promo)
{
    $label = 'Formulario de contacto del sitio web';

    if (empty($phone)) {
        $phone = 'No proporciono';
    }

    $html_promo = '';

    if ($promo != null) {
        include($_SERVER['DOCUMENT_ROOT'] . '/models/mPromos.php');
        $deal =  new Promociones();
        $deals = $deal->getDeals();
        $img_promo = '';
        foreach ($deals as $promocion) {
            if ($promocion['promo'] == $promo) {
                $img_promo = trim($promocion['image']);
            }
        }
        $label = 'Formulario de contacto para Promoción';
        $html_promo = '<strong>Promoción:</strong>' . $promo . '</br>
        <img src="http://www.sundecdecoracion.com/sources/promos/' . $img_promo . '" style="width: 150px;"></br>';
    }

    $body = '<span style="font-size:24px;">' . $label . '</span>
					 <br><br>
					 <p>El usuario con los siguientes datos nos contacto:</p><br>			 
                     <strong>Nombre:</strong> ' . $name . '<br>
                     <strong>Correo Electrónico:</strong> ' . $mail . '<br>
                     <strong>Teléfono:</strong>' . $phone . '<br>
                     ' . $html_promo . '
					 <strong>Mensaje:</strong>
                     <p>' . $message . '</p>
                     <strong>Correo enviado desde:</strong> ' . $from . '';

    return $body;
}
