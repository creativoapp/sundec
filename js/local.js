$(document).ready(function(){
	
	//SEND FORM CONTACTO
	$('#sendForm').on('click', function(event){
		event.preventDefault();

		var name = $('#ipt-name').val();
		var phone = $('#ipt-phone').val();
		var mail = $('#ipt-mail').val();
		var message = $('#ipt-message').val();
		var captcha = $('#ipt-captcha').val();

		if (name == null || name.length == 0 || /^\s+$/.test(name)) {
			$('#ipt-name').addClass('fielderror');
			$('#ipt-name').attr('placeholder','Su nombre es obligatorio.');
			$('#ipt-name').focus();
		}
		else
		{
			if (!(/\w+([-+.']\w+)*@\w+([-.]\w+)/.test(mail))) {
				$('#ipt-mail').addClass('fielderror');
				$('#ipt-mail').attr('placeholder','Ingresar un correo es obligatorio.');
				$('#ipt-mail').focus();
			}
			else
			{
				if(message == null || message.length == 0 || /^\s+$/.test(message)) {
					$('#ipt-message').addClass('fielderror');
					$('#ipt-message').attr('placeholder','Ecribir un mensaje es obligatorio');
					$('#ipt-message').focus();
				}
				else
				{
					if(captcha == $('#codesec').val())
					{
						var jSMail = { "name" : name, "phone" : phone, "mail" : mail, "message" : message }; 
						$('#sendForm').attr('disabled','disabled');

						$.ajax({
							data:  jSMail,
							url:   'helpers/hMail.php',
							type:  'post',
												
							beforeSend: function(){
								
							},

							success:  function (data) {
								var response = $.parseJSON(data);
								$('#sendForm').removeAttr('disabled');
								$('#ipt-name, #ipt-phone, #ipt-mail, #ipt-message, #ipt-captcha').val('');
								alert(response.message);

							}
						});
					}
					else
					{
						alert('Código de seguridad incorrecto');
						$('#ipt-captcha').focus();
					}
				}
			}
		}

	});

	//REMOVE CLASS ERROR FORM CONTACTO
	$('#ipt-name, #ipt-mail, #ipt-message, #ipt-captcha').on('keyup', function(){
		if($(this).hasClass('fielderror'))
		{
			$(this).removeClass('fielderror');
		}
	});

	//REGISTRO FORM NEWSLETTER
	$('#btn-newsletter').on('click', function(event){
		event.preventDefault();
		var mailNews = $('#newsletter').val();

		if (!(/\w+([-+.']\w+)*@\w+([-.]\w+)/.test(mailNews))) {
			$('#newsletter').addClass('errorNews');
			$('#newsletter').focus();
		}
		else
		{
			var news = {"mail" : mailNews }; 
								
			$.ajax({
				data:  news,
				url:   'helpers/hNewsletter.php',
				type:  'post',
									
				beforeSend: function(){	},

				success:  function (data) {
					var response = $.parseJSON(data);
					if(response.state == 'succes')
					{
						$('#newsletter').val('');
						$('.news-message').text(response.msgbody);
					}

				}
			});

		}

	});

	//REMOVE CLASS ERROR FORM NEWSLETTER
	$('#newsletter').on('keyup', function(){
		if($(this).hasClass('errorNews'))
		{
			$(this).removeClass('errorNews');
		}
	})


	//MENU MOBILE
	$('#navmobile').on('change', function(){
		var page = $('#navmobile option:selected').data('navmobile');
		
		if(page == '/')
		{
			window.location = 'http://www.sundecdecoracion.com';
		}
		else
		{
			window.location = page;
		}

	});

});