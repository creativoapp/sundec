<?php require('components/header.php'); ?>

<section class="is-view is-view-catalogopage container">

	<?php
		$catalogo = new Catalog();
		$viewpage = json_decode($catalogo->showCategory($route));
		$urlCity = $city_obj['url'];
		if ($urlCity == '/cancun') {
			$urlCity = '';
		}
    ?>

	<div class="columns">

		<div class="column is-half">
			<h1><?= $viewpage->page; ?></h1>
			
			<div class="isBreads">
				<a href="/" title="Sundes Soluciones Decorativas">Inicio</a> <i class="fas fa-long-arrow-alt-right"></i>
				<a href="<?= $urlCity ?>/catalogo" title="Decoración en <?= $city_obj['name'] ?>">Catalogo</a> <i class="fas fa-long-arrow-alt-right"></i>
				<span><?= $viewpage->{'page'}; ?></span>
			</div>

			<?= $viewpage->content; ?>
		</div>

		<div class="column is-half">
			<div class="isContact">

				<h3>Encuéntranos en:</h3>
				<span>
					<i class="fas fa-map-marker-alt"></i>
					<?= $contacto->{'firstAddres'} . '<br>' . $contacto->{'secondAddres'} . '<br>' . $contacto->{'thirdAddres'}; ?>
				</span>

				<span>
					<i class="fas fa-envelope"></i>
					<a href="mailto:<?php echo $contacto->{'mailCont'};?>" class="contactSide"><?php echo $contacto->{'mailCont'};?></a>
				</span>
				
				<span>
					<i class="fas fa-fax"></i>
					<a href="tel:<?php echo $contacto->{'phoneCont'};?>" class="contactSide"><?php echo $contacto->{'phoneCont'};?></a>
				</span>

				<a href="/catalogos-pdf" class="isButtonCatalog">CATALOGO PDF</a>
		
			</div>
		</div>

	</div>


	<div class="columns is-multiline">

		<div class="column is-full">
			<div class="isMenuTabs">

				<?php 
				$menuTabs = $catalogo->getAlbumCategory($viewpage->idcategory, null, 'albums_text'); 
				if($menuTabs != null) {
					$i = 1;
					foreach($menuTabs as $menu) { ?>

					<a href="#<?= MD5($menu->idAncla); ?>" class="isItem"><?= $menu->ancla; ?></a>	 
				<?php
						if($i < count($menuTabs)) { echo '<span>|</span>'; }
						$i++;
					}
				}
				?>

			</div>
		</div>
		
		<?php
		if($menuTabs != null) {
			foreach($menuTabs as $menu) { 
				
				$gallery = $catalogo->getAlbumCategory($viewpage->idcategory, $menu->idAncla);
		?>
				<div class="column is-full isBoxAlbum" id="<?= md5($menu->idAncla); ?>">
			
					<h3><?= $menu->ancla; ?></h3>

					<div class="columns is-multiline">
						<?php 
						if($gallery != null) {
						foreach($gallery as $picture) { ?>
						
						<div class="column is-one-fifth">
							<div class="isPicture">
								<a class="fancybox" rel="alb<?= $menu->idAncla; ?>" href="/sources/galerias/<?= $picture->photo; ?>" title="<?= $picture->name; ?>">
									<img src="/timthumb.php?src=/sources/galerias/<?= $picture->photo; ?>&w=345&h=220&ac=1&q=90" alt="<?= $picture->altPhoto; ?>">
									<div class="isMask"></div>
								</a>
							</div>
						</div>

						<?php } } ?>
						<div class="clr"></div>
					</div>
					
				</div>

		<?php
			}
		}
		?>
	</div>


</section>
	
<?php require('components/footer.php'); ?>