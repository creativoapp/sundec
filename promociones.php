<?php require('components/header.php'); ?>

<section class="is-view is-view-deals container">
    <div class="columns is-multiline is-clearfix">

        <?php
        $deal =  new Promociones();
        $deals = $deal->getDeals();

        if ($deals != null) {
            foreach ($deals as $promo) {
        ?>

                <div class="column is-half">
                    <div class="is-relative">
                        <a class="mask" href="/contacto?promo=<?= $promo['promo']; ?>" title="Preguntar Por <?= $promo['promo']; ?>">
                            <span class="title"><strong>Preguntar Por</strong></br><?= $promo['promo']; ?></span>
                        </a>
                        <img src="/sources/promos/<?= $promo['image']; ?>" alt="<?= $promo['alt']; ?>">
                    </div>
                </div>

        <?php
            }
        }
        ?>

    </div>
</section>

<?php require('components/footer.php'); ?>