<?php require('components/header.php'); ?>

<section class="is-view is-view-contact container">
	<div class="columns is-multiline">

		<div class="column is-half">
			<h1>CONTACTO</h1>
			<p>Si necesitas cotizar para algún proyecto decorativo en Cancún, cortinas o persianas, muebles sobre diseño o tienes alguna duda sobre nuestro catálogo de productos decorativos, ponte en contacto con nosotros, en Sundec Decoración estamos seguros de poder ayudarte.</p>


			<p>
				<span><?php echo $contacto->{'firstAddres'}; ?></span>
				<span><?php echo $contacto->{'secondAddres'}; ?></span>
				<span><?php echo $contacto->{'thirdAddres'}; ?></span>
			</p>

			<a href="mailto:<?php echo $contacto->{'mailCont'}; ?>"><?php echo $contacto->{'mailCont'}; ?></a>

			<a href="tel:<?php echo $contacto->{'phoneCont'}; ?>"><?php echo $contacto->{'phoneCont'}; ?></a>

		</div>

		<div class="column is-half">
			<form name="contacto" id="contacto" action="" method="post" class="columns is-multiline">
				<fieldset class="column is-full">
					<label>Nombre completo *</label>
					<input type="text" name="ipt-name" id="ipt-name" />
				</fieldset>
				<fieldset class="column is-two-thirds">
					<label>E-mail *</label>
					<input type="text" name="ipt-mail" id="ipt-mail" />
				</fieldset>
				<fieldset class="column is-one-third">
					<label>Teléfono</label>
					<input type="text" name="ipt-phone" id="ipt-phone" />
				</fieldset>
				<?php
				if (isset($_GET['promo']) && trim($_GET['promo']) != '') {
					$deal =  new Promociones();
					$deals = $deal->getDeals();
					if ($deals != null) {
				?>
						<fieldset class="column is-full">
							<label>Promoción</label>
							<select name="ipt-promo" id="ipt-promo">
								<?php foreach ($deals as $promo) { ?>
									<option value="<?= $promo['promo']; ?>" <?= ($promo['promo'] == $_GET['promo'] ? 'selected' : '') ?>><?= $promo['promo']; ?></option>
								<?php } ?>
							</select>
						</fieldset>
				<?php }
				} ?>
				<fieldset class="column is-full">
					<label>Mensaje *</label>
					<textarea name="ipt-message" id="ipt-message" rows="5"></textarea>
				</fieldset>
				<fieldset class="column is-full">
					<input type="submit" name="sendForm" id="sendForm" value="CONTACTAR" />
				</fieldset>
			</form>
		</div>

		<div class="column is-10 is-offset-1 isMap">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3721.6110200157154!2d-86.83870328523497!3d21.12806858594475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4c2bc423aa9cc1%3A0x2ff422a1b20149ac!2sSundec%20Decoracion!5e0!3m2!1ses!2smx!4v1597861110124!5m2!1ses!2smx" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
		</div>

	</div>


</section>

<?php require('components/footer.php'); ?>