-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 18-09-2020 a las 23:01:33
-- Versión del servidor: 5.7.23-23
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `kaviramx_migration`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `albums`
--

CREATE TABLE `albums` (
  `idPhoto` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `altPhoto` varchar(255) NOT NULL DEFAULT 'Image galler Sundec Decoracion',
  `fkCategory` int(11) NOT NULL,
  `fkAncla` int(11) NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `albums`
--

INSERT INTO `albums` (`idPhoto`, `photo`, `name`, `altPhoto`, `fkCategory`, `fkAncla`, `Status`) VALUES
(46, '8/tapetes-cancun-exterior.jpg', 'Tapetes Cancún Decoración', 'Tapete decorativo para exterior  en Cancun', 8, 15, 1),
(47, '8/tapetes-casa-cancun.jpg', 'Tapetes Cancún Decoración', 'Donde comprar un tapete decorativo para la casa en Cancun', 8, 15, 1),
(48, '8/tapetes-decorativos-cancun.jpg', 'Tapetes Cancún Decoración', 'Comprar tapete tipo persa en Cancun', 8, 15, 1),
(49, '8/tapetes-decorativos-cancun-interior.jpg', 'Tapetes Cancún Decoración', 'Comprar tapete decorativo para interiores para sala', 8, 15, 1),
(50, '8/tapetes-para-casa-oficina-cancun.jpg', 'Tapetes Cancún Decoración', 'Tapetes minimalistas decorativos en Cancun', 8, 15, 1),
(51, '8/tapetes-para-sala-cancun.jpg', 'Tapetes Cancún Decoración', 'Empresa en Cancun de tapetes decorativos', 8, 15, 1),
(52, '9/cortinas-anticiclonicas-acordeon-2.jpg', 'Cortinas Anticiclónicas Cancún CUPRUM', 'Cortinas anticiclonicas en Cancun tipo acordeon', 9, 16, 1),
(53, '9/cortinas-anticiclonicas-acordeon-cancun.jpg', 'Cortinas Anticiclónicas Cancún CUPRUM', 'Empresa de cortinas anticiclonicas profesionales Cancun', 9, 16, 1),
(54, '9/cortinas-anticiclonicas-cancun.jpg', 'Cortinas Anticiclónicas Cancún CUPRUM', 'Diseñar a la medida e instalar cortinas anticiclonicas en Cancun', 9, 16, 1),
(55, '9/cortinas-anticiclonicas-cancun-cafe.jpg', 'Cortinas Anticiclónicas Cancún CUPRUM', 'Cortinas anticiclonicas de exterior en Cancun', 9, 16, 1),
(56, '9/cortina-anticiclonica-blanca-cancun.jpg', 'Persianas Anticiclónicas Persax Cancun', 'Instalar persianas anticiclonicas blancas en Cancun', 9, 17, 1),
(57, '9/persiana-anticiclonia-europea-cancun.jpg', 'Persianas Anticiclónicas Persax Cancun', 'Persianas anticiclonicas exteriores en Cancun bonitas', 9, 17, 1),
(58, '9/persiana-anticiclonica-cancun-cafe.jpg', 'Persianas Anticiclónicas Persax Cancun', 'Persianas anticiclonicas de lujo para hotel en Cancun', 9, 17, 1),
(59, '9/persiana-anticiclonica-cancun-vista-interior.jpg', 'Persianas Anticiclónicas Persax Cancun', 'Instalar persianas anticiclonicas en Cancun que se vean bien', 9, 17, 1),
(60, '9/persianas-anticiclonicas-cancun-fachada.jpg', 'Persianas Anticiclónicas Persax Cancun', 'Comprar persianas anticiclonicas en cancun al mayoreo', 9, 17, 1),
(61, '9/persianas-anticiclonicas-cancun-fachadas.jpg', 'Persianas Anticiclónicas Persax Cancun', 'Empresa de persianas anticiclonicas en Cancun buen precio', 9, 17, 1),
(62, '10/celosia-persax-cancun-lujo.jpg', 'Celosías en Cancún', 'Comprar e instalar celosias en Cancun', 10, 18, 1),
(63, '10/celosias-cancun-blancas.jpg', 'Celosías en Cancún', 'Celosias industriales en Cancun para estacionamientos', 10, 18, 1),
(64, '10/celosias-cancun-construccion.JPG', 'Celosías en Cancún', 'celosias especiales para el calor en cancun para industrias', 10, 18, 1),
(65, '10/celosias-cancun-grandes.JPG', 'Celosías en Cancún', 'Empresa que diseñe e instale celosias en Cancun grandes', 10, 18, 1),
(66, '10/celosias-cancun-persax.jpg', 'Celosías en Cancún', 'Instalar celosias para hoteles u oficinas en Cancun', 10, 18, 1),
(67, '10/celosias-decoracion-cancun.JPG', 'Celosías en Cancún', 'Empresa de instalacion de celosias en Cancun', 10, 18, 1),
(68, '10/celosias-ventanas-cancun.JPG', 'Celosías en Cancún', 'Empresa que vende celosias tipo ventilas en Cancun', 10, 18, 1),
(69, '11/mosquitera-cancun.jpg', 'Mosquiteras en Cancún', 'Comprar e instalar mosquiteras en Cancun', 11, 19, 1),
(70, '11/mosquitera-cancun-decoracion.jpg', 'Mosquiteras en Cancún', 'Mosquiteros corredisos Cancun diseño e instalacion', 11, 19, 1),
(71, '11/mosquitera-enrollable-cancun.jpg', 'Mosquiteros en Cancún', 'Mosquiteros diseñados a la medida en Cancun', 11, 19, 1),
(72, '11/mosquitera-plisada-cancun.jpg', 'Mosquiteras en Cancún', 'Comprar mosquiteras bonitas para la sala en Cancun', 11, 19, 1),
(73, '11/mosquitera-plisada-cancun-2.jpg', 'Mosquiteras en Cancún', 'Mosquiteras para casa residencial instalacion y diseño en cancun', 11, 19, 1),
(74, '11/mosquitera-plisada-decorativa.jpg', 'Mosquiteras en Cancún', 'Mosquiteras enrollables en Cancun para casa', 11, 19, 1),
(75, '11/mosquitera-plisada-exterior.jpg', 'Mosquiteros en Cancún', 'Mosquiteros en cancun especiales para sala y exterior', 11, 19, 1),
(76, '11/mosquitera-plisada-teka.jpg', 'Mosquiteras en Cancún', 'Mosquiteras correderas en Cancun empresa que las instale', 11, 19, 1),
(77, '11/mosquitera-puerta-lateral.jpg', 'Mosquiteros en Cancún', 'Mosquiteros diseñados a la medida para casas residenciales en Cancun', 11, 19, 1),
(78, '11/mosquitero-enrollable-cancun-persax.jpg', 'Mosquiteras en Cancún', 'Mosquitera enrollable marca persax distribuidor en Cancun', 11, 19, 1),
(79, '11/mosquitero-plisada-teka-cancun.jpg', 'Mosquiteras en Cancún', 'mosquiteros corredisos para jardin sala en Cancun', 11, 19, 1),
(80, '11/puerta-mosquitera-cancun.jpg', 'Mosquiteros en Cancún', 'Mosquiteros decorativos en cancun', 11, 19, 1),
(81, '11/puerta-mosquitera-lateral-cancun.jpg', 'Mosquiteras en Cancún', 'Mosquiteras para ventanas grandes en Cancun', 11, 19, 1),
(92, '13/instalacion-de-toldos-cancun.JPG', 'Toldos en Cancún', 'Diseño e instalacion de toldos en Cancun por Sundec', 13, 21, 1),
(93, '13/precio-toldos-medida-cancun.JPG', 'Toldos en Cancún', 'Toldos especiales para restaurantes en la acera', 13, 21, 1),
(94, '13/toldo-corta-vientos-cancun.JPG', 'Toldos en Cancún', 'Empresa que diseñe e instale toldos especiales en Cancun', 13, 21, 1),
(95, '13/toldo-para-sol-alberca-cancun.JPG', 'Toldos en Cancún', 'Toldos bonitos para alberca de hoteles en Cancun', 13, 21, 1),
(97, '13/toldos-decorativos-para-hotel-cancun.JPG', 'Toldos en Cancún', 'Toldos especiales decorativos para hoteles en Cancun', 13, 21, 1),
(98, '13/toldos-monoblock-cancun.JPG', 'Toldos en Cancún', 'Diseñar toldos monoblock en Cancun e instalar que empresa', 13, 21, 1),
(99, '13/toldos-negocio-calle-cancun.JPG', 'Toldos en Cancún', 'Toldos sobre diseño en cancun para la calle o acera', 13, 21, 1),
(100, '13/toldos-para-jardin-cancun-palillera.jpg', 'Toldos en Cancún', 'Comprar toldos para jardin en Cancun tipo palillera', 13, 21, 1),
(102, '13/toldos-para-sol-cancun.JPG', 'Toldos en Cancún', 'Toldos para sol retractil en Cancun para negocios en al calle', 13, 21, 1),
(103, '13/toldos-para-terrazas-cancun-estor.jpg', 'Toldos en Cancún', 'Toldos al mayoreo Estor para terraza de hotel o restaurante', 13, 21, 1),
(104, '13/toldos-para-terrazas-punto-recto.JPG', 'Toldos en Cancún', 'Toldos para terraza en Cancun corte punto recto para hotel o jardin', 13, 21, 1),
(110, '13/toldos-punto-recto-cancun.jpg', 'Toldos en Cancún', 'Toldos con corte punto recto en Cancun para alberca o ventanas', 13, 21, 1),
(112, '5/cortina-intimidad-cancun.jpg', 'Cortinas Drapes by Dues 1', 'Cortina decorativa para habitación Cancun', 5, 11, 1),
(114, '5/cortinas-decoracion-cancun.jpg', 'Cortinas Drapes by Dues 3', 'Cortina en Cancun para sala decorativa', 5, 11, 1),
(116, '5/cortinas-decorativas-cancun-raso.jpg', 'Cortinas Drapes by Dues 5', 'Cortinas decorativas para sala en Cancún', 5, 11, 1),
(117, '5/cortinas-elegantes-cancun.jpg', 'Cortinas Drapes by Dues 6', 'Cortinas profesionales para sala decorativas Cancun', 5, 11, 1),
(118, '5/cortinas-frescas-cancun.jpg', 'Cortinas Drapes by Dues 7', 'cortina decorativas juego con comedor Cancun', 5, 11, 1),
(119, '5/cortinas-frescas-sala-drapes.jpg', 'Cortinas Drapes by Dues 8', 'Cortinas frescas decorativas cancun', 5, 11, 1),
(122, '5/persianas-bonitas-cancun.jpg', 'Cortinas Drapes by Dues 11', 'Cortinas para cuarto en Cancun en la playa', 5, 11, 1),
(123, '5/persianas-cancun-blancas-drapes.jpg', 'Cortinas Drapes by Dues 12', 'Cortinas decorativas de lujo en Cancun', 5, 11, 1),
(124, '5/persianas-comedor-cancun.jpg', 'Cortinas Drapes by Dues 13', 'Cortinas decorativas blancas para comedor Cancun', 5, 11, 1),
(126, '5/persianas-cortinas-habitacion-cancun.jpg', 'Cortinas Drapes by Dues 15', 'Cortinas para habitación en Cancún decorativas', 5, 11, 1),
(127, '5/persianas-decorativas-cancun-nuit.jpg', 'Cortinas Drapes by Dues 16', 'Cortina decorativa blanca para habitacion', 5, 11, 1),
(128, '5/persianas-interiores-cancun.jpg', 'Cortinas Drapes by Dues 17', 'Empresa de cortinas en Cancun a la medida', 5, 11, 1),
(131, '15/sombrilla-cancun-vallarta.jpg', 'Sombrilla estilo Vallarta en Cancún', 'Comprar sombrillas en Cancún para area de alberca', 15, 22, 1),
(132, '15/sombrillas-cancun-bm-aluminum-classic.jpg', 'Sombrillas en Cancún de aluminio', 'Sombrillas hoteleras para alberca', 15, 22, 1),
(133, '15/sombrillas-cancun-flotante.jpg', 'Sombrillas en Cancún para Café o Restaurante', 'Sombrillas grandes flotantes en Cancún para terraza', 15, 22, 1),
(134, '15/sombrillas-cancun-grande-terraza.jpg', 'Sombrilla Flotante Cancún', 'Sombrillas flotantes grandes para terraza de restaurante cancun', 15, 22, 1),
(135, '15/sombrillas-cancun-solair-cafe.jpg', 'Sombrillas blancas Cancún', 'Empresa de sombrillas en Cancún flotantes para terrazas', 15, 22, 1),
(136, '15/sombrillas-cancun-tuuci-ocean-master.jpg', 'Sombrillas para mesas exteriores Cancun', 'Sombrillas naranjas para exterior con pie Cancun', 15, 22, 1),
(137, '15/sombrillas-cancun-tuuci-stingray.jpg', 'Sombrillas Modernas en Cancún Tuuci', 'Sombrillas modernas Cancun con forma de mantarraya', 15, 22, 1),
(138, '15/sombrillas-en-cancun-toltec.jpg', 'Sombrillas Hoteleras para la playa Cancun', 'Sombrillas elegantes para playa y alberca de hoteles Cancun', 15, 22, 1),
(150, '5/persianas-cancun-dual-romanlux.jpg', 'Persianas en Cancún Classic Romanlux', 'Persianas en Cancun decorativas profesionales', 5, 24, 1),
(151, '5/persianas-cancun-dual-romanlux-cortinas.jpg', 'Persianas en Cancún Classic Romanlux', 'Comprar e instalar persianas en Cancún para oficina', 5, 24, 1),
(152, '5/persianas-classic-dual-romanlux-cancun.jpg', 'Persianas en Cancún Classic Romanlux', 'Persianas decorativas en Cancún y precios', 5, 24, 1),
(153, '5/persianas-cortinas-dual-romanlux-cancun.jpg', 'Persianas en Cancún Classic Romanlux', 'Persianas bonitas para habitación en Cancún', 5, 24, 1),
(155, '5/persianas-cancun-classic-lumett-decoracion.jpg', 'Cortinas en Cancún Classic Lumett', 'comprar cortinas en Cancún e instalar para habitación', 5, 25, 1),
(156, '5/persianas-cancun-classic-lumett-decorativas.jpg', 'Cortinas en Cancún Classic Lumett', 'Cortinas para sala en Cancún decorativas', 5, 25, 1),
(157, '5/persianas-cancun-lumett-blancas.jpg', 'Cortinas en Cancún Classic Lumett', 'Cortinas decorativas blancas en Cancun para la sala', 5, 25, 1),
(158, '5/persianas-cancun-classic-lumett.jpg', 'Cortinas en Cancún Classic Lumett', 'Donde compro cortinas en Cancun decorativas bonitas', 5, 25, 1),
(159, '17/camastros-alberca-lujo-cancun.jpg', 'Camastros en Cancún', 'Diseño y venta de camastros de lujo en Cancun', 17, 26, 1),
(160, '17/camastros-cancun-1.jpg', 'Camastros en Cancún', 'camastros minimalistas para hoteles en Cancun', 17, 26, 1),
(161, '17/camastros-cancun-acolchonados.jpg', 'Camastros en Cancún', 'Venta y distribucion de camastros de lujo para hoteles en Cancun', 17, 26, 1),
(162, '17/camastros-cancun-adan-y-eva.jpg', 'Camastros en Cancún', 'Empresa de venta de camastros de lujo en Cancun', 17, 26, 1),
(163, '17/camastros-cancun-alberca.jpg', 'Camastros en Cancún', 'Donde comprar camastros minimalistas en cancun', 17, 26, 1),
(164, '17/camastros-cancun-alberca-infinity.jpg', 'Camastros en Cancún', 'camastros minimalistas empresa en Cancun', 17, 26, 1),
(165, '17/camastros-cancun-antracita.jpg', 'Camastros en Cancún', 'Empresa que distribuye camastros de exterior en Cancun', 17, 26, 1),
(166, '17/camastros-cancun-bali-madera.jpg', 'Camastros en Cancún', 'Camastros en Cancun tipo bali de madera para hoteles', 17, 26, 1),
(167, '17/camastros-cancun-colores.jpg', 'Camastros en Cancún', 'Comprar camastros de diferentes colores en Cancun para el hotel', 17, 26, 1),
(168, '17/camastros-cancun-con-ruedas.jpg', 'Camastros en Cancún', 'Camastros minimalistas en Cancun con ruedas', 17, 26, 1),
(169, '17/camastros-cancun-detalle-stack.jpg', 'Camastros en Cancún', 'Empresa que venda camastros en Cancun detalle Stack', 17, 26, 1),
(170, '17/camastros-cancun-dobles-kampinski.jpg', 'Camastros en Cancún', 'Camastros dobles en Cancun marca Kampinski', 17, 26, 1),
(171, '17/camastros-cancun-elegantes.jpg', 'Camastros en Cancún', 'Camastros minimalistas anchos para interior Cancun', 17, 26, 1),
(172, '17/camastros-cancun-flat.jpg', 'Camastros en Cancún', 'Camastros de diseño flat en cancun blancos', 17, 26, 1),
(173, '17/camastros-cancun-jardin.jpg', 'Camastros en Cancún', 'Camastros blancos para jardin o club de playa', 17, 26, 1),
(174, '17/camastros-cancun-minimalistas-blancos.jpg', 'Camastros en Cancún', 'Quien vende camastros minimalistas bonitos en Cancun', 17, 26, 1),
(175, '17/camastros-cancun-minimalistas-diseno.jpg', 'Camastros en Cancún', 'Camastro de diseño minimalista con mesa', 17, 26, 1),
(176, '17/camastros-cancun-minimal-nice.jpg', 'Camastros en Cancún', 'Camastros con colchon blanco minimalista de exterior', 17, 26, 1),
(177, '17/camastros-cancun-playa.jpg', 'Camastros en Cancún', 'Camastros duros de aluminio para la playa en Cancun', 17, 26, 1),
(178, '17/camastros-cancun-saler-y-mesa.jpg', 'Camastros en Cancún', 'Comprar juego de camastros y mesas para alberca o playa', 17, 26, 1),
(179, '17/daybed-cama-exterior-alberca-cancun.jpg', 'Camas de Exterior Cancún', 'Camas de exterior para alberca minimalistas para hoteles', 17, 27, 1),
(180, '17/daybed-cama-exterior-bali-cancun.jpg', 'Camas de Exterior Cancún', 'Daybed  o camas de exterior para playa o jardin en Cancun', 17, 27, 1),
(181, '17/daybed-cama-exterior-cancun.jpg', 'Camas de Exterior Cancún', 'Comprar Camas de alberca minimalistas en Cancun', 17, 27, 1),
(182, '17/daybed-cama-exterior-kempinski-cancun.jpg', 'Camas de Exterior Cancún', 'Camas de exterior con respaldo en Cancun', 17, 27, 1),
(183, '17/daybed-cama-exterior-lujo-cancun.jpg', 'Camas de Exterior Cancún', 'Cama o tumbona exterior de lujo tipo madera en Cancun', 17, 27, 1),
(184, '17/daybed-cama-exterior-luxury-cancun.jpg', 'Camas de Exterior Cancún', 'Donde comprar camas de exterior de lujo tipo minimalista', 17, 27, 1),
(185, '17/daybed-cama-exterior-playa-cancun.jpg', 'Camas de Exterior Cancún', 'Comprar cama exterior para club de playa en Cancun', 17, 27, 1),
(186, '17/daybed-cama-exterior-reclinable-cancun.jpg', 'Camas de Exterior Cancún', 'Camas de exterior dobles con respaldo reclinable blancas', 17, 27, 1),
(187, '17/daybed-sofa-exterior-cancun.jpg', 'Camas de Exterior Cancún', 'Pequeña cama exterior para jardin de casa en Cancun', 17, 27, 1),
(188, '17/mesa-comedor-periqueras-exterior-cancun.jpg', 'Muebles de Exterior Cancún', 'Mesa Periquera con Sillas para jardin en Cancun', 17, 28, 1),
(189, '17/mueble-exterior-silla-mesa-cancun.jpg', 'Muebles de Exterior Cancún', 'Muebles tipo sala para exterior jardin o playa', 17, 28, 1),
(190, '17/sala-exterior-cancun.jpg', 'Muebles de Exterior Cancún', 'Juego de sala exterior flexible minimal', 17, 28, 1),
(191, '17/silla-exterior-antracita-cancun.jpg', 'Muebles de Exterior Cancún', 'Comprar mesedora comoda para exteriores en Cancun', 17, 28, 1),
(192, '17/sillas-de-exterior-blancas-cancun.jpg', 'Muebles de Exterior Cancún', 'Donde comprar sillas de exterior minimalistas apilables', 17, 28, 1),
(193, '17/sillas-de-exterior-comedor-cancun.jpg', 'Mosquiteros en Cancún', 'Sillas para comedor exterior como jardin', 17, 28, 1),
(194, '17/sillas-exterior-gandia-blasco-cancun.jpg', 'Muebles de Exterior Cancún', 'Empresa en Cancun que venda sillas de exterior comodas', 17, 28, 1),
(195, '17/sillones-exterior-playa-cancun.jpg', 'Mosquiteros en Cancún', 'Sillones para sala exterior blancos en Cancun', 17, 28, 1),
(196, '18/pisos-de-madera-cancun-cappa.jpg', 'Pisos de Madera Cappa', 'Pisos de madera en Cancun para decoracion de hogar', 18, 29, 1),
(197, '18/pisos-de-madera-cancun-europrime-cappa.jpg', 'Pisos de Madera Cappa Cancún', 'Hemosos pisos de madera laminada por sundec decoracion', 18, 29, 1),
(198, '18/pisos-de-madera-cancun-torello-cappa.jpg', 'Pisos de Madera Cappa Cancún', 'Pisos de madera obscura diseño e instalacion en Cancun', 18, 29, 1),
(199, '18/pisos-de-madera-laminada-cancun-cappa.jpg', 'Pisos de Madera Cappa Cancún', 'Excelente diseño de pisos de madera laminada en Cancun', 18, 29, 1),
(201, '18/pisos-de-madera-laminados-cancun-cappa.jpg', 'Pisos de Madera Cappa Cancún', 'Instalación de pisos de madera laminada en Cancun', 18, 29, 1),
(202, '18/pisos-de-madera-moderna-cancun-cappa.jpg', 'Pisos de Madera Cappa Cancún', 'Empresa que instala pisos de madera decorativa en Cancun', 18, 29, 1),
(203, '19/piso-de-madera-cancun-cedrino-coventino.jpg', 'Pisos de Madera Conventino Cancún', 'Pisos de madera Cedrino por Coventino en Cancun', 19, 30, 1),
(204, '19/piso-de-madera-cancun-coventino-decoracion.jpg', 'Pisos de Madera Conventino Cancún', 'Marca de pisos de madera Conventino en Cancun', 19, 30, 1),
(205, '19/piso-de-madera-cancun-coventino-laminada.jpg', 'Pisos de Madera Conventino Cancún', 'Pisos de madera finos laminados en Cancun', 19, 30, 1),
(206, '19/piso-de-madera-cancun-coventino-laminados.jpg', 'Pisos de Madera Conventino Cancún', 'Diseño e instalacion de pisos de madera en Cancun', 19, 30, 1),
(207, '19/piso-de-madera-cancun-drizzle-coventino.jpg', 'Pisos de Madera Conventino Cancún', 'Instalacion de pisos de madera decorativa en Cancun', 19, 30, 1),
(208, '19/piso-de-madera-clara-cancun-coventino.jpg', 'Pisos de Madera Conventino Cancún', 'Empresas de pisos de madera decorativa en Cancun', 19, 30, 1),
(209, '19/piso-de-madera-decorativa-cancun-coventino.jpg', 'Pisos de Madera Conventino Cancún', 'Quien instala pisos de madera en cancun', 19, 30, 1),
(210, '19/piso-de-madera-laminada-cancun-coventino.jpg', 'Pisos de Madera Conventino Cancún', 'Como poner piso de madera en cancun para decorar Casa', 19, 30, 1),
(211, '19/pisos-de-madera-cancun-coventino-decorativo.jpg', 'Pisos de Madera Conventino Cancún', 'Empresa para instala pisos de madera decorativa en Cancun', 19, 30, 1),
(212, '5/persianas-cancun-classic-noxis.jpg', 'Persianas en Cancún Classic Noxis', 'Instalar persianas elegantes color negro para oficina en Cancun', 5, 31, 1),
(213, '5/persianas-cancun-classic-noxis-oficina.jpg', 'Persianas en Cancún Classic Noxis', 'Donde comprar persianas classy para oficina en Cancun', 5, 31, 1),
(214, '5/persianas-y-cortinas-cancun-classic-noxis-decor.jpg', 'Persianas en Cancún Classic Noxis', 'Empresa que venda persianas elegantes para estudio de lujo en Cancun', 5, 31, 1),
(215, '5/persianas-en-cancun-classic-panellet.jpg', 'Persianas en Cancún Classic Panellet', 'Instalar persianas ejecutivas para oficina en centro de Cancun', 5, 32, 1),
(216, '5/persianas-en-cancun-classic-panellet-decorativa.jpg', 'Persianas en Cancún Classic Panellet', 'Donde conseguir persianas semi transparentes para sala de casa en Cancun', 5, 32, 1),
(217, '5/cortinas-en-cancun-decorativas-classic-roller.jpg', 'Persianas en Cancún Classic Roller', 'Comprar persianas semi transparentes para la habitacion', 5, 33, 1),
(218, '5/persianas-cancun-para-cocina-classic-roller.jpg', 'Persianas en Cancún Classic Roller', 'Comprar e instalar persiana coqueta para cocina en Cancun', 5, 33, 1),
(219, '5/persianas-cancun-sala-comedor-classic-roller.jpg', 'Persianas en Cancún Classic Roller', 'Venta e instalación de persianas de lujo para sala o estudio', 5, 33, 1),
(220, '5/persianas-en-cancun-decorativas-classic-roller.jpg', 'Persianas en Cancún Classic Roller', 'Comprar persianas elegantes para comedor de casa residencial', 5, 33, 1),
(221, '5/cortinas-classic-romanlux-cancun.jpg', '34', 'Image galler Sundec Decoracion', 5, 34, 1),
(222, '5/cortina-tipo-persiana-classic-romanlux.jpg', '34', 'Image galler Sundec Decoracion', 5, 34, 1),
(223, '5/cortina-tipo-persiana-classic-romanlux-azul.jpg', '34', 'Image galler Sundec Decoracion', 5, 34, 1),
(224, '5/persiana-classic-romanlux-cocina.jpg', '34', 'Image galler Sundec Decoracion', 5, 34, 1),
(225, '5/persiana-classic-romanlux-endless-cord.jpg', '34', 'Image galler Sundec Decoracion', 5, 34, 1),
(226, '5/persiana-minimal-classic-romanlux.jpg', '34', 'Image galler Sundec Decoracion', 5, 34, 1),
(227, '5/persiana-minimalista-classic-romanlux.jpg', '34', 'Image galler Sundec Decoracion', 5, 34, 1),
(228, '5/persianas-classic-veilott-cancun.jpg', 'Persianas en Cancún Classic Veilott', 'Empresa que venda persianas para recamara en Cancun profesional', 5, 35, 1),
(229, '5/persianas-classic-veilott-lujo.jpg', 'Persianas en Cancún Classic Veilott', 'Comprar persianas blancas para estudio de casa', 5, 35, 1),
(230, '5/persianas-classic-veilott-sala.jpg', 'Persianas en Cancún Classic Veilott', 'Persianas elegantes tipo crema para comedor en Cancun', 5, 35, 1),
(231, '5/persianas-classic-veilott-semi-transparentes.jpg', 'Persianas en Cancún Classic Veilott', 'Instalar persianas minimalistas de moda en Cancun', 5, 35, 1),
(232, '5/persianas-classic-venetta-casa.jpg', 'Persianas en Cancún Classic Venetta', 'Existe en Cancun una empresa que diseñe e instale persianas elegantes', 5, 36, 1),
(233, '5/persianas-classic-venetta-oficina.jpg', 'Persianas en Cancún Classic Venetta', 'Comprar persianas bonitas de color obscuro para oficina ejecutiva', 5, 36, 1),
(234, '5/persianas-classic-wolett-acapulco.jpg', 'Persianas en Cancún Classic Wolett', 'Persianas elegantes grandes para sala para que entre mucha luz', 5, 37, 1),
(235, '5/persianas-classic-wolett-habitacion-lujo.jpg', 'Persianas en Cancún Classic Wolett', 'Persianas de media luz para habitacion de residencia en Cancun', 5, 37, 1),
(236, '5/persianas-classic-wolett-lujo.jpg', 'Persianas en Cancún Classic Wolett', 'Empresa que venda persianas Wolett en cancun con showroom', 5, 37, 1),
(237, '5/persianas-classic-wolett-monterrey.jpg', 'Persianas en Cancún Classic Wolett', 'Showroom de persianas en Cancun para poner en la casa', 5, 37, 1),
(238, '5/persianas-classic-wooders-estudio.jpg', 'Persianas en Cancún Classic Wooders', 'Donde conseguir persianas tipo madera o bamboo en Cancun', 5, 38, 1),
(239, '5/persianas-classic-wooders-tipo-madera.jpg', 'Persianas en Cancún Classic Wooders', 'Comprar persianas de madera o bambu en Cancun', 5, 38, 1),
(240, '5/panel-persianas-cancun-gabin.jpg', 'Persianas decorativas para Casa', 'Persianas decorativas para Casa en Cancun o Riviera Maya', 5, 41, 1),
(241, '5/persianas-cancun-disney-gabin.jpg', 'Cortina con diseño Disney para niños', 'Persiana con diseño Disney para niños en Cancun o Riviera Maya', 5, 41, 1),
(242, '5/persianas-de-madera-cancun-gabin.jpg', 'Persiana de Maderas', 'Persiana de Madera en Cancun, Playa del Carmen o Riviera Maya', 5, 41, 1),
(243, '5/persianas-playa-del-carmen-eclise-gabin.jpg', 'Persiana para oficina ', 'Persiana para oficina en Cancun o Playa del Carmen de PVC', 5, 41, 1),
(244, '12/muebles-en-cancun-1.jpg', 'Mueble de Sala Cancún', 'Muebles minimalistas en Cancún', 12, 20, 1),
(247, '12/muebles-en-cancun-4.jpg', 'Muebles para sala en Cancún Minimalista', 'Muebles minimalistas para departamento en Cancun', 12, 20, 1),
(252, '14/candil-cancun.jpg', 'Candil Vintage en Cancún', 'Donde comprar candiles decorativos en cancun', 14, 12, 1),
(253, '14/candil-cancun-2.jpg', 'Candil Colgante Cancún', 'Tienda de iluminacion con candiles en cancun', 14, 12, 1),
(254, '14/candil-cancun-3.jpg', 'Candil Vintage en Cancún', 'Donde conseguir candiles originales en Cancun', 14, 12, 1),
(255, '14/candil-playa-del-carmen-vintage.jpg', 'Candil Vintage Playa del Carmen', 'Conseguir candiles tipo vintage en Playa del Carmen', 14, 12, 1),
(256, '14/lampara-de-pie-cancun-1.jpg', 'Lámpara de pie en Cancún', 'Tienda de lamparas en cancun para conseguir lampara de pie', 14, 12, 1),
(257, '14/lampara-de-piso-cancun-1.jpg', 'Lámpara de Piso Cancún', 'Tienda de lamparas de piso en Cancun', 14, 12, 1),
(258, '16/deck-cumaru-cancun.jpg', 'Deck Natural Cumaru Cancún', 'Decks en Cancún de madera natural', 16, 42, 1),
(259, '16/deck-cumaru-playa-del-carmen.jpg', 'Deck Cumaru en Playa del Carmen', 'Empresa de instalacion de decks naturales en playa del carmen', 16, 42, 1),
(260, '16/deck-kumaru-alberca-cancun.jpg', 'Deck Natural para alberca Cancún', 'Decks naturales en Cancun para alberca', 16, 42, 1),
(261, '16/deck-kumaru-cancun.jpg', 'Deck Natural Cumaru Cancún', 'Decks Naturales para jardin en Cancun', 16, 42, 1),
(262, '16/deck-kumaru-playa-del-carmen.jpg', 'Deck Cumaru Playa del Carmen', 'Decks decorativos naturales en Playa del Carmen', 16, 42, 1),
(263, '16/deck-pucte-cancun.jpg', 'Deck Madera Pucte Cancún', 'Decks naturales en Cancun de madera pucte', 16, 42, 1),
(264, '16/deck-teca-cancun.jpg', 'Deck Madera Teca Cancún', 'Deck Natural en Cancun de madera Teca', 16, 42, 1),
(265, '16/deck-tzalam-cancun.jpg', 'Deck Natural Tzalam Cancún', 'Deck en Cancun natural de madera tzalam para alberca', 16, 42, 1),
(266, '20/telas-cancun-1.jpg', 'Telas Cancún', 'Telas perennials en Cancún', 20, 43, 1),
(267, '20/telas-cancun-2.jpg', 'Telas Cancún', 'Telas especiales para sala Cancun', 20, 43, 1),
(268, '20/telas-cancun-3.jpg', 'Telas Cancún para Exterior', 'Telas especiales para exterior para club de playa', 20, 43, 1),
(269, '20/telas-cancun-4.jpg', 'Telas Cancún', 'Telas de marcas reconocidas en cancun', 20, 43, 1),
(270, '20/telas-cancun-5.jpg', 'Telas Cancún para Exterior Sunbrella', 'Telas marca sunbrella para exterior', 20, 43, 1),
(271, '20/telas-cancun-6.jpg', 'Telas Cancún Marca Perennials', 'Telas en Cancun marca perennials', 20, 43, 1),
(272, '20/telas-cancun-artell.jpg', 'Telas Cancún', 'Telas en Cancún marca artell', 20, 43, 1),
(273, '20/telas-cancun-artell-2.jpg', 'Telas Cancún Artell', 'Telas en Cancun marca artell especiales', 20, 43, 1),
(274, '20/telas-cancun-jonathan-adler.jpg', 'Telas Cancún Jonathan Adlers', 'Telas en Cancun marca adler distribuidor', 20, 43, 1),
(275, '20/telas-cancun-kravet-couture.jpg', 'Telas para Exterior', 'Telas para cojines de exterior en Playa del Carmen', 20, 43, 1),
(276, '20/telas-cancun-main.jpg', 'Telas Artell', 'Telas marca Artell en Playa del Carmen', 20, 43, 1),
(277, '20/telas-cojines-exterior-playa-del-carmen.jpg', 'Telas para exterior', 'Telas para exterior en playa del Carmen', 20, 43, 1),
(278, '20/telas-playa-del-carmen-artell.jpg', 'Telas Artell', 'Telas marca artell en playa del carmen distribuidor', 20, 43, 1),
(279, '20/telas-playa-del-carmen-exterior.jpg', 'Telas para exterior', 'Telas para exterior en playa del Carmen', 20, 43, 1),
(280, '20/telas-sunbrella-playa-del-carmen.jpg', 'Telas Sunbrella', 'Telas especiales sunbrella en playa del carmen', 20, 43, 1),
(281, '20/telas-sunbrella-playa-del-carmen-2.jpg', 'Telas Sunbrella', 'Telas de diseñador sunbrella en playa del carmen', 20, 43, 1),
(282, '19/pisos-vinilicos-cancun.jpg', 'Pisos de Vinilo en Cancún', 'Pisos Vinílicos en Cancún para sala', 19, 44, 1),
(283, '19/pisos-vinilicos-cancun-2.jpg', 'Pisos de Vinilo en Cancún', 'Vinil para pisos en Cancún', 19, 44, 1),
(284, '19/pisos-vinilicos-cancun-bano.jpg', 'Piso vinílico Bano', 'Piso vinilico para cocina', 19, 44, 1),
(285, '19/pisos-vinilicos-playa-del-carmen.jpg', 'pisos vinilicos cancun ', 'pisos vinilicos en cancun', 19, 44, 1),
(286, '19/piso-vinilico-cappa-cancun.jpg', 'Piso Vinilico marca Cappa', 'Vinilo para pisos en Cancún', 19, 44, 1),
(287, '19/piso-vinilico-playa-del-carmen.jpg', 'Piso Vinilico en Cancún', 'Piso vinilico para cocina', 19, 44, 1),
(288, '21/articulos-decorativos-cancun.jpg', 'Mesa de Articulos Decorativos Sundec Decoracion', 'Articulos Decorativos Sundec Decoracion', 21, 45, 1),
(289, '21/articulos-decorativos-cancun-2.jpg', 'Plantas decorativas de interior', 'Articulos Decorativos Sundec Decoracion', 21, 45, 1),
(290, '21/candil-decorativo-velas-cancun.jpg', 'Candiles decorativos en Cancún por Sundec', 'Articulos Decorativos Sundec Decoracion', 21, 45, 1),
(291, '21/jarrones-decorativos-cancun.jpg', 'Jarrones decorativos', 'Articulos Decorativos Sundec Decoracion', 21, 45, 1),
(292, '21/jarrones-decorativos-playa-del-carmen.jpg', 'Jarrones decorativos en Playa del Carmen', 'Articulos Decorativos Sundec Decoracion', 21, 45, 1),
(293, '21/porta-llaves-decorativo-cancun.jpg', 'Porta llaves decorativo para tu casa', 'Articulos Decorativos Sundec Decoracion', 21, 45, 1),
(294, '21/reloj-decorativo-de-pared-cancun.jpg', 'Relojes decorativos de mesa y pared', 'Articulos Decorativos Sundec Decoracion', 21, 45, 1),
(295, '7/2 Papel Tapiz Playa del Carmen.jpg', 'Papel Tapiz Cancún Sundec Decoración', 'Papel tapiz especial para tu sala en Cancun', 7, 13, 1),
(297, '7/1 Papel Tapiz Cancun.jpg', 'Papel Tapiz Cancún Sundec Decoración', 'Papel tapiz decorativo para tu habitacion en Cancun', 7, 13, 1),
(298, '7/3 Papel Tapiz Riviera Maya (2).jpg', 'Papel Tapiz Cancún Sundec Decoración', 'Papel tapiz vintage para decoracion de casa', 7, 13, 1),
(299, '7/4 Fotomural Playa del Carmen.jpg', 'Papel Tapiz Cancún Sundec Decoración', 'papel tapiz decorativo en Cancun para tu casa', 7, 13, 1),
(300, '14/6 Candil Cancun.jpg', 'Lampara colgante cancun', 'Donde comprar lamparas colgantes decorativas', 14, 12, 1),
(301, '14/7 Candil Cancun.jpg', 'Lampara colgante decorativa', 'Lampara colgante decorativa en Cancun y Playa del Carmen', 14, 12, 1),
(302, '14/8 Candil Cancun.jpg', 'Lampara de pared decorativa', 'Comprar lampara de pared decorativa', 14, 12, 1),
(303, '14/9 lampara exterior Cancun.jpg', 'Lamparas de diseñador', 'lamparas de diseñador en Cancun y Playa del Carmen', 14, 12, 1),
(304, '14/10 lampara textil Cancun.jpg', 'Lampara textil en colores varios', 'Lampara textil en colores varios en Cancun y playa del carmen', 14, 12, 1),
(305, '21/1 Tapete Cancun.jpg', 'Tapetes de colores decorativos', 'Articulos Decorativos Sundec Decoracion', 21, 45, 1),
(306, '21/1 Tapete Exterior  Cancun.jpg', 'Tapetes para exterior para decoración', 'Articulos Decorativos Sundec Decoracion', 21, 45, 1),
(307, '21/2 Tapete Playa del Carmen.jpg', 'Tapete decorativo para sala', 'Articulos Decorativos Sundec Decoracion', 21, 45, 1),
(308, '22/sombrillas-cancun-1.jpg', 'Sombrillas por Sundec Decoración', 'Sombrillas en Cancún para alberca', 22, 46, 1),
(309, '22/sombrillas-cancun-2.jpg', 'Sombrillas por Sundec Decoración', 'Sombrillas en Cancún para alberca minimalistas', 22, 46, 1),
(310, '22/sombrillas-cancun-3.jpg', 'Sombrillas por Sundec Decoración', 'Sombrillas en Cancún para alberca blancas', 22, 46, 1),
(311, '22/sombrillas-cancun-4.jpg', 'Sombrillas por Sundec Decoración', 'Sombrillas en Cancún para exterior de colores', 22, 46, 1),
(312, '22/sombrillas-cancun-5.jpg', 'Sombrillas por Sundec Decoración', 'Sombrillas en Cancún para alberca cuadradas', 22, 46, 1),
(313, '22/sombrillas-cancun-6.jpg', 'Sombrillas por Sundec Decoración', 'Sombrillas en Cancún para alberca cuadradas minimalistas', 22, 46, 1),
(314, '22/sombrillas-cancun-7.jpg', 'Sombrillas por Sundec Decoración', 'Sombrillas en Cancún con pie lateral', 22, 46, 1),
(315, '22/sombrillas-cancun-8.jpg', 'Sombrillas por Sundec Decoración', 'Sombrillas en Cancún para patio o club de playa', 22, 46, 1),
(316, '22/sombrillas-cancun-9.jpg', 'Sombrillas por Sundec Decoración', 'Sombrillas en Cancún con pie lateral para patio o alberca', 22, 46, 1),
(317, '22/sombrillas-cancun-10.jpg', 'Sombrillas por Sundec Decoración', 'Sombrillas en Cancún para alberca blancas clasicas', 22, 46, 1),
(318, '22/sombrillas-cancun-11.jpg', 'Sombrillas por Sundec Decoración', 'Sombrillas en Cancún para alberca blancas', 22, 46, 1),
(333, '14/amparas-candiles-cancun--6.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(334, '14/amparas-candiles-cancun-2.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(335, '14/amparas-candiles-cancun-3.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(336, '14/amparas-candiles-cancun-4.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(337, '14/amparas-candiles-cancun-5.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(338, '14/amparas-candiles-cancun-arik-levy-curtain-cascade-light-vibia-designboom-09.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(339, '14/amparas-candiles-cancun-Break-Floor-Lamp-By-Vibia.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(340, '14/amparas-candiles-cancun-designers1.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(341, '14/amparas-candiles-cancun-match-det-a-01.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(342, '14/amparas-candiles-cancun-maxresdefault.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(343, '14/amparas-candiles-cancun-NORTH-Wall-lamp-Vibia-237563-relef708fdb.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(344, '14/amparas-candiles-cancun-Plis_Outdoor_4040_01_br.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(345, '14/amparas-candiles-cancun-rhythm-col-pro-06-b.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(346, '14/lamparas-candiles-cancun-1.jpg', '12', 'Image galler Sundec Decoracion', 14, 47, 1),
(347, '12/muebles-exterior-cancun-gandio-blasco-2.jpg', 'Muebles de exterior Gandia Blasco', 'Muebles de exterior minimalistas en Cancún', 12, 48, 1),
(348, '12/muebles-exterior-cancun-gandio-blasco-3.jpg', 'Muebles de exterior Gandia Blasco', 'Camastros y mesas de exterior en Cancún', 12, 48, 1),
(349, '12/muebles-exterior-cancun-gandio-blasco-4.jpg', 'Muebles de exterior Gandia Blasco', 'Muebles minimalistas para exterior jardin o eventos', 12, 48, 1),
(350, '12/muebles-exterior-cancun-gandio-blasco-5.jpg', 'Muebles de exterior Gandia Blasco', 'Muebles para exterior con diseño diferente innovador', 12, 48, 1),
(351, '12/muebles-exterior-cancun-gandio-blasco-6.jpg', 'Muebles de exterior Gandia Blasco', 'Muebles para exterior ya sea para playa o jardin', 12, 48, 1),
(352, '12/muebles-exterior-cancun-gandio-blasco-7.jpg', 'Muebles de exterior Gandia Blasco', 'Muebles blancos para exterior de una sola pieza', 12, 48, 1),
(353, '12/muebles-exterior-cancun-gandio-blasco-8.jpg', 'Muebles de exterior Gandia Blasco', 'Muebles sofa tipo columpio para exterior', 12, 48, 1),
(354, '12/muebles-exterior-cancun-gandio-blasco-9.jpg', 'Muebles de exterior Gandia Blasco', 'Mueble tipo sofa para exterior para jardín o patio', 12, 48, 1),
(355, '12/muebles-exterior-cancun-gandio-blasco-10.jpg', 'Muebles de exterior Gandia Blasco', 'Muebles decorativos de exterior', 12, 48, 1),
(356, '12/muebles-exterior-cancun-gandio-blasco-11.jpg', 'Muebles de exterior Gandia Blasco', 'Pufs para exterior que aguantan la lluvia', 12, 48, 1),
(357, '12/muebles-exterior-cancun-gandio-blasco-12.jpg', 'Muebles de exterior Gandia Blasco', 'Camastros y mesas de madera para exterior Cancun', 12, 48, 1),
(358, '12/muebles-exterior-cancun-gandio-blasco-13.jpg', 'Muebles de exterior Gandia Blasco', 'Camastros y mesas blancas para exterior en Cancun', 12, 48, 1),
(359, '12/muebles-exterior-cancun-gandio-blasco-14.jpg', 'Muebles de exterior Gandia Blasco', 'Sillas periqueras blancas de exterior en Cancun', 12, 48, 1),
(360, '12/muebles-exterior-cancun-gandio-blasco-15.jpg', 'Muebles de exterior Gandia Blasco', 'Carpa blanca de exterior tipo tipi', 12, 48, 1),
(361, '12/muebles-exterior-cancun-gandio-blasco-16.jpg', 'Muebles de exterior Gandia Blasco', 'Muebles para exterior de diseño y decoracion en cancun', 12, 48, 1),
(362, '12/muebles-exterior-cancun-gandio-blasco-arumi-composition-ambience-image-1.jpg', 'Muebles de exterior Gandia Blasco', 'Mesas de tipo aluminio para exterior en Cancun', 12, 48, 1),
(363, '16/deck-sintetico-cancun--9.jpg', 'Decks sintéticos', 'Decks sintéticos, especiales para playa en la riviera maya', 16, 23, 1),
(364, '16/deck-sintetico-cancun--11.jpeg', 'Decks sintéticos', 'Decks sintéticos, especiales para playa en la riviera maya', 16, 23, 1),
(365, '16/deck-sintetico-cancun-1.jpg', 'Decks sintéticos para hoteles', 'Decks sintéticos, especiales para playa en la riviera maya', 16, 23, 1),
(366, '16/deck-sintetico-cancun-gris-2.jpg', 'Decks sintéticos', 'Decks sintéticos, especiales para playa en la riviera maya', 16, 23, 1),
(367, '16/deck-sintetico-cancun-muro-trenzado-8.jpg', 'Decks sintéticos para hoteles', 'Decks sintéticos, especiales para playa en la riviera maya', 16, 23, 1),
(368, '16/deck-sintetico-cancun-ocean-hotel-3.jpg', 'Decks sintéticos', 'Decks sintéticos, especiales para playa en la riviera maya, cancun', 16, 23, 1),
(369, '16/deck-sintetico-cancun-ocean-hotels-4.jpg', 'Decks sintéticos para hoteles', 'Decks sintéticos, especiales para playa en la riviera maya, cancun, playa del carmen', 16, 23, 1),
(370, '16/deck-sintetico-cancun-plafon-10.jpeg', 'Decks sintéticos para techos', 'Decks sintéticos, especiales para playa en la riviera maya, cancun', 16, 23, 1),
(371, '16/deck-sintetico-cancun-plafon-12.jpeg', 'Decks sintéticos para hoteles', 'Decks sintéticos, especiales para playa en la riviera maya, cancun', 16, 23, 1),
(372, '16/deck-sintetico-cancun-plafon-13.jpeg', 'Decks sintéticos para hoteles', 'Decks sintéticos, especiales para playa en la riviera maya, cancun', 16, 23, 1),
(373, '23/alfombras-cancun-1-hexagonal.jpg', 'Alfombra modular hexagonal', 'Alfombra modular en Cancun Sundec Decoracion', 23, 49, 1),
(374, '23/alfombras-cancun-2-hotel.jpg', 'Alfombra Decorativa', 'Alfombra en Cancun Sundec Decoracion', 23, 49, 1),
(375, '23/alfombras-cancun-3-modular.jpg', 'Alfombra para oficina en Cancun', 'Alfombra para oficina en Cancun', 23, 49, 1),
(376, '23/alfombras-cancun-4-modular-playa-del-carmen.jpg', 'Alfombra modular Playa del Carmen', 'Alfombra modular Playa del Carmen', 23, 49, 1),
(377, '23/alfombras-cancun-5-oficinas.jpg', 'Alfombra para oficina en Cancun', 'Alfombra para oficina en Cancun', 23, 49, 1),
(378, '23/alfombras-cancun-6-playa-del-carmen.jpg', 'Alfombras en Cancun y Playa del Carmen', 'Alfombras en Cancun y Playa del Carmen', 23, 49, 1),
(379, '23/alfombras-cancun-7-riviera-maya.jpg', 'Alfombra Decorativa', 'Alfombras en Cancun, Playa del Carmen y Riviera Maya', 23, 49, 1),
(380, '23/alfombras-cancun-salon-de-eventos.jpg', 'Alfombra para salones de eventos', 'Alfrombra para salones en la Riviera Maya o Cancun', 23, 49, 1),
(416, '13/toldo-caida-vertical-cancun.jpg', 'Toldos en Cancún', 'Image galler Sundec Decoracion', 13, 21, 1),
(417, '13/toldo-caida-vertical-en-cancun.jpg', 'Toldos en Cancún', 'Image galler Sundec Decoracion', 13, 21, 1),
(418, '13/toldo-de-brazos-invisibles-precios-cancun.jpeg', 'Toldos en Cancún', 'Precios de toldos con brazos invisibles en Cancún', 13, 21, 1),
(419, '13/toldo-de-caida-vertical-cancun.jpg', 'Toldos en Cancún', 'Toldos verticales para restaurantes en Cancún', 13, 21, 1),
(420, '13/toldo-para-sol-cancun.jpg', 'Toldos en Cancún', 'Toldos para sol en Cancún', 13, 21, 1),
(421, '13/toldos-barzos-invisibles-cancun.jpg', 'Toldos en Cancún', 'Toldos invisibles para terraza en Cancún', 13, 21, 1),
(422, '13/toldos-caida-vertical-cancun.jpg', 'Toldos en Cancún', 'Toldo caída vertical para negocios en Cancún', 13, 21, 1),
(423, '13/toldos-invisibles-cancun.jpg', 'Toldos en Cancún', 'Comprar toldos brazos invisibles en Cancún', 13, 21, 1),
(424, '13/toldos-verticales-en-cancun.jpg', 'Toldos en Cancún', 'Toldos para sol precios en Cancún', 13, 21, 1),
(425, '13/toldo-vertical-cancun.jpg', 'Toldos en Cancún', 'Toldos para exteriores en Cancún', 13, 21, 1),
(426, '16/deck-de-madera-cancun.jpg', 'Decks en Cancún', 'Image galler Sundec Decoracion', 16, 23, 1),
(427, '16/deck-de-madera-en-cancun.jpg', 'Decks en Cancún', 'decks para salón de eventos y jardín en Cancún', 16, 23, 1),
(428, '16/deck-de-madera-para-jardin-cancun.jpg', 'Decks en Cancún', 'Deck para terraza y jardín en Cancún', 16, 23, 1),
(429, '16/deck-exteriores-cancun.jpg', 'Decks en Cancún', 'Decks para alberca, patio y jardín en Cancún', 16, 23, 1),
(430, '16/deck-para-patio-cancun.jpg', 'Decks en Cancún', 'Pisos de deck para hoteles en Cancún', 16, 23, 1),
(431, '16/deck-para-piscina-cancun.jpg', 'Decks en Cancún', 'Decks para piscina y patio en Cancún', 16, 23, 1),
(432, '16/deck-para-terrazas-cancun.jpg', 'Decks en Cancún', 'Decks sinteticos para terraza en Cancún', 16, 23, 1),
(433, '16/deck-piscina-cancun.jpg', 'Decks en Cancún', 'decks para piscina en Cancún', 16, 23, 1),
(434, '16/deck-plastico-cancun.jpg', 'Decks en Cancún', 'Decks de plástico para exteriores en Cancún', 16, 23, 1),
(435, '16/decks-exteriores-cancun.JPG', 'Decks en Cancún', 'Pisos de deck en exteriores en Cancún', 16, 23, 1),
(436, '16/deck-sintetico-cancun.jpg', 'Decks en Cancún', 'deck para  alberca en Cancún', 16, 23, 1),
(437, '16/deck-sintetico-en-cancun.jpg', 'Decks en Cancún', 'Deck sintetico para exteriores en Cancún', 16, 23, 1),
(438, '16/deck-terraza-cancun.jpg', 'Decks en Cancún', 'decks sinteticos para negocios en Cancún', 16, 23, 1),
(439, '16/pisos-de-deck-cancun.jpg', 'Decks en Cancún', 'Piso de deck de madera para patio en Cancún', 16, 23, 1),
(440, '16/plafon-de-madera-cancun.jpg', 'Decks en Cancún', 'Plafon de madera para techos en Cancún para negocios', 16, 23, 1),
(441, '16/plafon-de-techo-cancun.jpeg', 'Decks en Cancún', 'Plafon de madera para techos en Cancún', 16, 23, 1),
(442, '5/cortina-para-cuartos-cancun.jpg', 'Cortinas en Cancún', 'Cortina tradicional para cuartos en Cancún', 5, 11, 1),
(443, '5/cortina-para-dormitorio-cancun.jpg', 'Cortinas en Cancún', 'cortinas para dormitorio en Cancún', 5, 11, 1),
(444, '5/cortina-para-sala-cancun.jpg', 'Cortinas en Cancún', 'cortina para salas en Cancún', 5, 11, 1),
(446, '5/cortinas-con-jareta-cancun.jpg', 'Cortinas en Cancún', 'Cortinas con jareta en Cancún', 5, 11, 1),
(447, '5/cortinas-doble-cancun.jpeg', 'Cortinas en Cancún', 'Cortinas dobles en Cancún para el hogar', 5, 11, 1),
(449, '5/cortinas-modernas-cancun.jpg', 'Cortinas en Cancún', 'Cortinas modernas para cuartos en Cancún', 5, 11, 1),
(450, '5/cortinas-noche-dia-cancun.jpg', 'Cortinas en Cancún', 'Cortinas para noche y día en Cancún', 5, 11, 1),
(451, '5/cortinas-para-casa-cancun.jpeg', 'Cortinas en Cancún', 'Cortinas modernas para casa en Cancún', 5, 11, 1),
(452, '5/cortinas-para-el-hogar-cancun.jpg', 'Cortinas en Cancún', 'cortinas para el hogar en Cancún', 5, 11, 1),
(453, '5/cortinas-para-habitacion.jpg', 'Cortinas en Cancún', 'cortinas modernas para habitación en Cancún', 5, 11, 1),
(454, '5/cortinas-para-recamaras-cancun.jpg', 'Cortinas en Cancún', 'Cortinas modernas para recamaras en Cancún', 5, 11, 1),
(455, '5/cortinas-para-sala-cancun.jpg', 'Cortinas en Cancún', 'Cortinas traslucidas para sala Cancún', 5, 11, 1),
(456, '5/cortinas-para-sala-modernas-cancun.jpg', 'Cortinas en Cancún', 'Cortinas modernas traslucidas para sala en Canc+un', 5, 11, 1),
(457, '5/cortinas-tradicionales-cancun.jpg', 'Cortinas en Cancún', 'cortinas modernas para sala en Cancún', 5, 11, 1),
(458, '5/cortinas-tradicionales-en-cancun.jpg', 'Cortinas en Cancún', 'Cortinas tradicionales en Cancún para sala', 5, 11, 1),
(459, '5/cortinas-traslucidas-cancun.jpeg', 'Cortinas en Cancún', 'Cortina traslucida decorativa para el hogar en Cancún', 5, 11, 1),
(460, '5/cortina-tradicional-cancun.jpg', 'Cortinas en Cancún', 'Cortina tradicional para hogar en Cancún', 5, 11, 1),
(461, '5/cortina-tradicional-en-cancun.jpeg', 'Cortinas en Cancún', 'Cortina tradicional en Cancún', 5, 11, 1),
(462, '5/persiana-cocina-cancun.jpg', 'Persianas en Cancún', 'Persiana para cocina en Cancún', 5, 41, 1),
(463, '5/persiana-dia-noche-cancun.jpg', 'Persianas en Cancún', 'Persianas para el día y la noche en Cancún', 5, 41, 1),
(464, '5/persiana-enrollable-cancun.jpg', 'Persianas en Cancún', 'Persiana triple enrollable en Cancún', 5, 41, 1),
(465, '5/persiana-enrollable-doble-cancun.jpg', 'Persianas en Cancún', 'Persiana doble enrollable en Cancún', 5, 41, 1),
(467, '5/persianas-dia-noche-cancun.jpg', 'Persianas en Cancún', 'Persianas día y noche para el hogar en Cancún', 5, 41, 1),
(468, '5/persianas-en-cancun.jpg', 'Persianas en Cancún', 'Persianas decorativas para oficina en Cancún', 5, 41, 1),
(469, '5/persianas-enrollables-caida-inversa-cancun.jpg', 'Persianas en Cancún', 'Persianas enrollables caida inversa en Cancún', 5, 41, 1),
(470, '5/persianas-enrollables-cancun (2).jpg', 'Persianas en Cancún', 'Persianas enrollables para hogar en Cancún', 5, 41, 1),
(471, '5/persianas-enrollables-cancun.jpg', 'Persianas en Cancún', 'Persianas enrollables decortativas para casa en Cancún', 5, 41, 1),
(472, '5/persianas-enrollables-noche -dia-cancun.jpg', 'Persianas en Cancún', 'Persianas enrollables dia y noche en Cancún para interiores', 5, 41, 1),
(473, '5/persianas-enrollables-screen-cancun.jpg', 'Persianas en Cancún', 'Persianas enrollables para casa en Cancún', 5, 41, 1),
(474, '5/persianas-enrollables-traslucidas-cancun.jpg', 'Persianas en Cancún', 'Persianas enrollables traslucidas para comedor en Cancún', 5, 41, 1),
(476, '5/persianas-estilo-celular-cancun.jpg', 'Persianas en Cancún', 'Persianas estilo celular en Cancún', 5, 41, 1),
(477, '5/persiana-sheer-cancun.jpg', 'Persianas en Cancún', 'Persiana sheer para sala en Cancún', 5, 41, 1),
(478, '5/persiana-sheer-elegance.jpg', 'Persianas en Cancún', 'Persiana sheer para el hogar en Cancún', 5, 41, 1),
(479, '5/persianas-romanas-cancun.jpg', 'Persianas en Cancún', 'Persianas estilo romano para oficinas en Cancún', 5, 41, 1),
(480, '5/persianas-romanas-deslizantes-cancun.jpg', 'Cortinas en Cancún', 'Persianas romanas deslizantes en Cancún', 5, 41, 1),
(481, '5/persianas-tipo-sheer-cancun.jpg', 'Persianas en Cancún', 'Persiana tipo sheer en Cancún para restaurantes', 5, 41, 1),
(482, '5/persianas-tipo-sheer-traslucidas-cancun.jpg', 'Persianas en Cancún', 'Persiana tipo sheer translucida en Cancún para sala', 5, 41, 1),
(483, '5/persiana-traslucida-cancun.jpg', 'Persianas en Cancún', 'Persianas traslucidas en Cancún', 5, 41, 1),
(484, '5/persiana-traslucida-enrollable-cancun.jpg', 'Persianas en Cancún', 'Persiana traslucida enrrollable en Cancún', 5, 41, 1),
(485, '5/persiana-tripe-shade-cancun.jpg', 'Persianas en Cancún', 'Persoanas triple sade para sala en Cancún', 5, 41, 1),
(486, '5/persianas-enrollable-screen-cancun.jpg', 'Persianas en Cancún', 'Persianas enrollables tipo screen en Cancún', 5, 41, 1),
(487, '5/persianas-tradicionales-en-cancun.jpg', 'Persianas en Cancún', 'Persianas tradicionales para el hogar en Cancún', 5, 41, 1),
(488, '5/cortinas-estilo-portugues-cancun.jpg', 'Cortinas en Cancún', 'Cortinas estilo portugues en Cancún', 5, 11, 1),
(489, '5/cortinas-estilo-portugues-en-cancun.jpg', 'Cortinas en Cancún', 'Cortinas estilo portugues en Cancún', 5, 11, 1),
(490, '27/toldo-caida-vertical-en-playa-del-carmen.jpg', 'Toldos Playa Del Carmen', 'Toldos para palapas en Playa del Carmen', 27, 51, 1),
(491, '27/toldo-caida-vertical-playa-del-carmen.jpg', 'Toldos Playa Del Carmen', 'Toldo caída vertical en Playa del Carmen', 27, 51, 1),
(492, '27/toldo-de-brazos-invisibles-precios-playa-del-carmen.jpeg', 'Toldos Playa Del Carmen', 'Toldo brazos invisibles en Playa del Carmen', 27, 51, 1),
(493, '27/toldo-de-caida-vertical-playa-del-carmen.jpg', 'Toldos Playa Del Carmen', 'Toldos caida vertical Playa del Carmen', 27, 51, 1),
(494, '27/toldo-para-sol-playa-del-carmen.jpg', 'Toldos Playa Del Carmen', 'Toldos para sol Playa del Carmen', 27, 51, 1),
(495, '27/toldos-barzos-invisibles-playa-del-carmen.jpg', 'Toldos Playa Del Carmen', 'Toldos invisible para terraza Playa del Carmen', 27, 51, 1),
(496, '27/toldos-caida-vertical-playa-del-carmen.jpg', 'Toldos Playa Del Carmen', 'Toldos caída vertical para negocios en Playa del Carmen', 27, 51, 1),
(497, '27/toldos-invisibles-playa-del-carmen.jpg', 'Toldos Playa Del Carmen', 'Toldos invisibles en Playa del Carmen', 27, 51, 1),
(498, '27/toldos-verticales-en-playa-del-carmen.jpg', 'Toldos Playa Del Carmen', 'Toldos verticales en Playa del Carmen', 27, 51, 1),
(499, '27/toldo-vertical-playa-del-carmen.jpg', 'Toldos Playa Del Carmen', 'Toldo vertical para terraza Playa del Carmen', 27, 51, 1),
(516, '28/deck-de-madera-en-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Deck de madera para exteriores Playa del Carmen', 28, 53, 1),
(517, '28/deck-de-madera-para-jardin-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Decks para jardín en Playa del Carmen', 28, 53, 1),
(518, '28/deck-de-madera-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Decks de madera en Playa del Carmen', 28, 53, 1),
(519, '28/deck-exteriores-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Decks para exteriores en Playa del Carmen', 28, 53, 1),
(520, '28/deck-para-patio-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Decks para patio en Playa del Carmen', 28, 53, 1),
(521, '28/deck-para-piscina-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Decks para piscina Playa del Carmen', 28, 53, 1),
(522, '28/deck-para-terrazas-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Decks para terraza y patio Playa del Carmen', 28, 53, 1),
(523, '28/deck-piscina-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Decks para alberca en Playa del Carmen', 28, 53, 1),
(524, '28/deck-plastico-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Decks sintéticos en Playa del Carmen', 28, 53, 1),
(525, '28/decks-exteriores-playa-del-carmen.JPG', 'Decks en Playa del Carmen', 'Decks para exterior Playa del Carmen', 28, 53, 1),
(526, '28/deck-sintetico-en-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Decks de Plástico Playa del Carmen', 28, 53, 1),
(527, '28/deck-sintetico-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Decks sintético para hoteles o negocios Playa del Carmen', 28, 53, 1),
(528, '28/deck-terraza-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Decks para negocios y casa en Playa del Carmen', 28, 53, 1),
(529, '28/pisos-de-deck-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Pisos de deck para Playa Del Carmen', 28, 53, 1),
(530, '28/plafon-de-madera-playa-del-carmen.jpg', 'Decks en Playa del Carmen', 'Plafon para techo en Playa del Carmen', 28, 53, 1),
(531, '28/plafon-de-techo-playa-del-carmen.jpeg', 'Decks en Playa del Carmen', 'Techo de plafon en Playa del Carmen', 28, 53, 1),
(532, '29/cortina-para-cuartos-playa-del-carmen.jpg', 'Cortinas y Persianas en Playa del Carmen', 'Cortina para habitación en Playa del Carmen', 29, 57, 1),
(533, '29/cortina-para-dormitorio-playa-del-carmen.jpg', 'Cortinas y Persianas en Playa del Carmen', 'Cortina para dormitorio Playa del Carmen', 29, 57, 1),
(534, '29/cortina-para-sala-playa-del-carmen.jpg', 'Cortinas y Persianas en Playa del Carmen', 'Cortina para sale en Playa del Carmen', 29, 57, 1);
INSERT INTO `albums` (`idPhoto`, `photo`, `name`, `altPhoto`, `fkCategory`, `fkAncla`, `Status`) VALUES
(535, '29/cortinas-con-jareta-playa-del-carmen.jpg', 'Cortinas y Persianas en Playa del Carmen', 'Cortinas con jaretas Playa del Carmen', 29, 57, 1),
(536, '29/cortinas-doble-playa-del-carmen.jpeg', 'Cortinas y Persianas en Playa del Carmen', 'Cortinas dobles en Playa del Carmen', 29, 57, 1),
(537, '29/cortinas-estilo-portugues-en-playa-del-carmen.jpg', 'Cortinas y Persianas en Playa del Carmen', 'Cortinas estilo portugues en Playa del Carmen', 29, 57, 1),
(538, '29/cortinas-estilo-portugues-playa-del-carmen.jpg', 'Cortinas y Persianas en Playa del Carmen', 'Image galler Sundec Decoracion', 29, 57, 1),
(539, '29/cortinas-modernas-playa-del-carmen.jpg', 'Cortinas en Playa del Carmen', 'Cortinas modernas en Playa del Carmen', 29, 57, 1),
(540, '29/cortinas-noche-dia-playa-del-carmen.jpg', 'Cortinas en Playa del Carmen', 'Cortinas día y noche Playa del Carmen', 29, 57, 1),
(541, '29/cortinas-para-casa-playa-del-carmen.jpeg', 'Cortinas en Playa del Carmen', 'Costinas para casa en Playa del Carmen', 29, 57, 1),
(542, '29/cortinas-para-cuartos-playa-del-carmen.jpg', 'Cortinas en Playa del Carmen', 'Cortinas para cuartos en Playa del Carmen', 29, 57, 1),
(543, '29/cortinas-para-el-hogar-playa-del-carmen.jpg', 'Cortinas en Playa del Carmen', 'Cortinas para el hogar en Playa del Carmen', 29, 57, 1),
(544, '29/cortinas-para-habitacion-playa-del-carmen.jpg', 'Cortinas en Playa del Carmen', 'Cortinas para habitación juvenil en Playa del Carmen', 29, 57, 1),
(545, '29/cortinas-para-recamaras-playa-del-carmen.jpg', 'Cortinas en Playa del Carmen', 'Cortinas para recamáras en Playa del Carmen', 29, 57, 1),
(546, '29/cortinas-para-sala-modernas-cancun.jpg', 'Cortinas en Playa del Carmen', 'Cortinas para salas modernas en Playa del Carmen', 29, 57, 1),
(547, '29/cortinas-para-sala-playa-del-carmen.jpg', 'Cortinas en Playa del Carmen', 'cortinas para sala en Playa del Carmen', 29, 57, 1),
(548, '29/cortinas-tradicionales-en-playa-del-carmen.jpg', 'Cortinas en Playa del Carmen', 'Cortinas tradicionales en Playa del Carmen', 29, 57, 1),
(549, '29/cortinas-tradicionales-playa-del-carmen.jpg', 'Cortinas en Playa del Carmen', 'Cortinas tradicionales para el hogar en Playa del Carmen', 29, 57, 1),
(550, '29/cortinas-traslucidas-playa-del-carmen.jpeg', 'Cortinas en Playa del Carmen', 'cortinas traslucidas para sala en Playa del Carmen', 29, 57, 1),
(551, '29/cortina-tradicional-en-cancun.jpeg', 'Cortinas en Playa del Carmen', 'Cortina tradicional traslucida en Playa del Carmen', 29, 57, 1),
(552, '29/persiana-cocina-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persiana para cocina en Playa del Carmen ', 29, 59, 1),
(553, '29/persiana-dia-noche-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas día y noche en Playa del Carmen', 29, 59, 1),
(554, '29/persiana-enrollable-doble-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas enrollables en Playa del Carmen', 29, 59, 1),
(555, '29/persiana-enrollable-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas para casa en Playa del Carmen', 29, 59, 1),
(556, '29/persianas-dia-noche-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas enrollables en Playa del Carmen', 29, 59, 1),
(557, '29/persianas-en-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas tradicionales en Playa del Carmen', 29, 59, 1),
(558, '29/persianas-enrollables-caida-inversa-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas enrollables caída inversa Playa del Carmen', 29, 59, 1),
(559, '29/persianas-enrollable-screen-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'persiana enrollable screen Playa del Carmen', 29, 59, 1),
(560, '29/persianas-enrollables-en-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas enrollables en Playa del Carmen', 29, 59, 1),
(561, '29/persianas-enrollables-noche -dia-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas enrollables día y noche Playa del Carmen', 29, 59, 1),
(562, '29/persianas-enrollables-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas enrollables para oficina Playa del Carmen', 29, 59, 1),
(563, '29/persianas-enrollables-screen-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'persianas enrollables tipo sreen en Playa del Carmen', 29, 59, 1),
(564, '29/persianas-enrollables-traslucidas-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'persianas enrollables traslucidas Playa del Carmen', 29, 59, 1),
(565, '29/persianas-estilo-celular-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas estilo celular Playa del Carmen', 29, 59, 1),
(566, '29/persiana-sheer-elegance-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas tipo sheer elegance playa del carmen', 29, 59, 1),
(567, '29/persiana-sheer-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas sheer en Playa del Carmen', 29, 59, 1),
(568, '29/persianas-romanas-deslizantes-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas tipo romanas deslizantes en Playa del Carmen', 29, 59, 1),
(569, '29/persianas-romanas-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas Romanas en Playa del Carmen', 29, 59, 1),
(570, '29/persianas-tipo-sheer-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas tipo sheer Playa del Carmen', 29, 59, 1),
(571, '29/persianas-tipo-sheer-traslucidas-playa-del-carmen.jpg', 'Persianas en Playa del Carmen', 'Persianas tipo sheer traslucidas Playa del Carmen', 29, 59, 1),
(572, '30/toldo-caida-vertical-en-tulum.jpg', 'Toldos en Tulum', 'Toldos caída vertical en Tulum', 30, 61, 1),
(573, '30/toldo-caida-vertical-tulum.jpg', 'Toldos en Tulum', 'Toldos caída vertical en Tulúm para hogar o negocios', 30, 61, 1),
(574, '30/toldo-de-brazos-invisibles-precios-tulum.jpeg', 'Toldos en Tulum', 'Toldo brazos invisibles Tulum', 30, 61, 1),
(575, '30/toldo-de-caida-vertical-tulum.jpg', 'Toldos en Tulum', 'Toldo caida vertical en Tulum', 30, 61, 1),
(576, '30/toldo-para-sol-tulum.jpg', 'Toldos en Tulum', 'Toldos para sol en Tulum', 30, 61, 1),
(577, '30/toldos-barzos-invisibles-tulum.jpg', 'Toldos en Tulum', 'toldos invisibles en tulum', 30, 61, 1),
(578, '30/toldos-caida-vertical-tulum.jpg', 'Toldos en Tulum', 'Toldos caida vertical para negocios en Tulum', 30, 61, 1),
(579, '30/toldos-invisibles-tulum.jpg', 'Toldos en Tulum', 'Toldos invisibles en Tulum', 30, 61, 1),
(580, '30/toldos-verticales-en-tulum.jpg', 'Toldos en Tulum', 'Toldos verticales en Tulum', 30, 61, 1),
(581, '30/toldo-vertical-tulum.jpg', 'Toldos en Tulum', 'toldo vertical en tulum', 30, 61, 1),
(582, '31/deck-de-madera-en-tulum.jpg', 'Decks en Tulum', 'Decks de madera en Tulum', 31, 62, 1),
(583, '31/deck-de-madera-para-jardin-tulum.jpg', 'Decks en Tulum', 'Decks de Madera para Jardín en Tulum', 31, 62, 1),
(584, '31/deck-de-madera-tulum.jpg', 'Decks en Tulum', 'Deck de madera Tulum', 31, 62, 1),
(585, '31/deck-exteriores-tulum.jpg', 'Decks en Tulum', 'Decks exteriores Tulum', 31, 62, 1),
(586, '31/deck-para-patio-tulum.jpg', 'Decks en Tulum', 'Decks para patio Tulum', 31, 62, 1),
(587, '31/deck-para-piscina-tulum.jpg', 'Decks en Tulum', 'Decks para piscina en Tulum', 31, 62, 1),
(588, '31/deck-para-terrazas-tulum.jpg', 'Decks en Tulum', 'Deck para terraza en Tulum', 31, 62, 1),
(589, '31/deck-piscina-tulum.jpg', 'Decks en Tulum', 'Deck para piscina en Tulum', 31, 62, 1),
(590, '31/deck-plastico-tulum.jpg', 'Decks en Tulum', 'Decks sinteticos en Tulum', 31, 62, 1),
(591, '31/decks-exteriores-tulum.JPG', 'Decks en Tulum', 'Decks para exteriores en Tulum', 31, 62, 1),
(592, '31/deck-sintetico-en-tulum.jpg', 'Decks en Tulum', 'Decks sinteticos en Tulum', 31, 62, 1),
(593, '31/deck-sintetico-tulum.jpg', 'Decks en Tulum', 'Decks sintetico para psicinas Tulum', 31, 62, 1),
(594, '31/deck-terraza-tulum.jpg', 'Decks en Tulum', 'Deck para terraza en Tulum', 31, 62, 1),
(595, '31/pisos-de-deck-tulum.jpg', 'Decks en Tulum', 'Pisos de decks en Tulum', 31, 62, 1),
(596, '31/plafon-de-madera-tulum.jpg', 'Decks en Tulum', 'Plafon de madera para techos Tulum', 31, 62, 1),
(597, '31/plafon-de-techo-tulum.jpeg', 'Decks en Tulum', 'Plafon para techo en Tulum', 31, 62, 1),
(598, '32/persiana-cocina-tulum.jpg', 'Persianas en Tulum', 'Persianas para cocinas en Tulum', 32, 63, 1),
(599, '32/persiana-dia-noche-tulum.jpg', 'Persianas en Tulum', 'persianas día y noche en Tulum', 32, 63, 1),
(600, '32/persiana-enrollable-doble-tulum.jpg', 'Persianas en Tulum', 'Persianas enrollables dobles Tulum', 32, 63, 1),
(601, '32/persiana-enrollable-tulum.jpg', 'Persianas en Tulum', 'Persianas en Tulum', 32, 63, 1),
(602, '32/persianas-dia-noche-tulum.jpg', 'Persianas en Tulum', 'Persianas para el dia y la noche en Tulum', 32, 63, 1),
(603, '32/persianas-enrollables-caida-inversa-tulum.jpg', 'Persianas en Tulum', 'Persianas enrollables caida inversa en Tulum', 32, 63, 1),
(604, '32/persianas-enrollable-screen-tulum.jpg', 'Persianas en Tulum', 'persianas enrollables tipo screen tulum', 32, 63, 1),
(605, '32/persianas-enrollables-en-tulum.jpg', 'Persianas en Tulum', 'Persianas enrollables en Tulum', 32, 63, 1),
(606, '32/persianas-enrollables-noche -dia-tulum.jpg', 'Persianas en Tulum', 'persianas noche y día en Tulum', 32, 63, 1),
(607, '32/persianas-enrollables-screen-tulum.jpg', 'Persianas en Tulum', 'Persianas enrollables screen en Tulum', 32, 63, 1),
(608, '32/persianas-enrollables-traslucidas-tulum.jpg', 'Persianas en Tulum', 'Persianas enrollables traslucidas tulum', 32, 63, 1),
(609, '32/persianas-enrollables-tulum.jpg', 'Persianas en Tulum', 'Persianas enrollables en Tulum', 32, 63, 1),
(610, '32/persianas-en-tulum.jpg', 'Persianas en Tulum', 'Persianas para negocios y casas en Tulum', 32, 63, 1),
(611, '32/persianas-estilo-celular-tulum.jpg', 'Persianas en Tulum', 'Persianas estilo celular en Tulum', 32, 63, 1),
(612, '32/persiana-sheer-elegance-tulum.jpg', 'Persianas en Tulum', 'Persiana tipo sheer elegance en Tulum', 32, 63, 1),
(613, '32/persiana-sheer-tulum.jpg', 'Persianas en Tulum', 'persiana tipo sheer en Tulum para negocios y hogar', 32, 63, 1),
(614, '32/persianas-romanas-deslizantes-tulum.jpg', 'Persianas en Tulum', 'Persianas tipo romanas deslizantes en Tulum', 32, 63, 1),
(615, '32/persianas-romanas-tulum.jpg', 'Persianas en Tulum', 'Persianas tipo romanas para sala en Tulum', 32, 63, 1),
(616, '32/persianas-tipo-sheer-traslucidas-tulum.jpg', 'Persianas en Tulum', 'persianas tipo sheer traslucidas en Tulum', 32, 63, 1),
(617, '32/persianas-tipo-sheer-tulum.jpg', 'Persianas en Tulum', 'Persianas tipo sheer en Tulum para casa o negocios', 32, 63, 1),
(618, '32/cortina-para-cuartos-tulum.jpg', 'Cortinas en Tulum', 'Cortinas en Tulum para negocios o casas', 32, 64, 1),
(619, '32/cortina-para-dormitorio-tulum.jpg', 'Cortinas en Tulum', 'Cortina para dormitorio en Tulum', 32, 64, 1),
(620, '32/cortina-para-sala-tulum.jpg', 'Cortinas en Tulum', 'Cortina para sala en Tulum', 32, 64, 1),
(621, '32/cortinas-con-jareta-tulum.jpg', 'Cortinas en Tulum', 'Cortinas con jaretas en Tulum', 32, 64, 1),
(622, '32/cortinas-doble-tulum.jpeg', 'Cortinas en Tulum', 'Cortinas dobles en Tulum', 32, 64, 1),
(623, '32/cortinas-estilo-portugues-en-tulum.jpg', 'Cortinas en Tulum', 'Cortinas estilo portugues en Tulum', 32, 64, 1),
(624, '32/cortinas-estilo-portugues-tulum.jpg', 'Cortinas en Tulum', 'Cortinas estilo portugues en Tulum para hogar o negocios', 32, 64, 1),
(625, '32/cortinas-modernas-tulum.jpg', 'Cortinas en Tulum', 'Continas modernas en Tulum', 32, 64, 1),
(626, '32/cortinas-noche-dia-tulum.jpg', 'Cortinas en Tulum', 'Cortinas noche y día en Tulum', 32, 64, 1),
(627, '32/cortinas-para-casa-tulum.jpeg', 'Cortinas en Tulum', 'Cortinas para casa en Tulum', 32, 64, 1),
(628, '32/cortinas-para-cuartos-tulum.jpg', 'Cortinas en Tulum', 'Cortinas para cuartos en Tulum', 32, 64, 1),
(629, '32/cortinas-para-el-hogar-tulum.jpg', 'Cortinas en Tulum', 'Cortinas para el hogar en Tulum', 32, 64, 1),
(630, '32/cortinas-para-habitacion-tulum.jpg', 'Cortinas en Tulum', 'Cortinas para habitacion en Tulum', 32, 64, 1),
(631, '32/cortinas-para-recamaras-tulum.jpg', 'Cortinas en Tulum', 'cortinas para recamaras modernas en Tulum', 32, 64, 1),
(632, '32/cortinas-para-sala-modernas-tulum.jpg', 'Cortinas en Tulum', 'cortinas para sala modernas en Tulum', 32, 64, 1),
(633, '32/cortinas-para-sala-tulum.jpg', 'Cortinas en Tulum', 'cortinas para sala en Tulum', 32, 64, 1),
(634, '32/cortinas-tradicionales-en-tulum.jpg', 'Cortinas en Tulum', 'Cortinas tradicionales en Tulum', 32, 64, 1),
(635, '32/cortinas-tradicionales-tulum.jpg', 'Cortinas en Tulum', 'cortinas tradicionales para el hogar en Tulum', 32, 64, 1),
(636, '32/cortinas-traslucidas-tulum.jpeg', 'Cortinas en Tulum', 'Cortinas traslucidas en Tulum', 32, 64, 1),
(637, '32/cortina-tradicional-en-tulum.jpeg', 'Cortinas en Tulum', 'Cortinas tradicionales en Tulum', 32, 64, 1),
(638, '35/papel-tapiz-para-pared-playa-del-carmen.jpg', 'Papel Tapiz ', 'Papel Tapiz para sala en Playa del Carmen', 35, 65, 1),
(639, '35/papel-tapiz-para-recamara-playa-del-carmen.jpg', 'Papel Tapiz Playa del Carmen', 'Papel Tapiz para pared de dormitorio Playa del Carmen', 35, 65, 1),
(640, '35/tapiceria-playa-del-carmen.jpg', 'Papel Tapiz en Playa del Carmen', 'Tapicería de paredes Playa del Carmen', 35, 65, 1),
(641, '35/tapices-de-pared-playa-del-carmen.jpg', 'Tapices de Pared en Playa del Carmen', 'Tapices de Pared Playa del Carmen', 35, 65, 1),
(642, '33/alfombras-cancun-playa-del-carmen.jpg', 'Alfombras ', 'Alfombras en Playa del Carmen', 33, 66, 1),
(643, '33/alfombras-grandes-playa-del-carmen.jpg', 'Alfombras Grandes Playa del Carmen', 'Alfombras Grandes Playa del Carmen', 33, 66, 1),
(644, '33/alfombras-modular-playa-del-carmen.jpg', 'Alfombras', 'Alfombras para eventos Playa del Carmen', 33, 66, 1),
(645, '33/alfombras-para-hoteles-playa-del-carmen.jpg', 'Alfombras para hoteles en Playa del Carmen', 'Alfombras para hoteles en Playa del Carmen', 33, 66, 1),
(646, '33/alfombras-para-oficina-playa-del-carmen.jpg', 'Alfombras para oficina', 'Alfombras para oficina en Playa del Carmen', 33, 66, 1),
(647, '33/alfombras-playa-del-carmen-para-eventos.jpg', 'Alfombras para eventos Playa del Carmen', 'Alfombras para eventos Playa del Carmen', 33, 66, 1),
(648, '33/alfombras-riviera-maya.jpg', 'Alfombras en Playa del Carmen y Riviera Maya', 'Alfombras en Playa del Carmen y Riviera Maya', 33, 66, 1),
(649, '33/tapetes-playa-del-carmen.jpg', 'Tapetes', 'Tapetes en Playa del Carmen', 33, 66, 1),
(650, '36/alfombras-cancun-playa-del-carmen.jpg', 'Alfombras en Playa del Carmen', 'Alfombras en Playa del Carmen', 36, 67, 1),
(651, '36/alfombras-grandes-playa-del-carmen.jpg', 'Alfombras Grandes Playa del Carmen', 'Alfombras Grandes Playa del Carmen', 36, 67, 1),
(652, '36/alfombras-modular-playa-del-carmen.jpg', 'Alfombras Playa del Carmen', 'Alfombras para eventos en Playa del Carmen', 36, 67, 1),
(653, '36/alfombras-para-hoteles-playa-del-carmen.jpg', 'Alfombras en Playa del Carmen', 'Alfombras para hoteles en Playa del Carmen', 36, 67, 1),
(654, '36/alfombras-para-oficina-playa-del-carmen.jpg', 'Alfombras Playa del Carmen', 'Alfombras para oficina en Playa del Carmen', 36, 67, 1),
(655, '36/alfombras-playa-del-carmen-para-eventos.jpg', 'Alfombras para evento en Playa del Carmen', 'Alfombras para eventos en Playa del Carmen', 36, 67, 1),
(656, '36/alfombras-riviera-maya.jpg', 'Alfombras en Playa del Carmen', 'Alfombras Riviera Maya y Playa del Carmen', 36, 67, 1),
(657, '36/tapetes-playa-del-carmen.jpg', 'Alfombras y Tapetes', 'Alfombras Playa del Carmen', 36, 67, 1),
(658, '34/piso-de-madera-clara-playa-del-carmen.jpg', 'Pisos de Madera Playa del Carmen', 'pisos decorativos de madera en Playa del Carmen', 34, 69, 1),
(659, '34/piso-de-madera-decorativa-playa-del-carmen-coventino.jpg', 'Losa de madera en Playa del Carmen', 'Pisos de madera para la casa en Playa del Carmen', 34, 69, 1),
(660, '34/piso-de-madera-playa-del-carmen-coventino-decoracion.jpg', 'Pisos de madera en Playa del Carmen', 'Pisos de madera para decoración en Playa del Carmen', 34, 69, 1),
(661, '34/piso-de-madera-playa-del-carmen-coventino-laminada.jpg', 'Pisos de madera laminada', 'Pisos de Madera laminada en Playa del Carmen', 34, 69, 1),
(662, '34/piso-de-madera-playa-del-carmen-coventino-laminados.jpg', 'Piso de madera', 'Pisos del madera laminados en Playa del Carmen', 34, 69, 1),
(663, '34/pisos-color-madera-playa-del-carmen.jpg', 'Pisos de madera en Playa del Carmen', 'Pisos color madera en Playa del Carmen', 34, 69, 1),
(664, '34/pisos-de-madera-decorativos-playa-del-carmen.jpg', 'Pisos de madera decorativos', 'Pisos de madera decorativos en Playa del Carmen', 34, 69, 1),
(665, '34/pisos-de-madera-laminada-playa-del-carmen.jpg', 'Pisos de madera en Playa del Carmen', 'pisos de madera en playa del carmen', 34, 69, 1),
(666, '34/pisos-de-madera-playa-del-carmen-drizzle-coventino.jpg', 'Pisos de madera para el hogar en Playa del Carmen', 'Pisos de madera playa del carmen', 34, 69, 1),
(667, '34/piso-vinilico-playa-del-carmen.jpg', 'Pisos Vinilicos', 'Pisos Vinilicos en Playa del Carmen', 34, 70, 1),
(668, '34/losetas-vinilicas-playa-del-carmen.jpg', 'Pisos de Vinilo', 'Pisos de vinilo en Playa del Carmen', 34, 70, 1),
(669, '34/pisos-vinilicos-playa-del-carmen.jpg', 'Pisos Vinilicos en Playa del Carmen', 'Pisos de vinilo en Playa del Carmen', 34, 70, 1),
(670, '34/vinilicos-para-pisos-playa-del-carmen.jpg', 'Vinilos para pisos', 'Pisos de vinilo en Playa del Carmen', 34, 70, 1),
(671, '34/pisos-de-vinilo-playa-del-carmen.jpg', 'Pisos de vinilo en Playa del Carmen', 'Pisos para el hogar en Playa del Carmen', 34, 70, 1),
(672, '34/piso-vinilicos-playa-del-carmen.jpg', 'Pisos sinteticos en Playa del Carmen', 'Pisos sinteticos en Playa del Carmen', 34, 70, 1),
(673, '37/piso-de-madera-decorativa-tulum.jpg', 'Pisos en Tulum', 'Pisos de madera en Tulum', 37, 73, 1),
(674, '37/piso-de-madera-tulum.jpg', 'Pisos de madera decorativos en Tulum', 'Pisos de madera en Tulum', 37, 73, 1),
(675, '37/piso-de-madera-tulum-coventino-decoracion.jpg', 'Pisos en Tulum', 'Pisos de madera en Tulum', 37, 73, 1),
(676, '37/piso-de-madera-tulum-coventino-laminada.jpg', 'Piso laminado de madera en Tulum', 'Pisos de madera en Tulum para restaurantes', 37, 73, 1),
(677, '37/piso-de-madera-tulum-coventino-laminados.jpg', 'Pisos de madera en Tulum laminados', 'Pisos de madera laminados en Tulum para hoteles', 37, 73, 1),
(678, '37/pisos-color-madera-tulum.jpg', 'Pisos color madera', 'Pisos de madera en Tulum', 37, 73, 1),
(679, '37/pisos-de-madera-decorativos-tulum.jpg', 'Pisos de madera en tulum', 'pisos de madera en tulum para casa', 37, 73, 1),
(680, '37/pisos-de-madera-laminada-tulum.jpg', 'pisos de madera laminada en tulum', 'pisos de madera laminada en tulum', 37, 73, 1),
(681, '37/pisos-de-madera-tulum-drizzle-coventino.jpg', 'Pisos de madera decorativos en Tulum', 'pisos de madera decorativa en tulum', 37, 73, 1),
(682, '37/losetas-vinilicas-tulum.jpg', 'Pisos Vinilicos en Tulum', 'Pisos Vinilicos en Tulum', 37, 75, 1),
(683, '37/pisos-de-vinilo-tulum.jpg', 'Pisos de Vinilo en Tulum', 'Vinilicos para Pisos en Tulum', 37, 75, 1),
(684, '37/pisos-vinilicos-tulum.jpg', 'Pisos de Vinil en Tulum', 'Pisos Vinilicos en Tulum', 37, 75, 1),
(685, '37/piso-vinilicos-tulum.jpg', 'Vinilicos para Pisos en Tulum', 'Pisos de Vinil Tulum', 37, 75, 1),
(686, '37/piso-vinilico-tulum.jpg', 'Pisos de Vinil en Tulum', 'Pisos en Tulum', 37, 75, 1),
(687, '37/vinilicos-para-pisos-tulum.jpg', 'Vinilicos para Pisos en Tulum', 'Pisos de Vinilicos en Tulum', 37, 75, 1),
(688, '38/alfombras-en-tulum.jpg', 'Alfombras en Tulum', 'Alfombras en Tulum para el hogar', 38, 76, 1),
(689, '38/alfombras-grandes-en-tulum.jpg', 'Alfombras grandes en Tulum', 'Alfombras en Tulum para hotel ', 38, 76, 1),
(690, '38/alfombras-modular-tulum.jpg', 'Alfombra Modular en Tulum', 'Alfombras Modular en Tulum ', 38, 76, 1),
(691, '38/alfombras-para-hoteles-tulum.jpg', 'Alfombras para hoteles en Tulum', 'Alfombras para Hoteles en Tulum', 38, 76, 1),
(692, '38/alfombras-para-oficina-tulum.jpg', 'Alfombras para oficina en Tulum', 'Alfombras para oficina en Tulum', 38, 76, 1),
(693, '38/alfombras-riviera-maya-tulum.jpg', 'Alfombras en la Riviera Maya y tulum', 'Alfombras en la Riviera Maya y Tulum', 38, 76, 1),
(694, '38/alfombras-tulum.jpg', 'Alfombras en Tulum', 'Alfombras en Tulum para hoteles', 38, 76, 1),
(695, '38/tapetes-en-tulum.jpg', 'Tapetes en Tulum', 'Tapetes en Tulum', 38, 76, 1),
(696, '39/papel-tapiz-para-pared-tulum.jpg', 'Papel Tapiz en Tulum', 'Papel Tapiz en Tulum', 39, 77, 1),
(697, '39/papel-tapiz-para-recamara-tulum.jpg', 'Papel Tapiz para recamara en Tulum', 'Papel Tapiz para recamara en Tulum', 39, 77, 1),
(698, '39/tapiceria-en-tulum.jpg', 'Tapicería en Tulum', 'Tapicería en Tulum', 39, 77, 1),
(699, '39/tapices-de-pared-tulum.jpg', 'Tapices para pared en Tulum', 'Tapices para pared en Tulum', 39, 77, 1),
(700, '23/alfombra-modular -shaw contract-alfombra.jpg', ' alfombras contract modelo shaw', 'Alfombras modernas en cancun', 23, 49, 1),
(701, '23/alfombra-para-sala-en-cancun.jpg', 'alfombra Gradation Shaw Contract ', 'Alfombra para sala en Cancun', 23, 49, 1),
(702, '23/alfombras-contract-cancun.jpg', 'Shaw alfombras contract en Cancún', 'Alfombras modulares en Cancún', 23, 49, 1),
(703, '23/alfombras-modernas-en-cancun.jpg', 'Shaw contract. alfombra en Cancún', 'Alfombras modernas en Cancún', 23, 49, 1),
(704, '23/alfombras-modulares-en-cancun.jpg', 'Alfombra modular Shaw en Cancún', 'Alfombras modulares en Cancún', 23, 49, 1),
(705, '23/alfombras-para-sala-cancun.jpg', 'alfombra modelo shaw contract ', 'Alfombras shaw contract en cancún', 23, 49, 1),
(706, '23/shaw-contract- alfombra-cancun.jpeg', 'Alfombra modelo shaw contract en Cancún', 'Alfombras para hoteles en Cancún', 23, 49, 1),
(707, '19/deck-de-madera-en-cancun.jpg', 'Deck de Madera de ingeniería modelo Dunes Natur', 'Pisos de Madera en Cancún', 19, 30, 1),
(708, '19/madera-de-ingenieria-cancun.jpg', 'Piso madera de ingeniería', 'Pisos de madera en Cancún', 19, 30, 1),
(709, '19/piso-color-madera-en-cancun.jpg', 'Piso Amb Serengeti Safari', 'Piso color madera en Cancún', 19, 30, 1),
(710, '19/piso-de-ingenieria-cancun.jpg', 'Piso de Ingenieria', 'Pisos de ingeniería en Cancún', 19, 30, 1),
(711, '19/piso-laminado-recamaras-cancun.jpg', 'Piso Laurentina Oak', 'Pisos Laminados en Cancun', 19, 30, 1),
(712, '19/piso-madera-de-ingenieria-cancun.jpg', 'Piso Amb Grey Sand Natur', 'Piso de madera de ingeniería Cancun', 19, 30, 1),
(713, '19/piso-para-recamara-cancun.jpg', 'PISO LATTE MADERA DE INGENIERIA  CAPPA', 'Pisos para recamaras en Cancun', 19, 30, 1),
(714, '19/pisos-de-madera-en-cancun (2).jpg', 'HAZELNUT MADERA DE INGENIERIA CAPPA', 'pisos de madera en cancun', 19, 30, 1),
(715, '19/pisos-de-madera-en-cancun.jpg', 'Pisos de Madera en Cancún', 'Pisos laminados en Cancún', 19, 30, 1),
(716, '19/pisos-de-madera-laminados-cancun.jpg', 'PISO EXOTIC CARBON', 'Pisos de madera laminados en Cancun', 19, 30, 1),
(717, '19/pisos-laminado-de-madera-cancun.jpg', 'Pisos Laminados ', 'Pisos laminados en Cancún', 19, 30, 1),
(718, '19/pisos-laminados-de-madera-en-cancun.jpg', 'Amb Heartwood Natur', 'Pisos laminados en madera Cancun', 19, 30, 1),
(719, '19/pisos-laminados-en-cancun.jpg', 'Pisos Laminados en Cancún', 'Pisos de ingenieria laminados', 19, 30, 1),
(720, '19/pisos-para-recamara-cancun.jpg', 'Pisos para recamara', 'Pisos para recamara en Cancún', 19, 30, 1),
(724, '19/pisos-vinilicos-en-cancun.jpg', 'Pisos Vinilicos en Cancún', 'Vinilico para pisos en Cancún', 19, 44, 1),
(725, '19/piso-vinilico-cancun.jpg', 'Piso Vinilo Sahara Sacndinavian', 'Pisos de Vinilo en Cancún', 19, 44, 1),
(726, '19/vinilo-para-pisos-en-cancun.jpg', 'Pisos de Vinil en Cancun', 'Vinilo para pisos en Cancún', 19, 44, 1),
(727, '7/decoracion-papel-tapiz-cancun.jpg', 'Papel Tapiz Loft', 'Papel tapiz decorativo Cancun', 7, 13, 1),
(728, '7/papeles-tapiz-cancun.png', 'Papel Tapiz Cancun', 'Papel Tapiz Decorativo en Cancun', 7, 13, 1),
(729, '7/papel-tapiz-cuarto-cancun.jpg', 'Papel Tapiz Decosimil', 'Papel Tapiz para cuarto Cancun', 7, 13, 1),
(730, '7/papel-tapiz-decorativo-cancun.png', 'Papel Tapiz decoraciones verticales', 'Tapiz para pared en Cancún', 7, 13, 1),
(731, '7/papel-tapiz-decorativo-en-cancun.png', 'Papel Tapiz Tropical Ravena', 'Image galler Sundec Decoracion', 7, 13, 1),
(732, '7/papel-tapiz-para-pared.jpg', 'Papel Tapiz Debut', 'Papel Tapiz pared Cancun', 7, 13, 1),
(733, '7/papel-tapiz-para-pared-en-cancun.jpg', 'Papel Tapiz Decosmil', 'Tapiz para pared en Cancún', 7, 13, 1),
(734, '7/papel-tapiz-para-sala-cancun.jpg', 'Papel Tapiz Debut', 'Papel Tapiz para Sala en Cancun', 7, 13, 1),
(735, '7/papel-tapiz-para-sala-cancun.png', 'Papel Tapiz modelo Debut', 'Papel Tapiz para sala en Cancun', 7, 13, 1),
(736, '7/papel-tapiz-pared-cancun.jpg', 'Papel Tapiz Castelli Ravena', 'Papel Tapiz para Pared Cancun', 7, 13, 1),
(737, '7/tapiz-para-muros-cancun.png', 'Papel Tapiz Sewing Ravena', 'Papel Tapiz para Muros Cancún', 7, 13, 1),
(738, '7/tapiz-para-pared-cancun.jpg', 'Papel Tapiz Loft', 'Papel Tapiz para baño Cancun', 7, 13, 1),
(739, '7/tapiz-para-pared-en-cancun.jpg', 'Papel Tapiz Castelli Decora Pro', 'Tapiz para Pared en Cancun', 7, 13, 1),
(740, '7/tapiz-para-sala-cancun.jpg', 'Papel Tapiz Colección Natura', 'Tapiz para Sala en Cancún', 7, 13, 1),
(741, '33/alfombra-modular -shaw contract-alfombra-playa-del-carmen.jpg', 'Alfombra Modular Playa del Carmen', 'Alfombra modular playa del carmen', 33, 66, 1),
(742, '33/alfombra-para-sala-en-playa-del-carmen.jpg', 'Alfombras Shaw Contrat Playa del Carmen', 'Alfombras en Playa del Carmen', 33, 66, 1),
(744, '33/alfombras-contract-playa-del-carmen.jpg', 'Alfombras Contract en Playa del Carmen', 'Alfombras y Tapetes Playa del Carmen', 33, 66, 1),
(746, '33/alfombras-modernas-en-playa-del-carmen.jpg', 'Alfombra Shaw Contract', 'Alfombras modernas en Playa del Carmen', 33, 66, 1),
(747, '33/alfombras-modulares-en-playa-del-carmen.jpg', 'Alfombra Modular Shaw Contract', 'Alfombras modulares en Playa del Carmen', 33, 66, 1),
(751, '33/alfombras-para-sala-playa-del-carmen.jpg', 'Alfombra Shaw Contract ', 'Alfombras para sala playa del carmen', 33, 66, 1),
(754, '33/shaw-contract- alfombra-playa-del-carmen.jpeg', 'Alfombra Shaw Contract', 'Alfombras y Tapetes Playa del Carmen', 33, 66, 1),
(774, '34/deck-de-madera-en-playa-del-carmen.jpg', 'Pisos en Playa del Carmen', 'Deck de madera Playa del Carmen', 34, 69, 1),
(775, '34/madera-de-ingenieria-playa-del-carmen.jpg', 'Pisos madera de ingeniería Playa del Carmen', 'Pisos madera de ingeniería Playa del Carmen', 34, 69, 1),
(776, '34/piso-color-madera-en-playa-del-carmen.jpg', 'Pisos en Playa del Carmen', 'Pisos color madera en Playa del Carmen', 34, 69, 1),
(777, '34/piso-de-ingenieria-playa-del-carmen.jpg', 'Piso madera de ingeniería Playa del Carmen', 'Pisos de madera Playa del Carmen', 34, 69, 1),
(778, '34/piso-laminado-recamaras-playa-del-carmen.jpg', 'Pisos laminados Playa del Carmen', 'Pisos laminados en Playa del Carmen', 34, 69, 1),
(779, '34/piso-madera-de-ingenieria-playa-del-carmen.jpg', 'Pisos de madera Playa del Carmen', 'Pisos de madera en Playa del Carmen', 34, 69, 1),
(780, '34/piso-para-recamara-playa-del-carmen.jpg', 'Piso para recamara Playa del Carmen', 'Pisos para recamaras en Playa del Carmen', 34, 69, 1),
(781, '34/pisos-de-madera-en-playa-del-carmen.jpg', 'Pisos de madera en Playa del Carmen', 'Pisos en Playa del Carmen', 34, 69, 1),
(782, '34/pisos-de-madera-laminados-playa-del-carmen.jpg', 'Pisos de madera laminados Playa del Carmen', 'Pisos de madera laminados', 34, 69, 1),
(783, '34/pisos-de-madera-playa-del-carmen.jpg', 'Pisos de madera en Playa del Carmen', 'Pisos de madera Playa del Carmen', 34, 69, 1),
(784, '34/pisos-laminado-de-madera-playa-del-carmen.jpg', 'Pisos laminados de madera en Playa del Carmen', 'Pisos de madera Playa del Carmen', 34, 69, 1),
(785, '34/pisos-laminados-de-madera-en-playa-del-carmen.jpg', 'Pisos de madera en Playa del Carmen', 'Pisos de madera Playa del Carmen', 34, 69, 1),
(786, '34/pisos-laminados-en-playa-del-carmen.jpg', 'Pisos laminados en Playa del Carmen', 'Pisos laminados en Playa del Carmen', 34, 69, 1),
(787, '34/pisos-para-recamara-playa-del-carmen.jpg', 'Pisos en Playa del Carmen', 'Pisos para recamara en Playa del Carmen', 34, 69, 1),
(791, '34/pisos-vinilicos-en-playa-del-carmen.jpg', 'Pisos de viníl en Playa del Carmen', 'Vinilo para pisos en Playa del Carmen', 34, 70, 1),
(792, '34/piso-vinilico-playa-del-carmen.jpg', 'Pisos Vinílicos en Playa del Carmen', 'Pisos de Vinílicos en Playa del Carmen', 34, 70, 1),
(793, '34/vinilo-para-pisos-en-playa-del-carmen.jpg', 'Pisos Vinílicos en Playa del Carmen', 'Pisos de vinílicos en Playa del Carmen', 34, 70, 1),
(794, '35/decoracion-papel-tapiz-playa-del-carmen.jpg', 'Papel Tapiz Decorativo Playa del Carmen', 'Papel Tapiz en Playa del Carmen', 35, 65, 1),
(795, '35/papeles-tapiz-playa-del-carmen.png', 'Papel Tapiz en Playa del Carmen', 'Papel Tapiz decorativo Playa del Carmen', 35, 65, 1),
(796, '35/papel-tapiz-cuarto-playa-del-carmen.jpg', 'Papel Tapiz para habitación Playa del Carmen', 'Papel Tapiz Playa del Carmen', 35, 65, 1),
(797, '35/papel-tapiz-decorativo-en-playa-del-carmen.png', 'Paple Tapiz Playa del Carmen', 'Papel Tapiz en Playa del Carmen', 35, 65, 1),
(798, '35/papel-tapiz-decorativo-playa-del-carmen.png', 'Papel Tapiz Playa del Carmen', 'Playa del Carmen Papel Tapiz', 35, 65, 1),
(799, '35/papel-tapiz-para-pared-en-playa-del-carmen.jpg', 'Papel Tapiz Playa del Carmen', 'Tapiz para pared Playa del Carmen', 35, 65, 1),
(800, '35/papel-tapiz-para-playa-del-carmen.jpg', 'Papel Tapiz Playa del Carmen', 'Tapiz decorativo Playa del Carmen', 35, 65, 1),
(801, '35/papel-tapiz-para-sala-playa-del-carmen.jpg', 'Papel Tapiz Playa del Carmen', 'Papel Tapiz para Sala', 35, 65, 1),
(802, '35/papel-tapiz-para-sala-playa-del-carmen.png', 'Papel Tapiz Playa del Carmen', 'Papel Tapiz Sala Playa del Carmen', 35, 65, 1),
(803, '35/papel-tapiz-pared-playa-del-carmen.jpg', 'Papel Tapiz Playa del Carmen', 'Papel Tapiz decorativo en Playa del Carmen', 35, 65, 1),
(804, '35/tapiz-para-muros-playa-del-carmen.png', 'Papel Tapiz Playa del Carmen', 'Tapiz para muros en Playa del Carmen', 35, 65, 1),
(805, '35/tapiz-para-pared-en-playa-del-carmen.jpg', 'Papel Tapiz Playa del Carmen', 'tapiz para pared en Playa del Carmen', 35, 65, 1),
(806, '35/tapiz-para-pared-playa-del-carmen.jpg', 'Papel Tapiz Playa del Carmen', 'Papel Tapiz para Playa del Carmen', 35, 65, 1),
(807, '35/tapiz-para-sala-playa-del-carmen.jpg', 'Papel Tapiz Playa del Carmen', 'Tapiz para sala en Playa del Carmen', 35, 65, 1),
(808, '39/decoracion-papel-tapiz-tulum.jpg', 'Papel Tapiz Decorativo Tulum', 'Papel Tapiz en Tulum', 39, 77, 1),
(809, '39/papeles-tapiz-tulum.png', 'Tapiz para pared en Tulum', 'Tapiz para pared en Tulum', 39, 77, 1),
(810, '39/papel-tapiz-cuarto-tulum.jpg', 'Papel Tapiz para habitación Tulum', 'Papel Tapiz decorativo en Tulum', 39, 77, 1),
(811, '39/papel-tapiz-decorativo-en-tulum.png', 'Tapiz para pared en Tulum', 'Papel Tapiz decorativo en Tulum', 39, 77, 1),
(812, '39/papel-tapiz-decorativo-tulum.png', 'Papel Tapiz en Tulum', 'Tapiceria para pared en Tulum', 39, 77, 1),
(813, '39/papel-tapiz-para-pared-en-tulum.jpg', 'Papel Tapiz decorativo ', 'Papel Tapiz para pared en Tulum', 39, 77, 1),
(814, '39/papel-tapiz-para-sala-tulum.jpg', 'Papel Tapiz en Tulum', 'Tapiz para pared en Tulum', 39, 77, 1),
(815, '39/papel-tapiz-para-sala-tulum.png', 'Papel Tapiz decorativo en tulum', 'Papel Tapiz para sala en Tulum', 39, 77, 1),
(816, '39/papel-tapiz-para-tulum.jpg', 'Papel Tapiz en Tulum', 'Papel Tapiz decorativo en tulum', 39, 77, 1),
(817, '39/papel-tapiz-pared-tulum.jpg', 'Papel Tapiz en Tulum', 'Papel Tapiz para pared en Tulum', 39, 77, 1),
(818, '39/tapiz-para-muros-tulum.png', 'Papel Tapiz por Sundec Decoración', 'Tapiz para muros en Tulum', 39, 77, 1),
(819, '39/tapiz-para-pared-en-tulum.jpg', 'Papel Tapiz en Tulum', 'Tapiz para pared en Tulum', 39, 77, 1),
(820, '39/tapiz-para-pared-tulum.jpg', 'Papel Tapiz decorativo ', 'Tapiz para pared en Tulum', 39, 77, 1),
(821, '39/tapiz-para-sala-tulum.jpg', 'Papel Tapiz para Sala', 'Papel Tapiz para sala en Tulum', 39, 77, 1),
(822, '38/alfombra-modular -shaw contract-alfombra-tulum.jpg', 'Alfombra Saw contract', 'Alfombras en Tulum', 38, 76, 1),
(823, '38/alfombra-para-sala-en-tulum.jpg', 'Alfombra Shaw Contract', 'Alfombras para sala en tulum', 38, 76, 1),
(825, '38/alfombras-contract-tulum.jpg', 'Alfombras shaw contract', 'Alfombras y tapetes en tulum', 38, 76, 1),
(826, '38/alfombras-modernas-en-tulum.jpg', 'Alfombra Shaw Contract', 'Alfombras modernas en tulum', 38, 76, 1),
(827, '38/alfombras-modulares-en-tulum.jpg', 'Alfombra Shaw Contract', 'Alfombras para eventos tulum', 38, 76, 1),
(828, '38/alfombras-para-sala-tulum.jpg', 'Alfombra Shaw Contract', 'alfombras modulares tulum', 38, 76, 1),
(829, '38/shaw-contract- alfombra-tulum.jpeg', 'Alfombra Shaw Contract', 'Alfombras shaw contract tulum', 38, 76, 1),
(830, '37/deck-de-madera-en-tulum.jpg', 'Decks en Tulum', 'Pisos de Deck en Tulum', 37, 73, 1),
(831, '37/madera-de-ingenieria-tulum.jpg', 'Pisos madera de ingeniería', 'Pisos de madera de ingeniería', 37, 73, 1),
(832, '37/piso-color-madera-en-tulum.jpg', 'Pisos de madera en Tulum', 'Pisos de madera en Tulum', 37, 73, 1),
(833, '37/piso-de-ingenieria-tulum.jpg', 'Pisos de madera Sundec', 'Pisos de madera en Tulum', 37, 73, 1),
(834, '37/piso-laminado-recamaras-tulum.jpg', 'Pisos laminados en Madera', 'Piso de madera laminado tulum', 37, 73, 1),
(835, '37/piso-madera-de-ingenieria-tulum.jpg', 'Piso de madera de ingeniería Tulum', 'Pisos de madera ingeniería tulum', 37, 73, 1),
(836, '37/piso-para-recamara-tulum.jpg', 'Piso de madera para recamara en Tulum', 'Pisos de madera recamara Tulum', 37, 73, 1),
(837, '37/pisos-de-madera-en-tulum.jpg', 'Pisos laminado en Tulum', 'Pisos de madera en Tulum', 37, 73, 1),
(838, '37/pisos-de-madera-laminados-tulum.jpg', 'Pisos de madera laminados Tulum', 'Pisos de madera laminados Tulum', 37, 73, 1),
(839, '37/pisos-de-madera-tulum.jpg', 'Pisos de madera decorativos en Tulum', 'Pisos de madera en Tulum', 37, 73, 1),
(840, '37/pisos-laminado-de-madera-tulum.jpg', 'Pisos laminados de madera Tulum', 'Pisos laminados de madera en Tulum', 37, 73, 1),
(841, '37/pisos-laminados-de-madera-en-tulum.jpg', 'Pisos laminados en madera Tulum', 'Pisos laminados en madera Tulum', 37, 73, 1),
(842, '37/pisos-laminados-en-tulum.jpg', 'Pisos laminados en Tulum', 'Pisos laminados en Tulum', 37, 73, 1),
(843, '37/pisos-para-recamara-tulum.jpg', 'Pisos para recamara en Tulum', 'Pisos para recamara en Tulum', 37, 73, 1),
(844, '37/pisos-vinilicos-en-tulum.jpg', 'Pisos Vinílicos en Tulum', 'Pisos de vinilico en Tulum', 37, 73, 1),
(845, '37/piso-vinilico-tulum.jpg', 'Piso vinilico Tulum', 'Pisos de vinil en Tulum', 37, 73, 1),
(846, '37/vinilo-para-pisos-en-tulum.jpg', 'Pisos de vinilo en Tulum', 'Vinilo para pisos en Tulum', 37, 73, 1),
(847, '37/pisos-vinilicos-en-tulum.jpg', 'Pisos vinílicos en Tulum', 'Pisos de Vinilo en Tulum', 37, 75, 1),
(848, '37/piso-vinilico-tulum.jpg', 'Pisos Vinílicos en Tulum', 'Pisos de vinil en Tulum', 37, 75, 1),
(849, '37/vinilo-para-pisos-en-tulum.jpg', 'Pisos Vinílicos en Tulum', 'Pisos de vinilo en Tulum', 37, 75, 1),
(850, '40/toldo-caida-vertical-en-tulum.jpg', 'Toldos en Tulum', 'Toldo caida vertical en Tulum', 40, 78, 1),
(851, '40/toldo-caida-vertical-tulum.jpg', 'Toldos en Tulum', 'Toldos caida vertical para negocios en Tulum', 40, 78, 1),
(852, '40/toldo-de-brazos-invisibles-precios-tulum.jpeg', 'Toldos en Tulum', 'Toldo brazos invisibles Tulum', 40, 78, 1),
(853, '40/toldo-de-caida-vertical-tulum.jpg', 'Toldos en Tulum', 'Toldo caida vertical en Tulum', 40, 78, 1),
(854, '40/toldo-para-sol-tulum.jpg', 'Toldos en Tulúm', 'toldo para sol en Tulum', 40, 78, 1),
(855, '40/toldos-barzos-invisibles-tulum.jpg', 'Toldos en Tulum', 'Toldos brazos invisibles Tulum', 40, 78, 1),
(856, '40/toldos-caida-vertical-tulum.jpg', 'Toldos caida vertical Cancun', 'Toldos en Tulum', 40, 78, 1),
(857, '40/toldos-invisibles-tulum.jpg', 'Toldos en Tulum', 'Toldos invisibles en Cancún', 40, 78, 1),
(858, '40/toldos-verticales-en-tulum.jpg', 'Toldos en Tulum', 'Toldos verticales en Tulum', 40, 78, 1),
(859, '40/toldo-vertical-tulum.jpg', 'Toldos en Tulum', 'Toldo vertical en Tulum', 40, 78, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anclas`
--

CREATE TABLE `anclas` (
  `idAncla` int(11) NOT NULL,
  `ancla` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `fkCategory` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `anclas`
--

INSERT INTO `anclas` (`idAncla`, `ancla`, `status`, `fkCategory`) VALUES
(10, 'Persianas Classic', 0, 5),
(11, 'Cortinas Drapes by Dues y Más', 1, 5),
(12, 'Candiles Cancun', 1, 14),
(13, 'Papel Tapíz', 1, 7),
(14, 'Alfombra Modular', 0, 8),
(15, 'Tapetes', 0, 8),
(16, 'Cortinas Anticiclonicas Cuprum', 0, 9),
(17, 'Persianas Anticiclonicas Persax', 0, 9),
(18, 'Persax', 0, 10),
(19, 'Mosquiteras Persax', 0, 11),
(20, 'Muebles Cancún por Sundec Decoración', 1, 12),
(21, 'Toldos Cancun', 1, 13),
(22, 'Sombrillas en Cancún', 0, 15),
(23, 'Decks Cancun Sintéticos', 1, 16),
(24, 'Persianas Classic Romanlux', 0, 5),
(25, 'Persianas Classic Lumett', 0, 5),
(26, 'Camastros Cancún', 0, 17),
(27, 'Daybed. Camas de Exterior Cancún', 0, 17),
(28, 'Mesas y Sillas de Exterior', 0, 17),
(29, 'Pisos Laminados ', 0, 19),
(30, 'Pisos de Madera', 1, 19),
(31, 'Persianas Classic Noxis', 0, 5),
(32, 'Persianas Classic Panellet', 0, 5),
(33, 'Persianas Classic Roller', 0, 5),
(34, 'Persianas Classic Romanlux', 0, 5),
(35, 'Persianas Classic Veilott', 0, 5),
(36, 'Persianas Classic Venetta', 0, 5),
(37, 'Persianas Classic Wolett', 0, 5),
(38, 'Persianas Classic Wooders', 0, 5),
(39, '', 0, 5),
(40, '', 0, 5),
(41, 'Persianas Gabin y Más', 1, 5),
(42, 'Decks Naturales', 0, 16),
(43, 'Telas Cancun', 1, 20),
(44, 'Pisos Vinílicos', 1, 19),
(45, 'Artículos Decorativos', 1, 21),
(46, 'Sombrillas en Cancún', 0, 22),
(47, 'Candiles y Lámparas Marca Vibia', 1, 14),
(48, 'Muebles Exterior Gandia Blasco', 0, 12),
(49, 'Alfombras Sundec Decoración', 1, 23),
(50, 'prueba', 0, 5),
(51, 'Toldos Playa del Carmen ', 1, 27),
(52, '', 0, 5),
(53, 'Decks Playa del Carmen', 1, 28),
(54, 'Cortinas en Playa del Carmen', 0, 5),
(55, 'Persianas en Playa del Carmen', 0, 5),
(56, '', 0, 5),
(57, 'Cortinas en Playa del Carmen', 1, 29),
(58, 'Persianas en Playa del Carmen', 0, 5),
(59, 'Persianas en Playa del Carmen', 1, 29),
(60, 'Toldos en Tulúm', 0, 5),
(61, 'Toldos en Tulúm', 0, 30),
(62, 'Decks Tulum', 1, 31),
(63, 'Persianas en Tulum', 1, 32),
(64, 'Cortinas en Tulum', 1, 32),
(65, 'Papel Tapiz decorativo Playa del Carmen', 1, 35),
(66, 'Alfombras Playa Del Carmen', 1, 33),
(67, 'Alfombras Playa Del Carmen', 1, 36),
(68, 'Pisos creativos Playa del Carmen', 0, 34),
(69, 'Pisos de Madera', 1, 34),
(70, 'Pisos Vinílicos', 1, 34),
(71, 'Pisos en Tulum', 0, 37),
(72, 'Pisos de Madera', 0, 37),
(73, 'Pisos de Madera en Tulum', 1, 37),
(74, 'Pisos Vinílicos en Tulum', 1, 5),
(75, 'Pisos Vinílicos en Tulum', 1, 37),
(76, 'Alfombras en Tulum', 1, 38),
(77, 'Papel Tapiz Tulum', 1, 39),
(78, 'Toldos en Tulúm', 1, 40),
(79, '', 0, 40);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogospdf`
--

CREATE TABLE `catalogospdf` (
  `pkcatalogo` int(11) NOT NULL,
  `catalogo` varchar(255) NOT NULL,
  `archivo` varchar(255) NOT NULL,
  `fkmodulo` int(11) DEFAULT NULL,
  `estado` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `catalogospdf`
--

INSERT INTO `catalogospdf` (`pkcatalogo`, `catalogo`, `archivo`, `fkmodulo`, `estado`) VALUES
(1, 'Alfombras y tapetes en Cancún', 'Alfombras.pdf', 20, 1),
(2, 'Decks en Cancún', 'Decks.pdf', 15, 1),
(3, 'Papel Tapiz Decorativo', 'PAPEL TAPIZ.pdf', 11, 1),
(4, 'Persianas y Cortinas en Cancún', 'Persianas y Cortinas.pdf', 10, 1),
(5, 'Pisos en Cancún', 'Pisos.pdf', 16, 1),
(6, 'Toldos en Cancún', 'TOLDOS.pdf', 13, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCategoria` int(11) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `photoCategory` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `altProfile` varchar(255) NOT NULL,
  `titleLink` varchar(255) NOT NULL,
  `fkcity` bigint(20) DEFAULT '1',
  `estado` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCategoria`, `categoria`, `photoCategory`, `descripcion`, `altProfile`, `titleLink`, `fkcity`, `estado`) VALUES
(5, 'Persianas y Cortinas', 'Cortinas en Playa del Carmen.jpg', '<strong>Persianas y Cortinas en Cancún</strong>, por Sundec Decoración.<div><br /></div><div>La flexibilidad en el diseño de los espacios es clave en las construcciones modernas.<div><br /></div><div>Es por esto que a partir de nuestras líneas de Persianas y Cortinas, desarrollamos una gran variedad de soluciones integrales para la decoración en casas, oficinas, proyectos de arquitectos y decoradores de interiores.</div></div><div><br /></div><div>En Cancun contamos con Persianas de la marca Gabin en todas sus variedades, en donde puedes encontrar desde persianas Enrollables, Romanas, Paneles hasta las mas sofisticadas como las Sheer elegance y Shangri-la. Practicamente todas nuestras persianas se pueden motorizar con las mejores marcas como lo es Somfy en la cual te ofrcemos 10 años de garantía.</div><div><p><span style=\"\\\\\\\" vertical-align:\"=\"\" sub;\\\\\\\"=\"\">En Cancun contamos con una extensa variedad de Cortinas donde tambien ofrecemos la linea de Drapes by Dues donde destaca su elegancia en textiles y cortineros de la mas alta calidad. Todos estos productos pueden ser utilizadas en casa, oficina y en especial hotelería<span style=\"\\\\\\\" color:\"=\"\" rgb(34,=\"\" 34,=\"\" 34);=\"\" font-family:=\"\" georgia;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);\\\\\\\"=\"\">.</span></span></p></div>', 'Persianas y Cortinas en Cancún', 'Persianas y Cortinas en Cancún', 1, 1),
(7, 'Papel Tapiz', '3 Papel Tapiz Riviera Maya.jpg', '<strong>Acabados en Pared en Cancún</strong>. Por su presencia visual, las paredes adquieren un destacado protagonismo dentro de la decoración.<div><br /></div><div>Contamos con un catálogo completo de Acabados en Pared para la decoración de tu casa u Hotel en Cancún y la Riviera Maya.</div><div><br /></div><div>Tenemos una gama completa de acabados en Piedra para Pared que llamados Panel Stone o Panel Piedra que darán un toque más armónico al ambiente que desees mejorando la decoración de cualquier lugar. También contamos con una colección de papel tapiz extensa para cualquier tipo de decoración.</div>', 'papel tapiz', 'Papel Tapiz - Catálogo', 1, 1),
(8, 'Alfombras y Tapetes', 'alfombras-tapetes-cancun-catalogo.jpg', '<div><strong>Alfombras y Tapetes en Cancún</strong>.</div><div><br /></div>En Sundec Decoración somos especialistas en Diseño de Interiores y tenemos las mejores opciones en Alfombras y Tapetes para la decoración de tu casa u oficina en Cancún, Riviera Maya y Playa del Carmen.<div><br /></div><div>Contamos con varios tipos de alfombras y tapetes, como por ejemplo la Alfombra Modular, que es excelente para el Lobby de un Hotel o un cine, así como salas de conferencias. Por el otro lado tenemos tapetes diseñados o tapetes hechos a la medida del cliente, para la sala, habitación o sala de juegos de cualquier casa, oficina u hotel en Cancún.</div>', 'alfombras y tapetes en cancun catalogo', 'Alfombras y Tapetes - Catálogo', 1, 0),
(9, 'Anticiclónicas', 'cortinas-anticiclonicas-cancun.jpg', '<trong>Cortinas Anticiclónicas en Cancún</strong>.<div><br /></div><div>Sundec Decoración provee de cortinas anticiclónicas para tu casa u Hotel en Cancún, con una variedad de opciones para integrarlas con tu arquitectura.</div><div><br /></div><div>Es muy importante cuidar tu propiedad, ya sea particular o residencial / hotelera, ya que en un lugar como Cancún sabemos que pueden llegar huracanes (ciclones) en la temporada de huracanes, la cual es aproximadamente de Mayo a Noviembre.</div><div><br /></div><div>Por eso contamos con un catálogo de cortinas anticiclónicas de todo tipo y además podemos diseñarlas a la medida de tus necesidades.</div>', 'Proveedor de cortinas anticiclonicas en Cancún', 'Cortinas Anticiclónicas Cancún - Catálogo', 1, 0),
(10, 'Celosías', 'celosias-cancun-persax.jpg', '<div><strong>Celosías en Cancún</strong>.</div><div><br /></div>La solución más elegante para la protección solar.<div><br /></div><div>Con nuestra gama de celosías, Persax ofrece multitud de aplicaciones en cerramientos de fachadas, patios, terrazas etc. Sus ventajas son claras: protección solar, fácil montaje en obra, colores resistentes a la luz y, por supuesto, un gran valor estético.</div>', 'Celosias diseño en Cancún', 'Celosías en Cancún', 1, 0),
(11, 'Mosquiteras', 'mosquiteras-cancun-persax.jpg', '<div><strong>Mosquiteras en Cancún</strong>. Enrollables o correderas.</div><div><br /></div>Todos nuestros diseños están estudiados para cumplir con todas las exigencias, tanto estéticas como de calidad. Protección y belleza, además de su función de protección de la entrada de insectos, en PERSAX siempre piensan en la belleza de los acabados.<div><br /></div><div>Las Mosquiteras con las que contamos pueden ser escogidas de nuestro catálogo y además podemos diseñarlas a la medida de las necesidades de cada espacio y lugar, haciendo que se adecúen perfectamente a la decoración del lugar. Contamos con Persianas Enrollables o correderas que se ajustarán al espacio y lugar donde sean colocadas. Puedes visitar nuestros productos en nuestro Showroom en Cancún en la Av. Huayacán.</div>', 'Mosquiteras en Cancún enrollables', 'Mosquiteras enrollables Cancún', 1, 0),
(12, 'Muebles ', 'muebles-cancun-sundec.jpg', '<strong>Muebles en Cancún</strong>. En todo proceso de decoración, ya sea un proyecto de cero o una reforma, se necesita elegir los muebles que se van a utilizar.<div><span style=\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"color: rgb(34, 34, 34); font-family: \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\'Century Gothic\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\', sans-serif; font-size: 13pt; background-color: white;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"><br /></span></div><div>Las lineas\r\nde muebles exhibidas en Sundec están a la vanguardia en la decoración, sin\r\ndejar a un lado lo mas importante que es la calidad en materiales y la mano de\r\nobra calificada. Personaliza el color de tu muebles y ecoge dentro de las miles\r\nde opciones que tenemos en textiles.</div><div><br /></div><div>Encontraras\r\nen nuestros modelos salas, love seats, sillones individuales y diseño unicos\r\nque podran cubrir cualquier área gracias a la diversidad de sus medidas y\r\nmodulaciones. Todos nuestros muebles pueden ser utilizados para hotelería,\r\nresidencial e incluso comercial.</div>', 'Muebles en Cancún', 'Muebles en Cancún', 1, 0),
(13, 'Toldos', '6 Toldo brazos invisibles.jpg', '<div><strong>Toldos en Cancún</strong>.</div><div><br /></div>En Sundec Decoración proveemos e instalamos\r\ntoldos de Alta Gama para tu Casa, Hotel o Restaurante, siempre cuidando la línea de diseño.\r\nContamos con marcas como Gabin, Persax y Solair contando con los modelos más\r\nbásicos hasta los más robustos y con accionamientos motorizados con accesorios\r\ncomo detectores de vientos y sol para su automática activación.<div><br /></div><div>Contamos con varios proyectos realizados en México y la mayoría de estos en Cancún y la Riviera Maya como Tulum. Contamos con varios modelos y mecanismos en toldos para instalarlos sin problema.</div><div><br /></div><div>Al poder realizar un proyecto a la medida, tenemos la posibilidad de hacerlo para todo tipo de negocios como restaurantes con terrazas o bares y albercas de hoteles. Recordando siempre que los toldos que manejamos en Sundec son de Gama Alta.</div>', 'Toldos en Cancún para sombra exterior', 'Toldos en Cancún - Sundec', 1, 1),
(14, 'Candiles y Lámparas', '1 Candil Cancun.jpg', '<strong>Iluminación en Cancún. Lámparas decorativas</strong>.<div><br /></div><div>La luz se convierte en un elemento muy influyente en la decoración y la ambientación de la estancia. </div><div><br /></div><div>Según la cantidad y el tipo de luz que tengamos el espacio puede verse más grande o más chico, los colores se modifican y las sombras nos juegan un efecto visual en la forma de los muebles.</div><div><br /></div><div>Por eso en Sundec Decoración contamos con el diseño de iluminación de interiores para realzar el ambiente de cualquier lugar, ya sea en hoteles, oficinas o casas residenciales. También contamos con un amplio catálogo de lámparas decorativas que nos ayudarán a crear el ambiente que deseas.</div>', 'Candiles en Cancún decoración interior', 'Candiles en Cancún', 1, 0),
(15, 'Sombrillas', 'sombrillas-en-cancun.jpg', '<strong>Sombrillas en Cancún</strong>.<div><br /></div><div>En Sundec Decoración Cancún, contamos con un catálogo de <strong>Sombrillas</strong> que quedarán perfectas con la decoración de un Club de Playa, Hotel y su área de albercas o para cualquier casa.</div><div><br /></div><div>Tenemos varios modelos para poder realzar el ambiente de cualquier lugar interior o exterior con nuestras sombrillas. Contamos con sombrillas de palo central o sombrillas con palo lateral para una mayor comodidad. Nuestras sombrillas pueden ser pequeñas, medianas o grandes según lo requieran.</div><div><br /></div><div>De igual manera podemos diseñar sombrillas a la medida para un proyecto especial en Cancún y la Riviera Maya.</div>', 'Sombrillas en Cancun para Hoteles o casas en la playa', 'Sombrillas en Cancún', 1, 0),
(16, 'Decks', '1. deck cumaru cancun.jpg', '<strong>Decks en Cancún</strong>.<div><br /></div><div>En Sundec Decoración somos especialistas en decoración de interiores y exteriores. Contamos con experiencia en el diseño de áreas para la colocación de <strong>Decks</strong>, ya sea para un hotel, un área de alberca, casa u oficina. </div><div><br /></div><div>Contamos con diferentes acabados de Decks sintéticos y naturales para el mejor diseño del ambiente exterior en Cancún.</div><div><br /></div><div>Hemos desarrollado varios proyectos a lo largo de los años en Cancún y la Riviera Maya y en otras partes de México. En proyectos como albercas interiores de hoteles y en clubs de playa realizando tanto el deck de los bares como de las albercas.</div>', 'Construccion o Instalacion de decks en Cancun', 'Decks en Cancún', 1, 1),
(17, 'Muebles de Exterior', 'camastros-cancun-a.jpg', '<div><strong>Muebles de Exterior en Cancún</strong>.</div><div><br /></div>En Sundec Decoración Cancún contamos con un extenso catálogo de <strong>muebles de exterior</strong> para cualquier tipo de gustos. Contamos con marcas como Deroviar y Gandia Blasco entre otras que nos proporcionan un amplio rango de oportunidades para tener los mejores muebles de exterior para el ambiente que necesite.&nbsp;<div><br /></div><div>Contamos con camastros, sombrillas de diseño minimalista, mesas y sillas de exterior elegantes, salas completas para el exterior para disfrutar con la familia o amigos contamos con preciosas camas (DayBed) de exterior para un Club de Playa, hotel o casa.&nbsp;</div><div><br /></div><div>Y contamos con todos los accesorios de exterior que pueden ser necesarios para una completa y armoniosa decoración exterior.</div>', 'Muebles de exterior en Cancun para hoteles y casas', 'Muebles de Exterior Cancun', 1, 0),
(19, 'Pisos ', 'Pisos Creativos 8.jpg', '<div><span style=\"font-weight: bold;\">Pisos en Cancún</span>. En <span bold;\\\\\\\\\\\\\\\"=\"\">Sundec Decoración Cancún</span> contamos con pisos de diferentes materiales y técnicas de fabricación. El piso de mayor lujo y elegancia es el de madera de ingeniería de la cual contamos con la variedad más extensa de especies y formatos de duelas para cubrir hasta los gustos más exigentes.</div><div><br /></div><div>También tenemos disponible duelas de imitación madera en material vinílico con tecnología Belga que está revolucionando el mercado de los pisos. Te recomendamos mucho estos <span style=\"font-weight: bold;\">Pisos Vinílicos</span>. Si estás buscando algo más accesible pero que no pierda de vista la apariencia y lujo de la madera contamos con piso laminado. Todos nuestros pisos soportan los climas de Cancún y la Riviera Maya.</div>', 'Pisos de Madera y laminados en Cancun', 'Pisos de Madera en Cancún', 1, 1),
(20, 'Telas', 'telas-cancun-main.jpg', 'En Sundec encontrarás las mejores colecciones de textiles para cualquier proyecto que tengas en mente ya sea para cortinas, cojines, retapizar un mueble, ya sea para interior o exteriores, lo encontrarás todo.&nbsp;<div><br /></div><div>Nuestras colecciones han sido creadas por los mejores diseñadores como Alexa Hampton, Barbara Barry, Barclay Butera, Calvin Klein Home, Candice Olson, Diane Von Furstenberg, Echo Design, J. Banks Design Group, Jeffrey Alan Marks, Jonathan Adler, etre otros.</div>', 'Tienda de decoracion en Cancun con telas especiales', 'Telas Cancún', 1, 0),
(21, 'Artículos Decorativos', 'articulos-decorativos-cancun-main.jpg', 'Si buscas ese portaretratos para colocar una linda imagen, un espejo diferente para adornar tu sala o comedor, un tapete que le de ese toque diferente a tu espacio, velas que iluminen alguna inolvidable velada o cualquier tipo de accesorio decorativo que te ayude a dar ese toque distintivo a tu ambiente lo puedes encontrar en <span style=\\\\\\\"font-weight: bold;\\\\\\\">Sundec Decoración Cancún</span>.', 'Articulos decorativos en Cancun', 'Artículos Decorativos Cancún', 1, 0),
(22, 'Sombrillas', 'sombrillas-cancun-11.jpg', '<div><span style=\"font-weight: bold;\">Sombrillas en Cancún</span>. En Sundec Decoración somos especialistas en Sombrillas para tu hotel, oficina o casa.</div><div><br /></div>Durante los últimos 21 años, nuestro equipo de especialistas se han esforzado para hacer la compra de un paraguas Galtech una experiencia positiva tanto para el comerciante y el consumidor. <br /><br />Nuestros productos de sombra están diseñadas con los mejores materiales disponibles. Nuestros paraguas de aluminio utilizan cables de acero inoxidable y mecanismos de inclinación automática patentados, nuestras sombrillas de madera están acabados con seis capas de barniz de calidad marina para proteger y embellecer la madera. Nuestra selección de tela Sunbrella es insuperable. ', 'Sombrillas en Cancún', 'Sombrillas en Cancún', 1, 0),
(23, 'Alfombras por Sundec Decoración', 'alfrombras-cancun.jpg', '<p class=\"MsoNormal\" style=\"margin: 0px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; background-color: rgb(255, 255, 255);\"><span style=\"font-size: 10.5pt; font-family: Arial, sans-serif; color: black; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">En Sundec Decoración somos especialistas en Diseño de Interiores y tenemos las mejores opciones en Alfombras y Tapetes para la decoración de tu casa u oficina en Cancún, Riviera Maya y Playa del Carmen.</span><span style=\"font-size: 12pt; font-family: \"Times New Roman\", serif;\"><span style=\"text-decoration:underline\"></span><span style=\"text-decoration:underline\"></span></span></p><p class=\"MsoNormal\" style=\"margin: 0px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; background: white;\"><span style=\"font-size: 10.5pt; font-family: Arial, sans-serif; color: black;\"><span style=\"text-decoration:underline\"></span> <span style=\"text-decoration:underline\"></span></span></p><p class=\"MsoNormal\" style=\"margin: 0px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; background: white;\"><span style=\"font-size: 10.5pt; font-family: Arial, sans-serif; color: black;\">Contamos con varios tipos de alfombras y tapetes, como por ejemplo la Alfombra Modular, que es excelente para el Lobby de un Hotel o un cine, así como salas de conferencias. Por el otro lado tenemos tapetes diseñados o tapetes hechos a la medida del cliente, para la sala, habitación o sala de juegos de cualquier casa, oficina u hotel en Cancún.</span></p>', 'Alfombras en Cancun a la medida', 'Alfombras en Cancún por Sundec Decoración', 1, 1),
(24, '', 'default.jpg', '', '', '', 1, 0),
(27, 'Toldos', 'toldos-playa-del-carmen.jpg', '<div>Tenemos lo que necesitas para adecuar tu espacio en <span style=\"font-weight: bold;\">Playa del Carmen</span>, conoce la variedad de <span style=\"font-weight: bold;\">Toldos</span> que tenemos para ti. En Sundec también puedes encontrar todo sobre decoración de interiores en Playa del Carmen.</div><div><br></div>En S<span style=\"font-weight: bold;\">undec Decoración </span>proveemos e instalamos <span style=\"font-weight: bold;\">toldos</span> de Alta Gama para tu Casa, Hotel o Restaurante, siempre cuidando la línea de diseño. Contamos con marcas como Gabin, Persax y Solair contando con los modelos más básicos hasta los más robustos y con accionamientos motorizados con accesorios como detectores de vientos y sol para su automática activación.<div><br></div><div>¡Encuentra los mejores toldos en Playa del Carmen solo en Sundec!</div>', 'Toldos en Playa del Carmen ', 'Toldos en Playa del Carmen', 2, 1),
(28, 'Decks', 'decks-en-playa-del-carmen.jpg', 'En Sundec la <span style=\"font-weight: bold;\">decoración de interiores</span> es nuestra especialidad, renueva tu piso y dale vida a tu hogar o negocio. Tenemos una amplia variedad de<span style=\"font-weight: bold;\"> Decks en Playa del Carmen</span> que te encantarán, encuentra las mejores Decks de madera, sintéticos o naturales en Sundec.<div><br><div><div>Contamos con varios proyectos realizados en México y la mayoría de estos en C<span style=\"font-weight: bold;\">ancún</span> y la Riviera Maya como <span style=\"font-weight: bold;\">Tulúm</span>. Contamos con varios modelos y mecanismos en toldos para instalarlos sin problema.</div><div><br style=\"font-family: Arial, Helvetica, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);\"></div></div></div>', 'Decks en Playa del Carmen', 'Decks en Playa del Carmen catálogo', 2, 1),
(29, 'Persianas y Cortinas', 'persianas-y-cortinas-playa-del-carmen.jpg', '<div>Obten lo mejor de Sundec en <span style=\"font-weight: bold;\">Cortinas y Persianas</span> en la ciudad de <span style=\"font-weight: bold;\">Playa del Carmen. </span>¡Decora y personaliza tu espacio para que luzca espectacular!</div><div><br></div>En Playa del Carmen contamos con<span style=\"font-weight: bold;\"> Persianas</span> de la marca Gabin en todas sus variedades, en donde puedes encontrar desde persianas Enrollables, Romanas, Paneles hasta las máss sofisticadas como las Sheer elegance y Shangri-la. Practicamente todas nuestras persianas se pueden motorizar con las mejores marcas como lo es Somfy en la cual te ofrecemos 10 años de garantía.<div><br></div><div><div>Contamos con una extensa variedad de <span style=\"font-weight: bold;\">Cortinas</span> donde tambien ofrecemos la linea de Drapes by Dues donde destaca su elegancia en textiles y cortineros de la mas alta calidad. Todos estos productos pueden ser utilizadas en casa, oficina y en especial hotelería. Encuéntranos en <span style=\"font-weight: bold;\">Playa del Carmen.</span></div><div><br></div><div>¡Lo mejor en decoración de interiores lo tenemos en Sundec!</div></div>', 'Cortinas Persianas y en Playa del Carmen', 'Cortinas y persianas Playa del Carmen', 2, 1),
(30, 'Toldos', 'toldos-en-tulum.jpg', '<div>En Sundec Decoración proveemos e instalamos toldos de Alta Gama para tu Casa, Hotel o Restaurante, siempre cuidando la línea de diseño. Contamos con marcas como Gabin, Persax y Solair contando con los modelos más básicos hasta los más robustos y con accionamientos motorizados con accesorios como detectores de vientos y sol para su automática activación.</div><div><br></div><div>Contamos con varios proyectos realizados en México y la mayoría de estos en Cancún y la Riviera Maya como Tulum. Contamos con varios modelos y mecanismos en toldos para instalarlos sin problema.</div><div><br></div><div>Al poder realizar un proyecto a la medida, tenemos la posibilidad de hacerlo para todo tipo de negocios como restaurantes con terrazas o bares y albercas de hoteles. Recordando siempre que los toldos que manejamos en Sundec son de Gama Alta.</div>', 'Toldos en Tulúm', 'Toldos en Tulum', 3, 0),
(31, 'Decks', 'decks-en-tulum.jpg', '<div>Contamos con experiencia en el diseño de áreas para la colocación de <span style=\"font-weight: bold;\">Decks</span>, ya sea para un hotel, un área de alberca, casa u oficina en Tulum.&nbsp;</div><div><br></div><div>Contamos con diferentes acabados de Decks sintéticos y naturales para el mejor diseño del ambiente exterior en Tulum.</div><div><br></div><div>Transforma el piso de tu lugar favorito con nuestros increíbles Decks.&nbsp;Hemos desarrollado varios proyectos a lo largo de los años en <span style=\"font-weight: bold;\">Cancún </span>y la <span style=\"font-weight: bold;\">Riviera Maya</span> y en otras partes de México. En proyectos como albercas interiores de hoteles y en clubs de playa realizando tanto el deck de los bares como de las albercas.</div>', 'Decks en Tulum', 'Pisos de Deck en Tulum', 3, 1),
(32, 'Persianas y Cortinas', 'persianas-y-cortinas-tulum.jpg', 'Conoce nuestra amplia línea de <span style=\"font-weight: bold;\">Persianas y Cortinas</span> en<span style=\"font-weight: bold;\">&nbsp;Tulum</span>, desarrollamos una gran variedad de soluciones integrales para la decoración en casas, oficinas, proyectos de arquitectos y decoradores de interiores.<div><br></div><div>Ofrecemos Persianas y Cortinas de marcas reconocidas; entre ellas&nbsp;la marca Gabin en todas sus variedades, en donde puedes encontrar desde persianas Enrollables, Romanas, Paneles hasta las mas sofisticadas como las Sheer elegance y Shangri-la. Practicamente todas nuestras persianas se pueden motorizar con las mejores marcas como lo es Somfy en la cual te ofrcemos 10 años de garantía.</div><div><br></div><div>¡Decora tu casa, negocio y oficina en <span style=\"font-weight: bold;\">Tulum</span>!</div>', 'Persianas y Cortinas en Tulum', 'Cortinas y persianas Tulum', 3, 1),
(33, 'Alfombras', 'alfrombras-playa-del-carmen.jpg', '<div>En <span style=\"font-weight: bold;\">Sundec Decoración</span> nos especializamos en la decoración de interiores de cualquier espacio. Tenemos lo mejor en <span style=\"font-weight: bold;\">Alfombras y Tapetes</span> para decorar tu casa, negocio u oficina en <span style=\"font-weight: bold;\">Playa del Carmen.</span></div><div><br></div><div>Contamos con varios tipos de alfombras y tapetes, como por ejemplo la <span style=\"font-weight: bold;\">Alfombra Modular,</span> que es excelente para el Lobby de un Hotel o un cine, así como para salas de conferencias. También tenemos tapetes diseñados o tapetes hechos a la medida del cliente, para la sala, habitación o sala de juegos de cualquier casa, oficina u hotel en Playa del Carmen, Tulum y Cancún.</div>', 'Alfombras en Playa del Carmen', 'Alfombras en Playa del Carmen', 2, 1),
(34, 'Pisos ', 'pisos playa del carmen.jpg', '<div>Los mejores <span style=\"font-weight: bold;\">Pisos en Playa del Carmen</span>. En Sundec Decoración contamos con pisos de diferentes materiales y técnicas de fabricación. El piso de mayor lujo y elegancia es el de madera de ingeniería de la cual contamos con la variedad más extensa de especies y formatos de duelas para cubrir hasta los gustos más exigentes.</div><div><br></div><div>También tenemos disponible <span style=\"font-weight: bold;\">duelas</span> de imitación madera en material vinílico con tecnología Belga que está revolucionando el mercado de los pisos. Te recomendamos mucho estos<span style=\"font-weight: bold;\"> Pisos Vinílicos</span>. Si estás buscando algo más accesible pero que no pierda de vista la apariencia y lujo de la madera contamos con piso laminado. Todos nuestros pisos soportan los climas de <span style=\"font-weight: bold;\">Playa del Carmen</span> y la <span style=\"font-weight: bold;\">Riviera Maya.</span></div><div><br></div>', 'Pisos en Playa del Carmen', 'Pisos creativos en Playa del Carmen', 2, 1),
(35, 'Papel Tapiz', 'papel tapiz playa del carmen.jpg', '<div>Acabados en Pared en<span style=\"font-weight: bold;\"> Playa del Carmen</span>. Si deseas agrega<span style=\"font-weight: bold;\">r Papel Tapiz </span>a tus paredes definitivamente no te arrepentirás. Las paredes lucen espectaculares con un acabado decorativo.&nbsp;</div><div><br></div><div>Contamos con un catálogo completo de Acabados en Pared para la decoración de tu casa u Hotel en Cancún, <span style=\"font-weight: bold;\">Playa del Carmen</span> o Riviera Maya.&nbsp;</div><div><br></div><div>Tenemos una gama completa de acabados en Piedra para Pared que llamados Panel Stone o Panel Piedra que darán un toque más armónico al ambiente que desees mejorando la decoración de cualquier lugar. También contamos con una colección de papel tapiz extensa para cualquier tipo de decoración.</div><div><br></div><div><br></div>', 'Papel Tapiz en Playa del Carmen', 'Papel Tapiz en Playa del Carmen', 2, 1),
(36, 'Alfombras', 'alfombras-grandes-playa-del-carmen.jpg', '<div>En Sundec Decoración somos especialistas en Diseño de Interiores y tenemos las mejores opciones en Alfombras y Tapetes para la decoración de tu casa u oficina en <span style=\"font-weight: bold;\">Cancún, Riviera Maya y Playa del Carmen.</span></div><div><br></div><div>&nbsp;</div><div>Contamos con varios tipos de alfombras y tapetes, como por ejemplo la Alfombra Modular, que es excelente para el Lobby de un <span style=\"font-weight: bold;\">Hotel</span> o un cine, así como salas de conferencias. Por el otro lado tenemos tapetes diseñados o tapetes hechos a la medida del cliente, para la sala, habitación o sala de juegos de cualquier casa, oficina u hotel en <span style=\"font-weight: bold;\">Playa del Carmen.</span></div>', 'Alfombras en Playa del Carmen', 'Alfombras en Playa del Carmen', 1, 0),
(37, 'Pisos ', 'pisos-decorativos-en-tulum.jpg', '<div>Los mejores<span style=\"font-weight: bold;\"> Pisos en</span> <span style=\"font-weight: bold;\">Tulum</span>. En Sundec Decoración contamos con pisos de diferentes materiales y técnicas de fabricación. El piso de mayor lujo y elegancia es el de madera de ingeniería de la cual contamos con la variedad más extensa de especies y formatos de duelas para cubrir hasta los gustos más exigentes. Decora tu espacio con nosotros, contamos con excelente precios y calidad.</div><div><br></div><div>También tenemos disponible duelas de imitación madera en material vinílico con tecnología Belga que está revolucionando el mercado de los pisos. Te recomendamos mucho estos <span style=\"font-weight: bold;\">Pisos Vinílicos</span>. Si estás buscando algo más accesible pero que no pierda de vista la apariencia y lujo de la madera contamos con piso laminado. Todos nuestros pisos soportan los climas de <span style=\"font-weight: bold;\">Tulum</span> y la <span style=\"font-weight: bold;\">Riviera Maya</span>.</div><div><br style=\"font-family: Arial, Helvetica, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);\"></div>', 'Pisos de Madera y Vinilicos en Tulum', 'Pisos decorativos en Tulum', 3, 1),
(38, 'Alfombras', 'alfombras-en-tulum.jpg', '<div>En Sundec Decoración somos especialistas en Diseño de Interiores y tenemos las mejores opciones en Alfombras y Tapetes para la decoración de tu casa, hotel u oficina en <span style=\"font-weight: bold;\">Cancún, Riviera Maya</span> y <span style=\"font-weight: bold;\">Tulum.</span></div><div><br></div><div>En <span style=\"font-weight: bold;\">Sundec Decoración</span> contamos con una amplia variedad de alfombras y tapetes, como por ejemplo la<span style=\"font-weight: bold;\"> Alfombra Modular</span>, que es excelente para el Lobby de un <span style=\"font-weight: bold;\">Hotel</span> o un cine, así como salas de conferencias. Por el otro lado tenemos tapetes diseñados o tapetes hechos a la medida del cliente, para la sala, habitación o sala de juegos de cualquier casa, oficina u hotel en <span style=\"font-weight: bold;\">Tulum.</span></div>', 'Alfombras en Tulum', 'Alfombras en Tulum', 3, 1),
(39, 'Papel Tapiz', 'papel-tapiz-tulum.jpg', '<div><span style=\"font-weight: bold;\">Acabados en Pared en Tulum</span>. Por su presencia visual, las paredes adquieren un destacado protagonismo dentro de la decoración. Agregale un toque único a tu hogar, negocio u oficina.</div><div><br></div><div>Contamos con un catálogo completo de Acabados en Pared para la decoración de tu casa u Hotel en Cancún, Playa del Carmen y <span style=\"font-weight: bold;\">Tulum.</span></div><div><br></div><div>Tenemos una gama completa de acabados en Piedra para Pared que llamados Panel Stone o Panel Piedra que darán un toque más armónico al ambiente que desees mejorando la decoración de cualquier lugar. También contamos con una colección de <span style=\"font-weight: bold;\">papel tapiz</span> extensa para cualquier tipo de decoración.</div>', 'Papel Tapiz en Tulum', 'Papel Tapiz en Tulum', 3, 1),
(40, 'Toldos ', 'toldos-en-tulum.jpg', '<div>En Sundec Decoración proveemos e instalamos toldos de Alta Gama para tu Casa, Hotel o Restaurante, siempre cuidando la línea de diseño. Contamos con marcas como Gabin, Persax y Solair contando con los modelos más básicos hasta los más robustos y con accionamientos motorizados con accesorios como detectores de vientos y sol para su automática activación.</div><div><br></div><div>Contamos con varios proyectos realizados en México y la mayoría de estos en Cancún y la Riviera Maya como Tulum. Contamos con varios modelos y mecanismos en toldos para instalarlos sin problema.</div><div><br></div><div>Al poder realizar un proyecto a la medida, tenemos la posibilidad de hacerlo para todo tipo de negocios como restaurantes con terrazas o bares y albercas de hoteles. Recordando siempre que los toldos que manejamos en Sundec son de Gama Alta.</div><div><br></div>', 'Toldos en Tulum', '', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `idcity` bigint(20) NOT NULL,
  `namecity` varchar(30) DEFAULT NULL,
  `urlcity` varchar(30) DEFAULT NULL,
  `statecity` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`idcity`, `namecity`, `urlcity`, `statecity`) VALUES
(1, 'Cancún', '/cancun', 1),
(2, 'Playa del Carmen', '/playa-del-carmen', 1),
(3, 'Tulum', '/tulum', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `idContacto` int(11) NOT NULL,
  `firstAddres` varchar(255) NOT NULL,
  `secondAddres` varchar(255) NOT NULL,
  `lastAddres` varchar(255) NOT NULL,
  `mailContacto` varchar(255) NOT NULL,
  `phoneContacto` varchar(255) NOT NULL,
  `mailForm` varchar(255) NOT NULL,
  `latContacto` varchar(255) NOT NULL,
  `longContacto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`idContacto`, `firstAddres`, `secondAddres`, `lastAddres`, `mailContacto`, `phoneContacto`, `mailForm`, `latContacto`, `longContacto`) VALUES
(1, 'Av. Huayacan Zona 5 ', 'Manzana 101 Lote 29', 'Cancún, Quintana Roo C.P.77560', 'contacto@sundecdecoracion.com', '(998) 2149274 / (998) 8889292', 'contacto@sundecdecoracion.com; noelurbainflores@gmail.com', '21.1281832', '-86.8366752');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

CREATE TABLE `galeria` (
  `idPhotoYacht` int(11) NOT NULL,
  `idPage` int(11) NOT NULL,
  `imageGallery` varchar(255) NOT NULL,
  `altImageGallery` varchar(255) NOT NULL DEFAULT 'alt of image',
  `viewTitle` varchar(255) NOT NULL DEFAULT 'Title of image',
  `viewDescription` varchar(255) NOT NULL DEFAULT 'Description of image'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `idmarca` int(11) NOT NULL,
  `marca` varchar(255) NOT NULL,
  `logotipo` varchar(255) NOT NULL,
  `descripcion` text,
  `linkTo` varchar(255) DEFAULT '#',
  `estado` int(1) NOT NULL DEFAULT '1',
  `titleLink` varchar(255) DEFAULT 'Marcas Sundec',
  `altLogotipo` varchar(255) DEFAULT 'Marcas Sundec'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`idmarca`, `marca`, `logotipo`, `descripcion`, `linkTo`, `estado`, `titleLink`, `altLogotipo`) VALUES
(10, 'Cappa', 'marca-cappa-pisos-cancun.jpg', 'Pisos  Creativos creó la  marca cappa®   la cual desde su creación ha ganado el reconocimiento y la aceptación de los clientes posicionándose inmediatamente como la marca de mayor prestigio en el ámbito de la arquitectura, la decoración y la construcción.\r\n \r\nEn cappa® usted encuentra la más amplia gama de pisos  laminados, piso de vinil, maderas de ingeniería y sólidas, así como decks para exterior.', 'catalogo/decks-cancun', 1, 'Decks y Pisos Laminados en Cancún', 'Quien instala pisos en Cancún con la marca cappa'),
(11, 'Coventino', 'marca-coventino-pisos-madera-cancun.jpg', 'Coventino se fabrica con la tecnología más avanzada a nivel mundial, juntando productos de la más alta calidad de países como Italia, Canadá y Brasil, entre otros.\r\n\r\nCada piso COVENTINO™ es fabricado por artesanos altamente entrenados y calificados, quienes dominan las distintas especies de madera tales como encino, nogal, maple y alerce.', 'catalogo/decks-cancun', 1, 'Pisos Laminados de Madera', 'Pisos de madera en Cancún marca Coventino'),
(13, 'Dues by Drapes', 'marca-dues-drapes-persianas-cortinas-cancun.jpg', 'Cortinas y Persianas de calidad con Sundec Decoración en Cancún. \r\n\r\nEn Dues Textil llevamos dos décadas especializados en la fabricación y comercialización de textiles y productos confeccionados de alto rendimiento para hotelería.\r\n\r\nNuestras colecciones exclusivas de textiles presentan una amplia gama de estilos y tendencias, siempre a la vanguardia.\r\n\r\nManejamos tejidos inherentemente retardantes al fuego pertenecientes a la prestigiosa marca: Trevira FR. También tenemos textiles repelentes a las manchas y al polvo de la reconocida marca Crypton.', 'catalogo/persianas-cortinas-cancun#Persianas Drapes by Dues', 1, 'Persianas y Cortinas Drapes by Dues', 'Cortinas y Persianas Dues by Drapes en Cancún'),
(18, 'Persax', 'marca-persax-persianas-toldos-cancun.jpg', 'Persax. Toldos, sombrillas, telax y más soluciones para exteriores. PERSAX es una empresa líder en el sector de las persianas, el cerramiento y todos nuestros productos están certificados por la norma ISO9001, CTE y normativas exigidas por diferentes mercados.\r\nPonemos a su disposición una amplia gama de productos de protección solar: persianas, accesorios, motorización, etc...', 'catalogo/toldos#Toldos Persax Cancun', 1, 'Toldos Persax en Cancún', 'Toldos, cortinas y mas telas para exteriores de persax en Cancún'),
(20, 'Somfy', 'marca-somfy-motores-cortinas-cancun.jpg', 'Mecanismos de apertura de cortinas, toldos,Somfy es el líder mundial en la fabricación de motores y sistemas de control para la automatización de todo tipo de cerramientos interiores (persianas europeas, toldos, cortinas y persianas) y exteriores (portones y puertas de garaje). persianas y puertas.', '', 1, '', 'Somfy mecanismos para abrir cortinas, persianas, puertas en Cancún'),
(21, 'Sunbrella', 'marca-sunbrella-telas-exteriores.jpg', 'Sunbrella TextileSunbrella produce una variedad de textiles para muebles ya sea de interiores o exteriores. Desde cojines a sombrillas para patios y toldos, Sunbrella es la solución perfecta para textiles durables que no se destiñen ni se percuden.s en Cancún.', '', 1, '', 'Sunbrella textiles para exteriores decorativos en Cancún'),
(25, 'Gabin', 'gabin-persianas-cancun.jpg', 'Persianas Gabín, empresa con más de 50 años de experiencia en el mercado, que fabrica y comercializa persianas, toldos y automatización con cobertura a nivel nacional bajo los estándares internacionales más estrictos de calidad. Con la colección más completa, Gabín ofrece soluciones para controlar luz natural, satisfaciendo las necesidades de privacidad, protección y decoración.', 'catalogo/persianas-cortinas-cancun', 1, 'Persianas en Cancún marca Gabin', 'Persianas en Cancun marca Gabin'),
(29, 'Solair', 'solair-toldos-cancun.jpg', 'Toldos SOLAIR® es una marca registrada de Tunali Tec®, empresa mexicana de distribución de productos para protección solar y textiles especializados, socio mexicano de Trivantage®, el mayor distribuidor para este mercado en Norteamérica.', 'catalogo/toldos-cancun', 1, 'Toldos en Cancún marca Solair', 'Toldos en Cancún para hoteles y albercas'),
(32, 'New Tech Wood', 'new-tech-wood-deck-sintetico.jpg', 'NewTechWood® es una empresa pionera en el desarrollo y fabricación de deck compuesto. Ha ganado una reputación mundial por nuestra atractiva, innovadora y confiable fabricación de materiales compuestos de madera y plástico. Los productos de NewTechWood® están diseñados para ofrecer un rendimiento sin igual resultando en la satisfacción total del cliente. Perfecto para Decks de exteriores.', 'catalogo/decks-cancun', 1, 'New Tech Wood Decks Sintéticos', 'New Tech Wood Decks sinteticos'),
(34, 'Terza', 'pisos terza en cancun.jpg', 'Somos distribuidores de uno de los fabricantes más grandes de alfombras, pisos y pasto sintético de México y Latinoamérica. Complementos únicos para tu diseño de interiores.\r\nCada piso y alfombra esta fabricado con los estándares de calidad más altos del mundo, por lo que puedes tener la seguridad que tendrás productos de primera.', NULL, 1, NULL, 'Pisos Terza cancun '),
(35, 'Eclissare', 'proveedores de persianas eclissare en cancun.jpg', 'Somos proveedores de una de las marcas innovadoras y vanguardistas del mercado; Eclissare, cuenta con venta de cortinas y persianas en México. Contamos con los clásicos modelos de persianas enrollables, de paneles y otras más para soluciones de decoración de interiores modernos y formales como lo podría ser una oficina. Sin embargo, las persianas de madera y diferentes materiales para el hogar también están disponibles en nuestro catalogo en una amplia gama de productos.', NULL, 1, NULL, 'Proveedores de persianas eclissare en cancun'),
(36, 'Tekno Step', 'proveedores-de-pisos-en-cancun-teknos-step.jpg', 'Distribuidores oficiales de Tekno Step, contamos con una amplia gama de  piso vinílico, piso de madera de Ingeniería, piso laminado, pasto sintético, moldura laminada y zoclo. Con gusto podemos ofrecer los mejores pisos en Cancún para que tu decoración de interiores o remodelación sea un éxito. Contamos con las mejores marcas.', NULL, 1, NULL, 'Proveedores de pisos en cancun Tekno Step');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulospdf`
--

CREATE TABLE `modulospdf` (
  `pkmodulo` int(11) NOT NULL,
  `modulo` varchar(255) NOT NULL,
  `nivel` int(11) DEFAULT NULL,
  `estadomod` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modulospdf`
--

INSERT INTO `modulospdf` (`pkmodulo`, `modulo`, `nivel`, `estadomod`) VALUES
(10, 'PERSIANAS Y CORTINAS', 1, 1),
(11, 'PAPEL TAPIZ', 1, 1),
(12, 'MUEBLES', 1, 1),
(13, 'TOLDOS', 1, 1),
(14, 'CANDILES Y LÁMPARAS', 1, 1),
(15, 'DECKS', 1, 1),
(16, 'PISOS', 1, 1),
(17, 'TELAS', 1, 1),
(18, 'ARTICULOS DECORATIVOS', 1, 1),
(19, 'SOMBRILLAS', 1, 1),
(20, 'ALFOMBRAS', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `newsletter`
--

CREATE TABLE `newsletter` (
  `idNewsletter` int(11) NOT NULL,
  `mailNews` varchar(255) NOT NULL,
  `dateSuscribe` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `newsletter`
--

INSERT INTO `newsletter` (`idNewsletter`, `mailNews`, `dateSuscribe`) VALUES
(1, 'ajimenez.devs@gmail.com', '2014-10-26'),
(2, 'testnewsletter@tester.com', '2014-10-30'),
(3, 'developer4@dtraveller.com', '2014-12-13'),
(4, 'joce_chio@hotmail.com', '2014-12-13'),
(5, 'developer3@dtraveller.com', '2014-12-13'),
(6, 'developer5@dtraveller.com', '2014-12-13'),
(7, 'clara09@hotmail.com', '2015-11-22'),
(8, 'terenunez71@hotmail.com', '2016-07-19'),
(9, 'eltruequemexican@gmail.com', '2016-09-14'),
(10, 'blanca3078@hotmail.com', '2016-10-04'),
(11, 'jc.morin.2@gmail.com', '2016-10-25'),
(12, 'angelinnapoles@gmail.com', '2016-12-16'),
(13, 'trifora60@gmail.com', '2017-06-04'),
(14, 'cecilia.ortiz@iberoservicemexico.com', '2017-06-12'),
(15, 'estherhernandez_ehd@yahoo.com', '2017-06-27'),
(16, 'azael_gam@hotmail.com', '2017-07-10'),
(17, 'Eliias_hernandez23@hotmail.com', '2017-08-09'),
(18, 'cieloo13@yahoo.com', '2017-08-12'),
(19, 'maurecabo@gmail.com', '2017-10-13'),
(20, 'Laynne_mendoza1009@hotmail.com', '2018-01-11'),
(21, 'Korramorra@hotmail.com', '2018-03-04'),
(22, 'elydecor1973@gmail.com', '2018-03-14'),
(23, 'victormdz_2@hotmail.com', '2018-03-21'),
(24, 'gracielamorenor@hotmail.com ', '2018-09-11'),
(25, 'Jaclin@investmentvallejo.com', '2018-09-12'),
(26, 'dorte4@hotmail.com', '2019-01-22'),
(27, 'samuelkumul@gmail.com', '2019-01-26'),
(28, 'arturo.martinez@posadas.com', '2019-03-22'),
(29, 'arq.clara.mohino@gmail.com', '2019-04-05'),
(30, 'rcollins911@me.com', '2020-05-01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promos`
--

CREATE TABLE `promos` (
  `idpromo` int(11) NOT NULL,
  `promo` varchar(255) NOT NULL,
  `imglist` varchar(255) NOT NULL,
  `imgoverlay` varchar(255) NOT NULL,
  `finicio` date DEFAULT NULL,
  `ffin` date DEFAULT NULL,
  `altPromo` varchar(255) NOT NULL DEFAULT 'Promociones del mes | Sundec',
  `titlePromo` varchar(255) NOT NULL DEFAULT 'Promociones del mes | Sundec'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `promos`
--

INSERT INTO `promos` (`idpromo`, `promo`, `imglist`, `imgoverlay`, `finicio`, `ffin`, `altPromo`, `titlePromo`) VALUES
(9, '20% de descuento en persianas Eclissare', 'persianas-eclissare-en-descuento-hasta-20.jpg', 'persianas-eclissare-descuento-cancun-todo-mexico.jpg', '2018-05-01', '2019-01-31', 'Persianas Eclissare en Cancun', '20% de descuento en persianas Eclissare'),
(11, 'Liquidación de muebles', 'muebles-en-cancun-liquidacion-existencias.jpg', 'muebles-liquidacion-en-cancun.jpg', '2017-11-15', '2018-12-31', 'Liquidación de muebles en Cancún', 'Liquidación de muebles en Cancún'),
(12, 'En pastos residenciales Natur y Proturf', 'descarga (2).png', 'descarga (1).png', '2018-11-16', '2018-06-20', 'En pastos residenciales Natur y Proturf', 'Promociones del temporada'),
(13, '25% en todas las maderas en inventario *No aplica si se coloca a producción o bajo pedido', 'descarga (3).png', 'promociones de buen fin madera pisos cancun 25 descuento.JPG', '2018-11-16', '2018-12-20', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(14, '10% de descuento en el piso Vinil Norwich II', '10 descuento piso Vinil Norwich II en cancun premium.jpg', '10 descuento piso Vinilico en Cancun Norwich II.JPG', '2018-11-16', '2018-11-23', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(15, '20% de descuento el los pisos Viniles Vallée du Mat', '20% DE DESCUENTO EL LOS PISOS VINILES VALLÉE DU MAT.jpg', '20% DE DESCUENTO EL LOS PISOS VINILES VALLÉE DU MAT pisos vinilivos.jpg', '2018-11-16', '2018-11-23', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(16, 'Del día 16 al 23 de noviembre, que consta de un 20% de descuento en diferentes estilos', 'descuento buen fin alfombras modulares en Cancun.jpg', '20 porciento descuento buen fin alfombras modulares en Cancun.jpg', '2018-11-16', '2018-11-23', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(17, 'Descuentos de Tekno Buen Fin 2018', 'descuentos y promociones de decoración de interiores Tekno Buen Fin 2018.jpg', 'Tekno Buen Fin descuentos y promociones de decoración de interiores.jpg', '2018-11-16', '2018-12-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(18, '20% de descuento en toda la linea de persianas Eclissare', 'SUNDEC - Promociones web 01 A.jpg', 'SUNDEC - Promociones web 01 B.jpg', '2019-05-06', '2019-06-30', 'Motores para persianas con 20% de descuento', 'Motores para persianas'),
(19, '20% de descuento en motores', 'SUNDEC - Promociones web 02 A.jpg', 'SUNDEC - Promociones web 02 B.jpg', '2019-05-06', '2019-06-30', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(20, '20% de descuento en cortinas y cortineros', 'SUNDEC - Promociones web 03 A.jpg', 'SUNDEC - Promociones web 03 B.jpg', '2019-05-30', '2020-01-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(21, '10% de descuento en rollos de papel tapiz', 'SUNDEC - Promociones web 04 A.jpg', 'SUNDEC - Promociones web 04 B.jpg', '2019-05-30', '2019-06-15', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(22, '15% de descuento por introducción ', 'SUNDEC - Promociones web 05 A.jpg', 'SUNDEC - Promociones web 05 B.jpg', '2019-05-30', '2019-06-15', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(23, '14% de descuento en model maker', 'SUNDEC - Promociones web 06 A.jpg', 'SUNDEC - Promociones web 06 B.jpg', '2019-05-30', '2019-06-07', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(24, '8% de descuento en modelos seleccionados ', 'SUNDEC - Promociones web 07 A.jpg', 'SUNDEC - Promociones web 07 B.jpg', '2019-05-30', '2019-06-07', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(25, '13% de descuento en varios modelos ', 'SUNDEC - Promociones web 08 A.jpg', 'SUNDEC - Promociones web 08 B.jpg', '2019-05-30', '2019-06-07', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(26, 'super rebajas en catalagos de papel tapiz', 'SUNDEC - Promociones web 09 A.jpg', 'SUNDEC - Promociones web 09 B.jpg', '2019-06-10', '2019-06-30', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(27, '20% de descuento en las lineas romana, papel y celulares', 'SUNDEC - Promociones web 10 A.jpg', 'SUNDEC - Promociones web 10 B.jpg', '2019-07-01', '2019-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(28, '20% de descuento en linea romana y panel', 'SUNDEC - Promociones web 11 A.jpg', 'SUNDEC - Promociones web 11 B.jpg', '2019-07-01', '2019-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(29, 'Motores con precio especial', 'SUNDEC - Promociones web 12 A.jpg', 'SUNDEC - Promociones web 12 B.jpg', '2019-07-01', '2019-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(30, 'super rebajas en catalogos de papel tapiz', 'SUNDEC - Promociones web 13 A.jpg', 'SUNDEC - Promociones web 13 B.jpg', '2019-07-01', '2019-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(31, '25% de descuento en persianas y cortinas gabin', 'SUNDEC - Promociones web 14 A.jpg', 'SUNDEC - Promociones web 14 B.jpg', '2019-07-01', '2019-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(32, '20% de descuento toldos y pisos bergo', 'SUNDEC - Promociones web 15 A.jpg', 'SUNDEC - Promociones web 15 B.jpg', '2019-07-01', '2019-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(33, 'super rebajas en la linea econoline ', 'SUNDEC - Promociones web 16.jpg', 'SUNDEC - Promociones web 16 (1).jpg', '2019-07-01', '2019-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(34, 'super rebajas en la linea shade solutions', 'SUNDEC - Promociones web 17.jpg', 'SUNDEC - Promociones web 17 (1).jpg', '2019-07-01', '2019-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(35, 'Super remate en persianas enrrollables', 'SUNDEC - Promociones web 18.jpg', 'SUNDEC - Promociones web 18 (1).jpg', '2019-08-11', '2019-10-30', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(36, '8% DE DESCUENTO LINEA PHIFER Y SUNTEX', 'SUNDEC - Promociones web 19.jpg', 'SUNDEC - Promociones web 19 (1).jpg', '2019-10-01', '2019-10-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(38, '13% DE DESCUENTO EN TELAS SELECCIONADAS ', 'SUNDEC - Promociones web 20.jpg', 'SUNDEC - Promociones web 20 (1).jpg', '2019-10-01', '2019-10-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(39, '14% DE DESCUENTO EN TELAS SELECCIONADAS ', 'SUNDEC - Promociones web 21.jpg', 'SUNDEC - Promociones web 21 (1).jpg', '2019-10-01', '2019-10-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(40, '10% DE DESCUENTO EN TELAS SELECCIONADAS ', 'SUNDEC - Promociones web 22.jpg', 'SUNDEC - Promociones web 22 (1).jpg', '2019-10-01', '2019-10-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(41, '20% DE DESCUENTO EN PERSIANAS', 'SUNDEC - Promociones web 23.jpg', 'SUNDEC - Promociones web 23 (1).jpg', '2019-10-01', '2019-10-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(42, '10% DE DESCUENTO EN TELAS SELECCIONADAS ', 'SUNDEC - Promociones web 24.jpg', 'SUNDEC - Promociones web 24 (1).jpg', '2019-10-01', '2019-10-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(43, '15% DE DESCUENTO EN TODAS LAS LINEAS ', 'SUNDEC - Promociones web 25.jpg', 'SUNDEC - Promociones web 25 (1).jpg', '2019-10-01', '2019-10-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(44, '15% DE DESCUENTO POR INTRODUCCION ', 'SUNDEC - Promociones web 26.jpg', 'SUNDEC - Promociones web 26 (1).jpg', '2019-10-01', '2019-10-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(45, 'Hasta 30% de descuento en lineas GABIN', 'SUNDEC - Promociones web 27.jpg', 'SUNDEC - Promociones web 27 (1).jpg', '2019-11-11', '2019-11-22', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(46, '25% de descuento alfombras modulares LUXOR', 'SUNDEC - Promociones web 28.jpg', 'SUNDEC - Promociones web 28 (1).jpg', '2019-11-11', '2019-11-22', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(47, '40% de descuento alfombras modulares MOHAWK', 'SUNDEC - Promociones web 29.jpg', 'SUNDEC - Promociones web 29 (1).jpg', '2019-11-11', '2019-11-22', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(48, '30% DE DESCUENTO Papales tapices linea verde', 'SUNDEC - Promociones web 30.jpg', 'SUNDEC - Promociones web 30 (1).jpg', '2019-11-01', '2019-12-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(49, '50% DE DESCUENTO Línea Natural Faux', 'SUNDEC - Promociones web 31.jpg', 'SUNDEC - Promociones web 31 (1).jpg', '2019-11-01', '2019-12-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(50, 'SÚPER DESCUENTO en persianas Eclissare', 'SUNDEC - Promociones web 32.jpg', 'SUNDEC - Promociones web 32 (1).jpg', '2019-11-01', '2019-11-30', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(52, '20% DE DESCUENTO Papel tapiz Decora Pro', 'SUNDEC - Promociones web 34.jpg', 'SUNDEC - Promociones web 34 (1).jpg', '2019-11-01', '2019-11-30', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(53, '15% DE DESCUENTO Decks Extrutech', 'SUNDEC - Promociones web 35.jpg', 'SUNDEC - Promociones web 35 (1).jpg', '2019-11-01', '2019-11-30', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(54, '14% DE DESCUENTO Telas seleccionadas Sumbrella', 'SUNDEC - Promociones web 36.jpg', 'SUNDEC - Promociones web 36 (1).jpg', '2019-11-01', '2019-12-03', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(55, '9% de descuento linea phifer y suntex', 'SUNDEC - Promociones web 37.jpg', 'SUNDEC - Promociones web 37 (1).jpg', '2019-11-01', '2019-12-03', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(56, 'SÚPER DESCUENTO en persianas Econoline', 'SUNDEC - Promociones web 33.jpg', 'SUNDEC - Promociones web 33 (1).jpg', '2019-11-01', '2019-11-30', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(57, '10% de descuento en rollos de papel tapiz', 'SUNDEC - Promociones web 04.jpg', 'SUNDEC - Promociones web 04 (1).jpg', '2020-01-01', '2020-01-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(58, '20% de descuento en persianas', 'Sheer elegance traslucido blanco.jpg', 'PROMO BASICS JUNIO 2020.jpeg', '2020-06-23', '2020-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(59, 'PERSIANA COVID19', 'IMAGEN PROMO ANTI COVID.jpeg', 'IMAGEN PROMO ANTI COVID.jpeg', '2020-06-23', '2020-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(60, '15% de descuento en papel tapiz', 'papel-tapiz-en-cancun-promocion.png', 'papel-tapiz-en-cancun-promocion.png', '2020-08-01', '2020-09-02', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(61, '10% de descuento variedad de pisos, laminados, vinilicos, madera, deck sintetico', 'SUNDEC - Promociones web 41.jpg', 'SUNDEC - Promociones web 41 (1).jpg', '2020-02-01', '2020-02-28', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(62, 'Remate persianas con telas descontinuadas', 'SUNDEC - Promociones web 42.jpg', 'SUNDEC - Promociones web 42 (1).jpg', '2020-02-01', '2020-02-28', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(63, 'Motor con precio especial marca coulisse', 'SUNDEC - Promociones web 43.jpg', 'SUNDEC - Promociones web 43 (1).jpg', '2020-02-01', '2020-02-28', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(64, 'rebajas en persianas modelos de tela', 'SUNDEC - Promociones web 44.jpg', 'SUNDEC - Promociones web 44 (1).jpg', '2020-02-01', '2020-02-28', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(65, 'rebajas persianas shades modelos en tela', 'SUNDEC - Promociones web 45.jpg', 'SUNDEC - Promociones web 45-2.jpg', '2020-02-01', '2020-02-28', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(66, '20% de Descuento en Cortinas de la Línea Basics', '20-descuento-cortinas-basics-cancun.jpeg', '20-descuento-cortinas-basics-cancun.jpeg', '2020-07-07', '2020-07-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(67, '20% de Descuento en la línea confort de Persianas', '20-descuento-persianas-cancun-confort.jpeg', '20-descuento-persianas-cancun-confort.jpeg', '2020-07-07', '2020-07-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(68, 'Motoriza tus persianas con Altus', 'persianas-cancun-motorizadas-altus-somfy.jpeg', 'persianas-cancun-motorizadas-altus-somfy.jpeg', '2020-07-07', '2020-07-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(69, '20% de descuento en cortinas tradicionales de tela', 'cortinas-en-cancun-promociones.png', 'cortinas-en-cancun-promociones.png', '2020-08-01', '2020-09-02', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(70, '5% de descuento en pisos de madera, vinílicos y laminados', 'promociones-pisos-en-cancun.png', 'promociones-pisos-en-cancun.png', '2020-08-01', '2020-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(71, '20% de descuento en alfombras, tapetes y pisos ', 'alfombras-tapetes-pisos-promociones-en-cancun.png', 'alfombras-tapetes-pisos-promociones-en-cancun.png', '2020-08-01', '2020-09-02', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(72, '10% de descuento en Decks modelo catalunya', 'promocion-pisos-de-decks-en-cancun.png', 'promocion-pisos-de-decks-en-cancun.png', '2020-08-01', '2020-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(73, 'Precios de remate en PISO VInílICO MODELO CHASSIS Y MODELO NORWICH II MARCA TERZA', 'pisos-vinilicos-en-cancun-promociones.png', 'pisos-vinilicos-en-cancun-promociones.png', '2020-08-01', '2020-08-31', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(74, 'Precio especial  en persianas motorizadas', 'promocion-cortinas-motorizadas-cancun.jpg', 'promocion-cortinas-motorizadas-cancun.jpg', '2020-09-03', '2020-09-30', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(75, '20% En persianas', 'persianas-en-cancun-descuentos.jpg', 'persianas-en-cancun-descuentos.jpg', '2020-09-03', '2020-09-30', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec'),
(76, '20% de descuento en  linea confort', 'persianas-en-cancun-promociones.jpg', 'persianas-en-cancun-promociones.jpg', '2020-09-03', '2020-09-30', 'Promociones del mes | Sundec', 'Promociones del mes | Sundec');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `pkproyecto` int(11) NOT NULL,
  `proyecto` varchar(255) NOT NULL,
  `miniatura` varchar(255) DEFAULT NULL,
  `altimg` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `contenido` text NOT NULL,
  `fkmedia` int(11) DEFAULT NULL,
  `creado` date DEFAULT NULL,
  `estado` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`pkproyecto`, `proyecto`, `miniatura`, `altimg`, `url`, `contenido`, `fkmedia`, `creado`, `estado`) VALUES
(1, 'Akumal Bay', 'proyecto-decoracion-cancun-akumal-bay.jpg', 'Proyecto de Decoración Cancún', NULL, 'Akumal Bay', NULL, '2017-05-04', 1),
(2, 'Andersons', 'proyecto-decoracion-cancun-andersons.jpg', 'Proyecto de Decoración Cancún Andersons', NULL, 'Andersons', NULL, '2017-05-04', 1),
(3, 'Club Med', 'proyecto-decoracion-cancun-club-med.jpg', 'Proyecto de Decoración Cancún Club Med', NULL, 'Club Med', NULL, '2017-05-04', 1),
(4, 'Garza Blanca', 'proyecto-decoracion-cancun-garza-blanca.jpg', 'Proyecto de Decoración Cancún Garza Blanca', NULL, 'Garza Blanca', NULL, '2017-05-04', 1),
(5, 'Halliburton', 'proyecto-decoracion-cancun-halliburton.jpg', 'Proyecto de Decoración Cancún Halliburton', NULL, 'Halliburton', NULL, '2017-05-04', 1),
(6, 'Hyatt', 'proyecto-decoracion-cancun-hyatt.jpg', 'Proyecto de Decoración Cancún Hyatt', NULL, 'Hyatt', NULL, '2017-05-04', 1),
(7, 'Mia Resort', 'proyecto-decoracion-cancun-mia-resort.jpg', 'Proyecto de Decoración Cancún Mia Resort', NULL, 'Mia Resort', NULL, '2017-05-04', 1),
(8, 'Restaurante Nomads', 'proyecto-decoracion-cancun-nomads.jpg', 'Proyecto de Decoración Cancún Nomads', NULL, 'Restaurante Nomads', NULL, '2017-05-04', 1),
(9, 'Hoteles Ocean', 'proyecto-decoracion-cancun-ocean-hotels.jpg', 'Proyecto de Decoración Cancún Ocean Hoteles', NULL, 'Hoteles Ocean', NULL, '2017-05-04', 1),
(10, 'Royal Resorts', 'proyecto-decoracion-cancun-royal-resorts.jpg', 'Proyecto de Decoración Cancún Royal Resorts', NULL, 'Royal Resorts', NULL, '2017-05-04', 1),
(11, 'Secretaría de Hacienda', 'proyecto-decoracion-cancun-shcp.jpg', 'Proyecto de Decoración Cancún Secretaria de Hacienda', NULL, 'Secretaría de Hacienda', NULL, '2017-05-04', 1),
(12, 'Thompson Playa del Carmen', 'proyecto-decoracion-cancun-thompson-hotel.jpg', 'Proyecto de Decoración Cancún Thompson Playa del Carmen', NULL, 'Thompson Playa del Carmen', NULL, '2017-05-04', 1),
(13, 'Hotel Mousai', 'proyecto-decoracion-hotel-mousai.jpg', 'Proyecto de Decoración Cancún', NULL, 'Hotel Mousai', NULL, '2017-05-04', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seo`
--

CREATE TABLE `seo` (
  `idSeo` int(11) NOT NULL,
  `urlPage` varchar(255) NOT NULL,
  `titlePage` varchar(255) NOT NULL DEFAULT 'Sundec Decoracion',
  `keywordsPage` varchar(255) NOT NULL DEFAULT 'Sundec Decoracion',
  `descriptionPage` text,
  `idYacht` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `seo`
--

INSERT INTO `seo` (`idSeo`, `urlPage`, `titlePage`, `keywordsPage`, `descriptionPage`, `idYacht`) VALUES
(1, '/index.php', 'Decoración en Cancún | Persianas, Cortinas, Pisos, alfombras y más | Sundec Decoración', 'decoracion cancun, decoracion en cancun, decoracion de interiores cancun, diseño de interiores cancun, decoracion de cocinas cancun, diseño de cocinas cancun, decoracion de cocinas modernas cancun, decoracion de salas en cancun, persianas decorativas', 'Sundec Decoración Cancún. Empresa especializada en Decoración de Interiores en Cancún, también de exteriores; cuenta cortinas y persianas decorativas. También se especializa en pisos de madera y muebles Decorativos.', 0),
(20, '/quienes-somos.php', 'Somos una Empresa con Soluciones en Decoración en Cancún | Sundec Decoración', 'empresa de decoracion cancun, decoracion de interiores cancun, diseño de interiores cancun', 'Sundec Decoración. Somos una empresa especializada que da soluciones en Decoración para tus áreas externas e internas, ya sea una casa, Hotel u Oficina. Contamos con Cortinas y Persianas decorativas y toda una solución en iluminación.', 0),
(21, '/contacto.php', 'Contáctanos y te daremos una Solución Decorativa para tu casa o proyecto en Cancún | Sundec Decoración', 'decoracion cancun, decoracion en cancun, decoracion de interiores cancun, diseño de interiores cancun, decoracion de cocinas cancun, diseño de cocinas cancun, decoracion de cocinas modernas cancun, decoracion de salas en cancun, persianas decorativas', 'Contacta con nosotros en Sundec Decoración. Estamos listos para darte la mejor solución en tu proyecto decorativo de interiores o exteriores. Tenemos lo necesario en cortinas, pisos, alfombras, paredes y más en Cancún.', 0),
(25, '/catalogo.php', 'Catálogo de Productos | Persianas | Muebles | Pisos | Lámparas | Sundec Decoración', 'productos decorativos cancun, articulos decorativos cancun, cortinas en cancun, persianas en cancun, empresa de decoracion cancun, lamparas decorativas, muebles de diseño', 'Encuentra en el catálogo de Sundec Decoración una completa gama de productos de Decoración entre Persianas, Muebles y lámparas decorativas.', 0),
(26, '/nuestras-marcas.php', 'Nuestras Marcas | Marcas de Decoración que usamos en Cancún | Sundec Decoración', 'empresas de decoracion, empresas de decoracion en cancun, decoracion en cancun, marcas de decoracion en cancun, marcas de muebles cancun, marcas de pisos cancun, sundec decoracion', 'Encuentra las marcas que Sundec Decoración maneja en Cancún, tenemos una amplia gama de productos para cortinas y persianas, pisos de madera o laminados, toldos. Marcas desde Persax, Classic hasta Ranka y más.', 0),
(27, '/promociones.php', 'Promociones de Persianas y Cortinas en Cancún o Soluciones en Decoración | Sundec Decoración', 'muebles en promocion, promocion de muebles en cancun, promocion de cortinas en cancun, promocion de persianas cancun, outlet de muebles en cancun, promo en muebles cancun, muebles baratos cancun', 'Encuentra Promociones en Cancún de Cortinas, Persianas, Muebles decorativos, Diseño de Interiores, Acabados en pared y más, aprovecha nuestros outlets de temporada. Sundec Decoración', 0),
(29, '/catalogo/persianas-cortinas-cancun', 'Persianas y Cortinas en Cancún | Cortinas para sala, comedor, habitación | Enrollable, PVC | Sundec Decoración', 'persianas en cancun, cortinas en cancun, cortinas decorativas, persianas decorativas, cortinas de diseno,  cortinas modernas, modelos de cortinas, cortinas romanas, cortinas para sala, cortinas enrollables, cortinas black out', 'Persianas y Cortinas en Cancún. Contamos con Persianas y Cortinas para sala, cocina, comedor, hotel u oficina, de todo tipo, romana, enrollable, bamboo, black out, de madera, pvc y más.', 5),
(31, '/catalogo/papel-tapiz-cancun', 'Acabados en Pared | Cancún | Acabados de pared en Piedra, madera | Sundec Decoracion', 'acabados en pared, pared laminada cancun, decoracion de pared cancun', 'Acabados en Pared. Pared decorativa en Cancún de Piedra, Madera, etc. Sundec Decoración', 7),
(32, '/catalogo/alfombras-tapetes-cancun', 'Alfombras y tapetes en Cancún | Tapetes para Sala o Alfombra Modular para Negocio | Sundec Decoracion', 'tapetes en cancun, alfombras en cancun, alfombra modular, tapetes para sala, comprar tapetes, comprar alfombra, donde comprar alfombra cancun, alfombra cancun, tapete cancun, tapete decorativo cancun', 'Alfombras y tapetes decorativos en Cancún. Tenemos la Solución para tu sala o Negocio con tapetes decorativos o Alfombras modulares que decorarán tu espacio interior.', 8),
(33, '/catalogo/cortinas-anticiclonicas-cancun', 'Cortinas anticiclónicas en Cancún | Persianas anticiclónicas | Sundec Decoracion', 'cortinas anticiclonicas, persianas anticiclonicas, cortinas anticiclonias cancun, anticiclonicas cancun, anticiclonicas', 'Cortinas anticiclónicas de alta seguridad. Encuentra Cortinas y Persianas anticiclónicas de diversas marcas y la más alta seguridad. Hacemos presupuesto e instalación.', 9),
(34, '/catalogo/celosias-cancun', 'Celosías en Cancún | Instalación y Cotización de Celosías | Sundec Decoracion', 'celosias cancun, celosias, celosias decorativas, instalacion de celosias, comprar celosias, celosías de madera, celosias decorativas exterior, celosia pvc, celosias de madera para jardin, celosias de hormigon, celosias para jardin, celosías de aluminio', 'Celosías en Cancún. Contamos con un catálogo surtido y la experiencia en instalación de Celosías en Cancún. Contamos con modelos de Celposías decorativas y resistentes al ambiente duro.', 10),
(35, '/catalogo/mosquiteras-cancun', 'Mosquiteras en Cancún | Decorativas | Enrollables | Correderas y más | Sundec Decoracion', 'mosquiteras en cancun, mosquiteras, instalacion de mosquiteras, mosquiteras decorativas, mosquiteras enrollables, mosquiteras baratas, mosquiteras para ventanas, mosquitera, mosquiteras correderas, mosquiteras para puertas, cortinas mosquiteras, mosquiter', 'Mosquiteras en Cancún o Mosquiteros. Contamos con un catálogo extenso de mosquiteras enrollables, correderas, para puertas o ventanas y son decorativas para el espacio que tu quieras.', 11),
(36, '/catalogo/muebles-cancun', 'Muebles sobre diseño en Cancún | Muebles sobre pedido a tu medida | Sundec Decoracion', 'muebles baratos, muebles sobre diseño, diseño de muebles, muebles cancun, muebles de diseño, muebles modernos, fabrica de muebles, tiendas de muebles, muebles minimalistas, muebles de oficina, muebles de salon de diseño, muebles contemporaneos, ', 'Muebles en Cancún. Tenemos y creamos muebles sobre diseño o sobre pedido para tu casa, oficina u hotel. Contamos con un catálogo de muebles contemporáneos y minimalistas, modernos.', 12),
(37, '/catalogo/toldos-cancun', 'Toldos en Cancún | Enrollables, plegables y durables | Sundec Decoracion', 'toldos en cancun, toldos, toldos para terrazas, toldos para ventanas, toldos para patios, toldos para balcones, toldos para negocios, toldo para terraza, toldo, toldos plegables, toldos para bares, toldos terraza, toldos para pergolas de madera', 'Toldos en Cancún. Cotización e instalación de toldos en Cancún. Hacemos el toldo decorativo a tu medida, ya sea para una terraza, un balcón o tu negocio en la calle. Son seguros, plegables o enrollables.', 13),
(38, '/catalogo/iluminacion-cancun', 'Iluminación decorativa en Cancún | Lámparas decorativas en Cancún | Sundec Decoracion', 'iluminacion cancun, lamparas cancun, iluminacion, lamparas, lamparas decorativas, iluminacion de interiores, iluminacion decorativa, candelabros decorativos, lamparas de techo decorativas, lampara de techo, lamparas colgantes, lamparas modernas, luminaria', 'Iluminación y lámparas decorativas en Cancún. En Sundec somos expertos en el arte de la iluminación de interiores por lo que contamos con lámparas y diseños decorativos que atraerán a todos tus invitados.', 14),
(40, '/catalogo/decks-cancun', 'Decks en Cancún | Decks Sintéticos o de Madera | Piso Laminado | Sundec Decoracion', 'decks, deck, decks en cancun, decks sinteticos, deck de pvc, deck modular, piso deck, decks para piscinas, deck madera, deck de plastico, piso laminado, deck plastico, deck ecologico', 'Decks en Cancún. Creamos la idea e instalamos decks sintéticos y de Madera en Cancún. Pueden ser decks de plástico para exteriores como alberca o terrazas o también de Madera tratada. Piso Laminado.', 16),
(41, '/catalogo/muebles-exterior-cancun', 'Muebles de Exterior en Cancún | Muebles para Jardín o Playa | Sundec Decoracion', 'muebles de exterior, muebles para exterior, muebles para exteriores, muebles exterior, muebles de jardín, muebles para jardín, muebles jardín, muebles jardin exterior, muebles para terrazas, muebles para la playa, muebles para alberca, camastros', 'Muebles de Exterior. Sundec Decoración es proveedor y diseñador de Muebles de exterior de alta calidad. Pueden ser muebles para jardín o playa, como camastros, mesas, sillas, cojines y más como balinesas.', 17),
(44, '/catalogo/pisos-de-madera-cancun', 'Pisos de Madera en Cancún | Cappa | Coventino | Sundec Decoracion', 'pisos de madera, pisos de madera en cancun, pisos de madera cancun, pisos de madera precios, piso de madera, pisos madera, pisos en madera, tipos de pisos de madera, instalación de pisos de madera', 'Pisos de Madera. En Sundec Decoración somos expertos en la Decoración del hogar, oficina u Hotel. Diseñamos e instalamos Pisos de Madera en Cancún de marcas Cappa y Coventino.', 19),
(45, '/catalogo/telas-cancun', 'Telas Cancún | Telas para muebles y Cortinas en Cancún | Sundec Decoracion', 'telas cancun, cancun telas, comprar telas en cancun, telas en cancun, telas para sofas cancun, telas para cortinas en cancun, telas para persianas cancun telas para ', 'Telas en Cancún. En Sundec Decoración tenemos el mejor catálogo de telas para tu casa u oficina. Contamos con telas para muebles y cortinas.', 20),
(46, '/catalogo/articulos-decorativos', 'Artículos Decorativos en Cancún | Candiles, Lámparas y más... | Sundec Decoracion', 'articulos decorativos, candiles, decoracion de interiores, decoracion de casas, revistas de decoracion, ideas decoracion, tiendas de decoracion, articulos de decoracion', 'Artículos decorativos en Cancún. En Sundec Decoración tenemos una amplia selección de artículos decorativos e ideas de decoración para el hogar y otros ambientes.', 21),
(47, '', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 22),
(48, '/proyectos.php', 'Proyectos | Sundec Decoracion', 'proyectos, decoracion de interiores, diseño de interiores, proyectos cancun, decoracion de interiores cancun, diseño de interiores cancun ', 'Conoce algunos de nuestros proyectos concluidos y clientes satisfechos. Ellos confiaron en Sundec Decoración para brindarles la soluciones decorativas que necesitaban.', 0),
(49, '/catalogos-pdf.php', 'Catalogos en PDF | Sundec Decoracion', 'Catalogos en PDF | Sundec Decoracion', 'Catalogos en PDF | Sundec Decoracion', 0),
(50, '/catalogo/alfombras-cancun', 'Alfombras y Tapetes en Cancún | Sundec Decoracion', 'Alfombras en Cancun, tapetes en Cancun, alfombras modulares, tapetes decorativos, alfombras para oficina en cancun, tapetes cancun, alfombras cancun', 'En Sundec somos especialistas en decoración y tenemos las mejores opciones en Alfombras y Tapetes para la decorar tu casa u oficina en Cancún, Riviera Maya y Playa del Carmen.', 23),
(51, '/catalogo/', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 24),
(52, '/playa-del-carmen/catalogo/categoria-de-prueba-playa', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 25),
(53, '/tulum/catalogo/tulum-test', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 26),
(54, '/playa-del-carmen/catalogo/toldos', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 27),
(55, '/playa-del-carmen/catalogo/decks', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 28),
(56, '/playa-del-carmen/catalogo/cortinas-y-persinas', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 29),
(57, '/tulum/catalogo/toldos', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 30),
(58, '/tulum/catalogo/decks', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 31),
(59, '/tulum/catalogo/cortinas-y-persinas', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 32),
(60, '/playa-del-carmen/catalogo/alfombras', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 33),
(61, '/playa-del-carmen/catalogo/pisos', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 34),
(62, '/playa-del-carmen/catalogo/papel-tapiz', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 35),
(63, '/catalogo/alfombras', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 36),
(64, '/tulum/catalogo/pisos', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 37),
(65, '/tulum/catalogo/alfombras', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 38),
(66, '/tulum/catalogo/papel-tapiz', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 39),
(67, '/tulum/catalogo/toldos', 'Sundec Decoracion', 'Sundec Decoracion', NULL, 40);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `idSetting` int(11) NOT NULL,
  `mailHeader` varchar(255) NOT NULL,
  `phoneHeader` varchar(255) NOT NULL,
  `addresFooter` varchar(255) NOT NULL,
  `mailFooter` varchar(255) NOT NULL,
  `phoneFooter` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`idSetting`, `mailHeader`, `phoneHeader`, `addresFooter`, `mailFooter`, `phoneFooter`) VALUES
(1, 'contacto@sundecdecoracion.com', '998 8889292/ 998 2149274', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

CREATE TABLE `slider` (
  `idSlider` int(11) NOT NULL,
  `imageSlider` varchar(255) NOT NULL,
  `textSlider` varchar(255) DEFAULT NULL,
  `altSlider` varchar(255) DEFAULT NULL,
  `linkSlider` int(1) NOT NULL DEFAULT '0',
  `urlToSlider` varchar(255) DEFAULT NULL,
  `titleSlider` varchar(255) DEFAULT NULL,
  `orderSlider` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`idSlider`, `imageSlider`, `textSlider`, `altSlider`, `linkSlider`, `urlToSlider`, `titleSlider`, `orderSlider`) VALUES
(29, 'toldos-en-cancun.jpg', NULL, 'Toldos y Sombrillas en Cancún para terrazas y Jardines', 1, 'www.sundecdecoracion.com/catalogo/toldos-cancun', 'Toldos en Cancún', 5),
(33, 'deck-pisos-laminados-cancun.jpg', NULL, 'Diseño y construcción de decks de madera en Cancún', 1, 'www.sundecdecoracion.com/catalogo/decks-cancun', 'Decks en Cancún', 7),
(36, 'pisos-de-madera-cancun.jpg', NULL, 'Pisos de madera en cancún instalación y diseño', 1, 'www.sundecdecoracion.com/catalogo/decks-cancun', 'Pisos de Madera en Cancún', 3),
(38, 'papel-tapiz-playa-del-carmen.jpg', NULL, 'Papel tapiz Playa del Carmen', 0, NULL, NULL, 12),
(39, 'papel-tapiz-en-cancun.jpg', NULL, 'Papel Tapiz en Cancun', 0, NULL, NULL, 8),
(40, 'cortinas-en-cancun-blancas.jpg', NULL, 'Comprar cortinas en Cancun Blancas', 0, NULL, NULL, 11),
(41, 'cortinas-en-playa-del-carmen.jpg', NULL, 'Comprar Cortinas en Playa del Carmen', 0, NULL, NULL, 15),
(42, 'telas-kravet-cancun-1.jpg', NULL, 'Comprar Telas especiales en Cancun', 0, NULL, NULL, 6),
(43, 'telas-kravet-playa-del-carmen.jpg', NULL, 'Tienda de Decoración con telas en Playa del Carmen', 0, NULL, NULL, 2),
(44, 'toldos-en-playa-del-carmen.jpg', NULL, 'Toldos en Playa del Carmen Instalación', 0, NULL, NULL, 9),
(50, 'persianas-en-cancun2.jpg', NULL, 'Persianas en Cancún Decorativas y de oficina', 0, NULL, NULL, 1),
(52, '1 Persianas en Playa del Carmen.jpg', NULL, 'Persianas en Cancun', 0, '', '', 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsers` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mailUser` varchar(255) NOT NULL,
  `rolUser` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsers`, `username`, `password`, `mailUser`, `rolUser`) VALUES
(1, 'devealex', '5e8edd851d2fdfbd7415232c67367cc3', 'ajimenez.devs@gmail.com', 1),
(2, 'emarketing', '30dd0ca73c5d7388757579cc2cc99f9a', 'noelurbainflores@gmail.com', 1),
(3, 'sundecdecoracion', 'fbaadf0092f5cd6d0295e9728605f4eb', 'amendez@kaviramx.com', 1),
(5, 'kaviramx', 'da71033e4e66ad8a8ede30cbc825d2c6', 'amendez@kaviramx.com', 1),
(6, 'demorol', '9061bce99f1b6aca17798d22d18d08c0', 'mail@domain.com', 3),
(7, 'admin', 'a7c8a83287c8990bdf186084cc16e27d', 'info@sundecdecoracion.com', 1),
(8, 'mkt', 'b2e4cd7d008014dbf87cb68efe5f838e', 'mkt@sundecdecoracion.com', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE `visitas` (
  `idVisita` int(11) NOT NULL,
  `pageVisit` varchar(255) NOT NULL,
  `dateVisit` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `yates`
--

CREATE TABLE `yates` (
  `idYacht` int(11) NOT NULL,
  `titleYacht` varchar(255) NOT NULL,
  `imageYacht` varchar(255) NOT NULL,
  `altThumb` varchar(255) DEFAULT NULL,
  `priceYacht` varchar(255) DEFAULT NULL,
  `contentYacht` text NOT NULL,
  `categoryAcht` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`idPhoto`);

--
-- Indices de la tabla `anclas`
--
ALTER TABLE `anclas`
  ADD PRIMARY KEY (`idAncla`);

--
-- Indices de la tabla `catalogospdf`
--
ALTER TABLE `catalogospdf`
  ADD PRIMARY KEY (`pkcatalogo`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`idcity`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`idContacto`);

--
-- Indices de la tabla `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`idPhotoYacht`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`idmarca`);

--
-- Indices de la tabla `modulospdf`
--
ALTER TABLE `modulospdf`
  ADD PRIMARY KEY (`pkmodulo`);

--
-- Indices de la tabla `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`idNewsletter`);

--
-- Indices de la tabla `promos`
--
ALTER TABLE `promos`
  ADD PRIMARY KEY (`idpromo`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`pkproyecto`);

--
-- Indices de la tabla `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`idSeo`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`idSetting`);

--
-- Indices de la tabla `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`idSlider`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsers`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`idVisita`);

--
-- Indices de la tabla `yates`
--
ALTER TABLE `yates`
  ADD PRIMARY KEY (`idYacht`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `albums`
--
ALTER TABLE `albums`
  MODIFY `idPhoto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=860;

--
-- AUTO_INCREMENT de la tabla `anclas`
--
ALTER TABLE `anclas`
  MODIFY `idAncla` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT de la tabla `catalogospdf`
--
ALTER TABLE `catalogospdf`
  MODIFY `pkcatalogo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `idcity` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `idContacto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `galeria`
--
ALTER TABLE `galeria`
  MODIFY `idPhotoYacht` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `idmarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `modulospdf`
--
ALTER TABLE `modulospdf`
  MODIFY `pkmodulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `idNewsletter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `promos`
--
ALTER TABLE `promos`
  MODIFY `idpromo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `pkproyecto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `seo`
--
ALTER TABLE `seo`
  MODIFY `idSeo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `idSetting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `slider`
--
ALTER TABLE `slider`
  MODIFY `idSlider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `idVisita` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `yates`
--
ALTER TABLE `yates`
  MODIFY `idYacht` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
